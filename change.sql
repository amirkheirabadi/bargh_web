ALTER TABLE `verification` ADD `status` ENUM('pending','confirmed','','') NOT NULL DEFAULT '''pending''' AFTER `last_name`;

ALTER TABLE `users` CHANGE `token` `forget_token` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `verification` DROP `status`;"
