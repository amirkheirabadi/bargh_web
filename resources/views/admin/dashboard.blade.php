@extends('layouts.dashboard') @section('content')
<br>
<div class="col-md-12">

    <div class="col-md-3">
        <a href="{{URL::to('/admin/users')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد کاربران سایت <i class="fa fa-chevron-left"></i>
                                        </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['users']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{URL::to('/admin/categories')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد دسته بندی های سایت <i class="fa fa-chevron-left"></i>
                    </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['admins']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{URL::to('/admin/brands')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد برندهای ثبت شده &nbsp<i class="fa fa-chevron-left"></i>
                                        </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['brands']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{URL::to('/admin/admins')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد مدیران سایت&nbsp<i class="fa fa-chevron-left"></i>
                                        </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['admins']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

<br><br>
<div class="clear"></div>
<div class="col-md-12">
    <div class="col-md-3">
        <a href="{{URL::to('/admin/companies')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد فروشنده ها&nbsp<i class="fa fa-chevron-left"></i>
                                        </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['companies']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{URL::to('/admin/producers')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد تولیدکنندگان&nbsp<i class="fa fa-chevron-left"></i>
                                        </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['producers']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{URL::to('/admin/importers')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد وارد کنندگان&nbsp<i class="fa fa-chevron-left"></i>
                                        </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['importers']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{URL::to('/admin/technicians')}}">
            <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                <div class="panel-heading top-left top-right ">
                    <div class="panel-title text-black hint-text">
                        <span class="fs-11 all-caps">تعداد تکنسین ها&nbsp<i class="fa fa-chevron-left"></i>
                    </span>
                    </div>
                </div>
                <div class="panel-body p-t-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <h4 class="no-margin p-b-5 text-danger semi-bold">{{$stat['technicians']}} نفر</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
@endsection