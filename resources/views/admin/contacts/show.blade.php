@extends('layouts.dashboard') @section('header') @endsection @section('content')
@php
$type = [
    'contact' => 'ارتباط با ما',
    'grievance' => 'شکایت از کسب و کار',
];
@endphp
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
          <a href={{URL::to('/admin/contacts')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>نام : </label>
              <span>{{$contact->name}}</span>
            </div>
            <div class="form-group">
              <label>ایمیل : </label>
              <span>{{$contact->email}}</span>
            </div>

              @php
                if(!empty($contact->file)):
              @endphp
                <a href="{{$contact->file}}" class="btn btn-primary" target="_blank">دانلود ضمیمه</a>
                <br><br>
              @php
                endif;
              @endphp
          </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>موبایل : </label>
            <span>{{$contact->mobile}}</span>
          </div>


          <div class="form-group">
            <label>نوع پیام : </label>
            <span>{{$type[$contact->type]}}</span>
          </div>
        </div>
        <br><br><br>
        <div class="col-md-12">
        <div class="form-group">
            <label>متن پیام : </label>
            <p>{{$contact->description}}</p>
          </div>
        </div>
      </div>
    </div>
    
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer') @endsection