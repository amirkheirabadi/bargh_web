@extends('layouts.auth')

@section('content')
    <p class="p-t-35">ورود مدیر سیستم</p>
    <!-- START Login Form -->
    <form id="form-login" class="p-t-15" method="post" role="form" action="{{URL::to('/admin/auth')}}">
      <!-- START Form Control-->
      <div class="form-group form-group-default">
        <label>نام کاربری</label>
        <div class="controls">
          <input required type="text" name="email" placeholder="ایمیل" class="form-control" required>
        </div>
      </div>
      <!-- END Form Control-->
      <!-- START Form Control-->
      <div class="form-group form-group-default">
        <label>رمز عبور</label>
        <div class="controls">
          <input  required type="password" class="form-control" name="password" placeholder="رمز عبور" required>
        </div>
      </div>
      <br>  
        <div class="row">
        <div class="col-md-6">
          <a  class="btn btn-primary btn-cons m-t-10" href="{{URL::to('/admin/auth/forget')}}">فراموشی رمز عبور</a>
        </div>
        <div class="col-md-6">
          <button class="btn btn-primary btn-cons m-t-10" type="submit"  style="float:right">ورود به سیستم</button>
        </div>

        </div>
      <br>     
    </form>
    <!--END Login Form-->
@endsection
