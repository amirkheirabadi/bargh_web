@extends('layouts.auth')

@section('content')
    <p class="p-t-35">ورود مدیر سیستم</p>
    <!-- START Login Form -->
    <form id="form-login" class="p-t-15" method="post" role="form">
      <!-- START Form Control-->
       <div class="form-group form-group-default">
        <label>رمز عبور</label>
        <div class="controls">
          <input type="password" required class="form-control" name="password" placeholder="رمز عبور" required>
        </div>
      </div>
      <!-- END Form Control-->
      <!-- START Form Control-->
      <div class="form-group form-group-default">
        <label>تکرار رمز عبور</label>
        <div class="controls">
          <input type="password" required class="form-control" name="password_confirmation" placeholder="تکرار رمز عبور" required>
        </div>
      </div>
      <button class="btn btn-primary btn-cons m-t-10" type="submit">تغییر رمز عبور</button>
    </form>
    <!--END Login Form-->
@endsection
