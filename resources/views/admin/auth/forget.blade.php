@extends('layouts.auth')

@section('content')
    <p class="p-t-35">ورود مدیر سیستم</p>
    <!-- START Login Form -->
    <form id="form-login" class="p-t-15" method="post" role="form" action="{{URL::to('/admin/auth/forget')}}">
      <!-- START Form Control-->
      <div class="form-group form-group-default">
        <label>ایمیل</label>
        <div class="controls">
          <input type="text" required name="email" placeholder="ایمیل" class="form-control" required>
        </div>
      </div>
      <!-- END Form Control-->
      <!-- START Form Control-->

      <button class="btn btn-primary btn-cons m-t-10" type="submit">ارسال</button>
    </form>
    <!--END Login Form-->
@endsection
