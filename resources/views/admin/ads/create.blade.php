@extends('layouts.dashboard') 
@section('header') 
@endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data">
          <div class="row">
              <div class="col-md-6">

              <div class="form-group">
                <label>نام</label>
                <input type="text" name="name" class="form-control" value={{$ads->name}}>
              </div>
              <br><br>

              <div class="form-group">
                <label>لینک اینترنتی تبلیغ</label>
                <input type="text" name="url" class="form-control" value={{$ads->url}}>
              </div>
              <br><br>

              <div class="form-group">
                <label>محل نمایش</label>
                <select name="type" class="form-control" value={{$ads->type}}>
                @php
                  foreach(['home'=> 'صفحه اصلی','company' => 'فروشنده / فروشگاه','producer' => 'تولید کننده','importer' => 'وارد کننده','technician' => 'تکنسین'] as $typeKey => $typeValue) :
                    if($typeKey == $ads->type):
                @endphp
                  <option value="{{$typeKey}}" selected>{{$typeValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$typeKey}}">{{$typeValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>

               <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value={{$ads->status}}>
                @php
                  foreach(['publish'=> 'نمایش', 'pending' => 'عدم نمایش'] as $statusKey => $statusValue) :
                    if($statusKey == $ads->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
                </select>
              </div>
              <br>

              <br><br>
              </div>
              <div class="col-md-6">
            <div class="form-group">
              @php
                if(!empty($ads->picture)):
              @endphp
                <img src="{{$ads->picture}}" alt="" width=300>
                <br><br>
              @php
                endif;
              @endphp

              <label>آپلود تبلیغ</label>
              <input type="file" name="picture" class="form-control">
            </div>
            <br>
            <br>    
          </div>
       
          </div>

          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/ads')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection 
  @section('footer')
     
  @endsection