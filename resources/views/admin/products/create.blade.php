@extends('layouts.dashboard') @section('header')
<link href="/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" /> @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>نام</label>
                <input type="text" name="name" class="form-control" value="{{$product->name}}">
              </div>
              <br>
              <div class="form-group">
                <label>توضیحات</label>
                <textarea name="description" class="form-control">{{$product->description}}</textarea>
              </div>
              <br><br>
              <div class="form-group">
                <label>نوع قیمت</label>
                <select name="price_type" id="price_type" class="form-control" value="{{$product->price_type}}">
                @php
                  foreach(['notknow'=> 'بدون قیمت', 'relative' => 'متغیر','absolute' => 'ثابت'] as $statusKey => $statusValue) :
                    if($statusKey == $product->price_type):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>

              <div class="form-group price_types" id="absolute" style="display:none">
                <label>قیمت ثابت</label>
                <input type="text" name="price" class="form-control" value={{$product->price}}>
              </div>


              <div class="price_types" id="relative" style="display:none">
                <div class="form-group">
                  <label>شروع از</label>
                  <input type="text" name="price" class="form-control" value={{$product->price}}>
                </div>
                <div class="form-group">
                  <label>تا پایان</label>
                  <input type="text" name="price_to" class="form-control" value={{$product->price_to}}>
                </div>
              </div>
              <br>

              <div class="form-group">
                <label>تاریخ اعتبار قیمت</label>
                <input type="text" name="price_expire" class="form-control" value={{$product->price_expire}}>
              </div>

            </div>
            <div class="col-md-6">
              <div class="form-group">
                @php if(!empty($product->thumb)): @endphp
                <img src="{{$product->thumb}}_normal.jpg" alt="" width=100>
                <br><br> @php endif; @endphp

                <label>آپلود تصویر بند انگشتی</label>
                <input type="file" name="thumb" class="form-control">
              </div>
              <br>
              <div class="form-group">
                <label>انتخاب دسته بندی</label>
                <select name="category_id" class="form-control" dir="rtl" id="category">
              </select>
              </div>
              <br>
              <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value={{$product->status}}>
                @php
                  foreach(['active'=> 'فعال', 'suspend' => 'مسدود','pending' => 'در حالت انتظار'] as $statusKey => $statusValue) :
                    if($statusKey == $product->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
              <br>
            </div>
          </div>
          <br><br>
          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to( '/admin/products')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')
  <script type="text/javascript" src="/backend/plugins/select2/dist/js/select2.min.js"></script>
  <script type="text/javascript" src="/backend/plugins/select2/fa.js"></script>
  <script>
    var parent = $('#category').select2({
      dir: "rtl",
      language: "fa",
      ajax: {
        url: "/admin/categories/get",
        dataType: 'json',
        method: 'post',
        delay: 250,
        data: function (params) {
          return {
            name: params.term, // search term
          };
        },
        processResults: function (data, params) {
          var results = data.items;
          results.unshift({
            id: 0,
            text: 'دسته بندی اصلی'
          });
          return {
            results: results,
          };
        },
        cache: true,
      },
    });

    @php
    if (!empty($category)):
      @endphp
    parent.append('<option value="{{$category->id}}" selected="selected">{{$category->name}}</option>');
    parent.trigger('change');
    @php
    endif;
    @endphp

    var change = function (type) {
      switch (type) {
        case "notknow":
          $('.price_types').css('display', 'none');
          break;
        case "relative":
          $('.price_types').css('display', 'none');
          $('#relative').css('display', 'block');
          break;
        case "absolute":
          $('.price_types').css('display', 'none');
          $('#absolute').css('display', 'block');
          break;
      }
    }
    var type = "{{$product->price_type}}";
    change(type);

    $("#price_type").on("change", function () {
      change($(this).val());
    });
  </script>
  @endsection