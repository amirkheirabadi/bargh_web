@extends('layouts.dashboard') 
@section('header') 
    <link href="/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
@endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data">
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                <label>نام</label>
                <input type="text" name="name" class="form-control" value="{{$technician->name}}">
              </div>
              <br>
              <div class="form-group">
                <label>شماره موبایل</label>
                <input type="text" name="mobile" class="form-control" value="{{$technician->mobile}}">
              </div>
              <br>
              <div class="form-group">
                <label>شماره تلفن</label>
                <input name="phone" class="form-control" value="{{$technician->phone}}">
              </div>
              <br>
              
              <div class="form-group">
                <label>تجربه کاری</label>
                <textarea name="experience" class="form-control">{{$technician->experience}}</textarea>
              </div>
              <br><br>

              <div class="form-group">
                <label>توضیحات</label>
                <textarea name="description" class="form-control">{{$technician->description}}</textarea>
              </div>
              <br><br>

  
              </div>

              <div class="col-md-6">

                <div class="row">

                  <div class="col-md-6">
                       <div class="form-group">
                        <label>انتخاب استان</label>
                        <select class="form-control"  dir="rtl" id="province_input">
                        @php
                             foreach($provinces as $pr) :
                                if($pr->id == $province->id):
                            @endphp
                              <option value="{{$pr->id}}" selected>{{$pr->name}}</option>
                            @php
                                else:
                            @endphp
                              <option value="{{$pr->id}}">{{$pr->name}}</option>
                            @php
                                endif;
                              endforeach;
                            @endphp
                        </select>
                      </div>
                  </div>

                  <div class="col-md-6">
                   <div class="form-group">
                    <label>انتخاب شهر</label>
                    <select name="city_id" class="form-control"   dir="rtl" id="city_input">
                      <option value="{{$city->id}}" selected> {{$city->name}}</option>
                    </select>
                  </div>
                  </div>
                </div>
                <br>


            <div class="form-group">
              @php
                if(!empty($technician->avatar)):
              @endphp
                <img src="{{$technician->avatar}}" alt="" width=100>
                <br><br>
              @php
                endif;
              @endphp


                <label>تصویر آواتار</label>
                <input type="file" name="avatar" class="form-control">
                <br>
                <div class="alert alert-info">
                فرمت های قابل پذیریش : jpg,png,jpeg | حداکثر حجم فایل ؛ 2 مگابابت
            </div>
            </div>
            <br>
  
            <div class="form-group">
              @php
                if(!empty($technician->catalog)):
              @endphp
                <a href="{{$technician->catalog}}" class="btn btn-primary">دانلود رزومه</a>
                <br><br>
              @php
                endif;
              @endphp

              <label>آپلود رزومه</label>
              <input type="file" name="catalog" class="form-control">
            </div>

            <br>
            
            <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value="{{$technician->status}}">
                @php
                  foreach(['active'=> 'فعال', 'suspend' => 'مسدود','pending' => 'در حالت انتظار'] as $statusKey => $statusValue) :
                    if($statusKey == $technician->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
            
              <div class="form-group">
                <label>کسب و کار برجسته</label>
                <select name="highlight" class="form-control" value="{{$technician->highlight}}">
                @php
                  foreach(['no'=> 'خیر', 'yes' => 'بلی'] as $statusKey => $statusValue) :
                    if($statusKey == $technician->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
          </div>

        
          <div class="col-md-12">
            <h5>آدرس :</h5>
          </div>

          <div class="col-md-6">
               <div class="row">
                  <div class="col-md-6">
                       <div class="form-group">
                        <label>انتخاب استان</label>
                        <select name="address_province" class="form-control"  dir="rtl" id="address_province_input">
                        @php
                             foreach($provinces as $pr) :
                                if($pr->id == $province->address_province_id):
                            @endphp
                              <option value="{{$pr->id}}" selected>{{$pr->name}}</option>
                            @php
                                else:
                            @endphp
                              <option value="{{$pr->id}}">{{$pr->name}}</option>
                            @php
                                endif;
                              endforeach;
                            @endphp
                        </select>
                      </div>
                  </div>

                  <div class="col-md-6">
                   <div class="form-group">
                    <label>انتخاب شهر</label>
                    <select name="address_city" class="form-control"   dir="rtl" id="address_city_input">
                      <option value="{{$address_city->id}}" selected> {{$address_city->name}}</option>
                    </select>
                  </div>
                  </div>
                </div>
                <br>
  
                <div class="form-group">
                <label>آدرس</label>
                <textarea name="address" class="form-control">{{$technician->address->address}}</textarea>
              </div>
              <br><br>                
          </div>

          <div class="col-md-6">
                <div id="google-map" style="height:400px"></div>
          </div>

          </div>
          <br><br>
          <input type="hidden" name="lat" id="lat" value="{{$technician->address->lat}}">
          <input type="hidden" name="long" id="long" value="{{$technician->address->long}}">

          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/technicians')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>

        <br><br>
        <h5>لیست تخصص ها</h5>
        <br>
        <form action="/admin/technicians/skill/{{$technician->id}}" method="post">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>انتخاب تخصص</label>
                <select name="skill" class="form-control" dir: "rtl" id="skills">
                        </select>
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>برجسته</label>
                <select name="highlight" class="form-control" dir="rtl" id="highlight">
                  <option value="yes">بلی</option>  
                  <option value="no">خیر</option>  
                </select>
              </div>
            </div>
            
            <div class="col-md-2"><br>
              <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
              <br>
            </div>
          </div>
        </form>
          <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>
                نام تخصص
              </th>
              <ht>
                برجسته
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @php foreach($technician->skills as $skill): @endphp
            <tr>
              <td>
                {{$skill->name}}
              </td>
              <td>
                @if($skill->pivot->highlight == 'yes') 
                  <a class="highlight-button skill-active" id="{{$skill->id}}"><i class="fa fa-star"></i></a>
                @else
                  <a class="highlight-button skill-deactive" id="{{$skill->id}}"><i class="fa fa-star-o"></i></a>
                @endif
              </td>
              <td>
                <div class="btn-group">
                  <a href="/admin/technicians/skill/{{$technician->id}}?id={{$skill->pivot->id}}" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                            </a>
                </div>
              </td>
            </tr>
            @php endforeach; @endphp
          </tbody>
        </table>

      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')
      <script type="text/javascript" src="/backend/plugins/select2/dist/js/select2.min.js"></script>
      <script type="text/javascript" src="/backend/plugins/select2/fa.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBQYpxBpSu2pjJ4mqFO6EyPtUEa20C8vZg" type="text/javascript"></script>
      <script>
        var province = <?= $province->id ?>;
        var address_province = <?= $province->id ?>;
  
        var province_select = $('#province_input').select2({
              dir: "rtl",
              language: "fa",
         });
        var city_select = $('#city_input').select2({
          dir: "rtl",
          language: "fa",
          ajax: {
            url: "/api/cities",
            dataType: 'json',
            method: 'post',
            delay: 250,
            data: function (params) {
              return {
                province: province,
                name: params.term, // search term
              };
            },
            processResults: function (data, params) {
              result = [];
              data.data.forEach(function(pr) {
                result.push({
                  id: pr.id,
                  text: pr.name
                });
              });
              return {
                results: result,
              };
            },
            cache: true
          },
        });
        province_select.on("select2:select", function (e) { province = e.params.data.id; });



      var address_province_select = $('#address_province_input').select2({
              dir: "rtl",
              language: "fa",
         });
      $('#address_city_input').select2({
          dir: "rtl",
          language: "fa",
          ajax: {
            url: "/api/cities",
            dataType: 'json',
            method: 'post',
            delay: 250,
            data: function (params) {
              return {
                province: address_province,
                name: params.term, // search term
              };
            },
            processResults: function (data, params) {
              result = [];
              data.data.forEach(function(pr) {
                result.push({
                  id: pr.id,
                  text: pr.name
                });
              });
              return {
                results: result,
              };
            },
            cache: true
          },
        });
        address_province_select.on("select2:select", function (e) { address_province = e.params.data.id; });

         var skill_input = $('#skills').select2({
          dir: "rtl",
          language: "fa",
          ajax: {
            url: "/admin/skills/get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
              };
            },
            processResults: function (data, params) {
              return {
                results: data.items,
              };
            },
            cache: true
          },
        });

        @php
          if(!empty($skills) && sizeof($skills)) :
          foreach($skills as $skill):
        @endphp
            skill_input.append('<option value="{{$skill->id}}" selected="selected">{{$skill->name}}</option>');
          skill_input.trigger('change');
        @php
          endforeach;
          endif;
        @endphp
    
(function($) {
    'use strict';
    google.maps.event.addDomListener(window, 'load', init);
    var map;
    var zoomLevel = 11;
    var myLatlng = new google.maps.LatLng({{$technician->address->lat}}, {{$technician->address->long}});
    function init() {
        var mapOptions = {
            zoom: zoomLevel,
            center: myLatlng,
           
        };
        var mapElement = document.getElementById('google-map');
        map = new google.maps.Map(mapElement, mapOptions);
        
        var marker = new google.maps.Marker({
            position: myLatlng, 
            map: map,
            draggable:true
        });

   google.maps.event.addListener(
    marker,
    'drag',
    function() {
      console.log(marker.position.lat())
        $('#lat').val( marker.position.lat());
        $('#long').val(marker.position.lng());
    }
);
    }
})(window.jQuery);

$(document).on('click', '.skill-active', function() {
      var _this = this;
      $.ajax({
        url :'/admin/technicians/highlight/skill/{{$technician->id}}/' + $(this).attr('id') + '/no'
      }).success(function() {
        setTimeout(function() {
          $(_this).removeClass('skill-active').addClass('skill-deactive').find('i').removeClass('fa-star').addClass('fa-star-o');
        }, 50);
      });
    });

    $(document).on('click', '.skill-deactive', function() {
      var _this = this;
      $.ajax({
        url :'/admin/technicians/highlight/skill/{{$technician->id}}/' + $(this).attr('id') + '/yes'
      }).success(function() {
        setTimeout(function() {
           $(_this).removeClass('skill-deactive').addClass('skill-active').find('i').removeClass('fa-star-o').addClass('fa-star');
        }, 50);
      });
    });
      </script>
  @endsection