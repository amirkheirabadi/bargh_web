@extends('layouts.dashboard')

@section('header')
    <link href="/backend/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('content')
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
          <div class="inner" style="transform: translateY(0px); opacity: 1;">
            <h4 class="">{{$title}}</h4>
          </div>
        </div>
      </div>

      <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg bg-white">
        <!-- START PANEL -->
        <div class="panel panel-transparent">
          <div class="panel-heading">
            <div class="panel-title">
            </div>
            <div class="export-options-container pull-right"></div>
            <div class="clearfix"></div>
          </div>
          <div class="panel-body">
            <br><br>
            <table class="table table-striped" id="mainTable">

            </table>
          </div>
        </div>
        <!-- END PANEL -->
      </div>
      <!-- END CONTAINER FLUID -->
@endsection

@section('footer')
    <script src="/backend/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="/backend/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="/backend/plugins/datatables-responsive/js/lodash.min.js"></script>

    <script src="/backend/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/backend/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/backend/datatable/jszip.min.js" type="text/javascript"></script>
    <script src="/backend/datatable/vfs_fonts.js" type="text/javascript"></script>
    <script src="/backend/datatable/buttons.html5.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    var settings = {
        "pProcessing": true,
        "bFilter": true,
		"bServerSide": true,
        "scrollCollapse": true,
        "aLengthMenu": [[10, 25, 50, 100000], [10, 25, 50, "همه"]],
        "oLanguage": {
            "sUrl": "{{URL::to('/backend/plugins/jquery-datatable/persian.js')}}"
        },
        "iDisplayLength": 10,
        "ajax": {
            'type': 'post',
            'url': "{{URL::to('/admin/technicians')}}"
        },
        "aoColumns": [
            { "mData": "id",  "aTargets": 0 ,'sTitle':'شناسه'},
            { "mData": "name", "aTargets": 1 ,'sTitle':'نام تکنسین'},
            { "mData": "user", "aTargets": 2 ,'sTitle':'کاربر'},
            { "mData": "created_at", "aTargets": 3 ,'sTitle':'تاریخ ایجاد'},
            { "mData": "status", "aTargets": 4 ,'sTitle':'وضعیت'},
            { "mData": "action",  "aTargets": 5 ,'sortable':false},
        ],
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                 columns: ':contains("Office")'
                },
                text: "کپی در کلیپ بورد",
                "className": 'btn btn-primary'
            },
            {
                extend: 'excelHtml5',
                text: "خروجی اکسل",
                "className": 'btn btn-primary'
            },
             {
                extend: 'csvHtml5',
                text: "خروجی CSV",
                "className": 'btn btn-primary'
            },
        ]
    };

    $('#mainTable').dataTable(settings);

    $(document).on('click','.btn-delete', function () {
        return confirm('از انجام این عملیات اطمینان دارید ؟');
    });
    </script>
@endsection
