@php
use App\Helper\Web;
@endphp
@extends('layouts.dashboard') @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
      <a href={{URL::to('/admin/messages')}} class="btn btn-primary btn-cons">بازگشت</a>
      <br><br>
    @foreach($messages as $msg)
    @php
      $date = Web::jalali($msg);
    @endphp
    <div class="row">
      @if ($msg->user_sender == $message->user_sender)
      <div class="col-md-6">
        <div class="panel panel-default bg-complete" data-pages="portlet">
          <div class="panel-heading ui-sortable-handle">
              <div class="panel-title">ارسال کننده : {{$msg->sender->first_name . ' ' . $msg->sender->last_name}} | تاریخ ارسال : {{$date['created_at']['ago']}}
              </div>
          </div>
          <div class="panel-body">
          <form method="post">
          @if($msg->id == $message->id)
                <div class="form-group">
                  <label>متن پیام</label>
                  <textarea name="text" class="form-control" >{{$msg->text}}</textarea>
                </div>
            @else
              <p class="text-white">{{$msg->text}}</p>
            @endif
            
            @if($msg->id == $message->id)
              <button type "submit" class="btn btn-primary btn-cons">ثبت</button>
            @else
              <a href="{{URL::to('/admin/messages/edit/'. $msg->id)}}"  class="btn btn-primary btn-cons">ویرایش دیدگاه</a>
            @endif
            <a href="{{URL::to('/admin/messages/delete/'. $msg->id)}}" class="btn btn-primary btn-cons btn-delete">حذف دیدگاه</a>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-6"></div>
      @else
      <div class="col-md-6"></div>
      <div class="col-md-6">
        <div class="panel panel-default bg-success" data-pages="portlet">
          <div class="panel-heading ui-sortable-handle">
            <div class="panel-title">ارسال کننده : {{$msg->sender->first_name . ' ' . $msg->sender->last_name}} | تاریخ ارسال : {{$date['created_at']['ago']}}
            </div>
          </div>
          <div class="panel-body">
            <form method="post">
          @if($msg->id == $message->id)
                <div class="form-group">
                  <label>متن پیام</label>
                  <textarea name="text" class="form-control" >{{$msg->text}}</textarea>
                </div>
            @else
              <p class="text-white">{{$msg->text}}</p>
            @endif
            
            @if($msg->id == $message->id)
            <button type "submit" class="btn btn-primary btn-cons">ثبت</button>
            @else
              <a href="{{URL::to('/admin/messages/edit/'. $msg->id)}}"  class="btn btn-primary btn-cons">ویرایش دیدگاه</a>
            @endif
            <a href="{{URL::to('/admin/messages/delete/'. $msg->id)}}" class="btn btn-primary btn-cons btn-delete">حذف دیدگاه</a>
            </form>
          </div>
        </div>
      </div>
      @endif
    </div>
    @endforeach
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection

  @section('footer')

<script>
    $(document).on('click','.btn-delete', function () {
        return confirm('از انجام این عملیات اطمینان دارید ؟');
    });
</script>
@endsection