@extends('layouts.dashboard') @section('header')
<link href="/backend/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen"> @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form rule="form" method="post" enctype="multipart/form-data" id="main_form">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>فعال سازی سرویس های اشتراک و پولی</label>
                <select name="commercial" class="form-control" value={{$data["commercial"]}}>
                @php
                  foreach(['1'=> 'بلی', '0' => 'خیر'] as $typeKey => $typeValue) :
                    if($typeKey == $data["commercial"]):
                @endphp
                  <option value="{{$typeKey}}" selected>{{$typeValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$typeKey}}">{{$typeValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
            </div>
          <br><br>
          <div class="col-md-12">
            <h5>قوانین وب سایت</h5>
            <div class="summernote-wrapper">
              <textarea name="rule" id="summernote">{!!$data['rule']!!}</textarea>
            </div>
          </div>
          <br><br>

        <div class="col-md-12">
            <h5>متن صفحه درباره ما</h5>
            <div class="summernote-wrapper">
              <textarea name="aboutus" id="summernote_aboutus">{!!$data['aboutus']!!}</textarea>
            </div>
          </div>
          <br><br>


          </div>
          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/settings')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')
  <script src="/backend/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
  <script src="/backend/summer_note_lang.js" type="text/javascript"></script>


  <script>
    $(document).ready(function () {
      function sendFile(file, el) {
      data = new FormData();
      data.append("file", file);
      $.ajax({
        data: data,
        type: "POST",
        url: "{{URL::to('/admin/upload')}}",
        cache: false,
        contentType: false,
        processData: false,
        success: function (url) {
          $(el).summernote('editor.insertImage', url);
        }
      });
    }

      $('#summernote').summernote({
        lang: "fa-IR",
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {
              sendFile(files[0], this);
            },
          },
        height: 200,
      });

       $('#summernote_aboutus').summernote({
        lang: "fa-IR",
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {
              sendFile(files[0], this);
            },
          },
        height: 200,
      });
    });


  </script>
  @endsection