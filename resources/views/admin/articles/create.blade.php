@extends('layouts.dashboard') @section('header')
<link href="/backend/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen"> @endsection @section('content')

<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" id="main_form" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>عنوان</label>
                <input type="text" name="title" class="form-control" value={{$news->title}}>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                @php if(!empty($news->thumb)): @endphp
                <img src="{{$news->thumb}}" alt="" width=100>
                <br><br> @php endif; @endphp

                <label>آپلود تصویر بند انگشتی</label>
                <input type="file" name="thumb" class="form-control">
              </div>
              <br>
              <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value={{$news->status}}>
                @php
                  foreach(['publish'=> 'منتشر شده', 'pending' => 'در انتظار انتشار'] as $statusKey => $statusValue) :
                    if($statusKey == $news->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <h5>متن مقاله</h5>
            <div class="summernote-wrapper">
              <textarea name="text" id="summernote">{!!$news->text!!}</textarea>
            </div>
          </div>
          <br><br>
          <button type="submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/articles')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')
   <script src="/backend/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
  <script src="/backend/summer_note_lang.js" type="text/javascript"></script>

  <script>
    $(document).ready(function () {

    function sendFile(file, el) {
      data = new FormData();
      data.append("file", file);
      $.ajax({
        data: data,
        type: "POST",
        url: "{{URL::to('/admin/upload')}}",
        cache: false,
        contentType: false,
        processData: false,
        success: function (url) {
          $(el).summernote('editor.insertImage', url);
        }
      });
    }

      $('#summernote').summernote({
          lang: "fa-IR",
          toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph', 'height']],
            ['insert', ['hr', 'picture', 'video', 'link']],
            ['misc', ['fullscreen', 'undo', 'redo']]
          ],
          callbacks: {
            onImageUpload: function (files, editor, welEditable) {
              sendFile(files[0], this);
            },
          },
          fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
          height: 200,
        });
      });
  </script>
  @endsection