@extends('layouts.dashboard') @section('header') @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data">
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                <label>نام</label>
                <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}">
              </div>
              <br>
              <div class="form-group">
                <label>نام خانوادگی</label>
                <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}">
              </div>
              <br>
              <div class="form-group">
                <label>شماره موبایل</label>
                <input type="text" name="mobile" class="form-control" value="{{$user->mobile}}">
              </div>
              <br>
              <div class="form-group">
                <label>ایمیل</label>
                <input type="email" name="email" class="form-control" value="{{$user->email}}">
              </div>
              <br><br>
              
              <div class="form-group">
                <label>رمز عبور</label>
                <input type="password" name="password" class="form-control">
              </div>
              <br>
              <div class="form-group">
                <label>تکرار رمز عبور</label>
                <input type="password" name="password_confirmation" class="form-control">
              </div>
              <br><br>


              <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value="{{$user->status}}">
                @php
                  foreach(['active'=> 'فعال', 'suspend' => 'مسدود', 'pending' => 'در انتظار تایید'] as $statusKey => $statusValue) :
                    if($statusKey == $user->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  @php if(!empty($user->avatar)): @endphp
                  <img src="{{$user->avatar}}" alt="" width=100>
                  <br><br> @php endif; @endphp

                  <label>آپلود آواتار</label>
                  <input type="file" name="avatar" class="form-control">
                  <br>
                  <div class="alert alert-info">
                    فرمت های قابل پذیریش : jpg,png,jpeg | حداکثر حجم فایل ؛ 2 مگابابت
                  </div>
                </div>


                @if(!empty($user->id))
                  <div class="form-group">
                    <label>تاریخ ثبت نام</label>
                    <br>
                    <span>{{$user->date['created_at']['time']}}</span>
                  </div>
                @endif
                <br>
                @if(!empty($user->id))
                  <div class="form-group">
                    <label>تاریخ آخرین ویرایش پروفایل</label>
                    <br>
                    <span>{{$user->date['updated_at']['ago']}}</span>
                  </div>
                @endif
              </div>
          </div>
          <br>
          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/users')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')

  @endsection