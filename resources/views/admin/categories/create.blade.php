@extends('layouts.dashboard') @section('header')
<link href="/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/backend/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen"> @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data" id="main_form">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>نام</label>
                <input type="text" name="name" class="form-control" value={{$category->name}}>
              </div>
              <br>
              <div class="form-group">
                <label>نوع</label>
                <select name="type" class="form-control" value={{$category->type}}>
                @php
                  foreach(['brand'=> 'برند', 'product' => 'محصول'] as $typeKey => $typeValue) :
                    if($typeKey == $category->type):
                @endphp
                  <option value="{{$typeKey}}" selected>{{$typeValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$typeKey}}">{{$typeValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
              <div class="form-group">
                <label>وضعیت</label>
                <select name="verified" class="form-control" value={{$category->verified}}>
                @php
                  foreach(['yes'=> 'تایید شده', 'no' => 'تایید نشده'] as $typeKey => $typeValue) :
                    if($typeKey == $category->verified):
                    @endphp
                      <option value="{{$typeKey}}" selected>{{$typeValue}}</option>
                    @php
                        else:
                    @endphp
                      <option value="{{$typeKey}}">{{$typeValue}}</option>
                    @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
              <div class="form-group">
                <label>دسته بندی پدر</label>
                <select name="parent" class="form-control" value="{{$category->parent}}" dir: "rtl" id="parent_input">
              </select>
              </div>
              <br>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                @php if(!empty($category->image)): @endphp
                <img src="{{$category->image}}" alt="" width=100>
                <br><br> @php endif; @endphp

                <label>آپلود تصویر</label>
                <input type="file" name="image" class="form-control">
              </div>
            </div>
            <br><br>


            <div class="col-md-12">
            <h5>توضیحات</h5>
            <div class="summernote-wrapper">
              <textarea name="description" id="summernote">{!!$category->description!!}</textarea>
            </div>
          </div>
          <br><br>
          </div>
          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/categories')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')

  <script type="text/javascript" src="/backend/plugins/select2/dist/js/select2.min.js"></script>
  <script type="text/javascript" src="/backend/plugins/select2/fa.js"></script>
  <script src="/backend/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
  <script src="/backend/summer_note_lang.js" type="text/javascript"></script>

  <script>
    var parent = $('#parent_input').select2({
      dir: "rtl",
      language: "fa",
      ajax: {
        url: "/admin/categories/get",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
          };
        },
        processResults: function (data, params) {
          var results = data.items;
          results.unshift({
            id: 0,
            text: 'دسته بندی اصلی'
          });
          return {
            results: results,
          };
        },
        cache: true
      },
    });

    @php
    if (!empty($parent)):
      @endphp
    parent.append('<option value="{{$parent->id}}" selected="selected">{{$parent->name}}</option>');
    parent.trigger('change');
    @php
    endif;
    @endphp
  </script>


  <script>
    $(document).ready(function () {
    function sendFile(file, el) {
      data = new FormData();
      data.append("file", file);
      $.ajax({
        data: data,
        type: "POST",
        url: "{{URL::to('/admin/upload')}}",
        cache: false,
        contentType: false,
        processData: false,
        success: function (url) {
          $(el).summernote('editor.insertImage', url);
        }
      });
    }

      $('#summernote').summernote({
        lang: "fa-IR",
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['insert', ['hr', 'picture']],
          ['misc', ['fullscreen', 'undo', 'redo']]
        ],
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {
              sendFile(files[0], this);
            },
          },
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
        height: 200,
      });
    });
  </script>
  @endsection