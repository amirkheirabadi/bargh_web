@extends('layouts.dashboard') @section('header') @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>کاربر :</label>
                <a href="/admins/users/{{$transaction->owner->id}}"><span>{{$transaction->owner->first_name}}</span></a>
              </div>
              <br>
              <div class="form-group">
                <label>مدت زمان ( روز )</label>
                <input type="text" name="duration" class="form-control" value={{$plan->duration}}>
              </div>
              <br>
              <div class="form-group">
                <label>قیمت ( تومان )</label>
                <input type="text" name="price" class="form-control" value={{$plan->price}}>
              </div>
              <br>
            </div>
            <br><br>
          </div>
          <a href={{URL::to('/admin/transactions')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer') @endsection