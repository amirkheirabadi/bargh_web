@extends('layouts.dashboard') @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner" style="transform: translateY(0px); opacity: 1;">
            <h4 class="">{{$title}}</h4>
        </div>
    </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
    <!-- START PANEL -->
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
            </div>
            <div class="export-options-container pull-right"></div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <form action="{{URL::to('/admin/pictures/create/' . $entity . '/' . $entity_id)}}" method="post"  enctype="multipart/form-data">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="file" name="picture" class="form-control">
                    </div>
                    <br>
                    <button type "submit" class="btn btn-complete btn-cons">آپلود تصویر</button>
                </div>
            </form>
            <br><br>
            <table class="table table-striped" id="mainTable">
                <thead>
                    <tr>
                        <td>تصویر</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pictures as $picture)
                    <tr>
                        <td>
                            <img src="{{$picture->url}}" alt="" width="100" height="100">
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="{{URL::to('/admin/pictures/delete/' . $entity . '/' . $entity_id . '/' . $picture->id)}}" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END CONTAINER FLUID -->

@endsection

@section('footer')

<script>
    $(document).on('click','.btn-delete', function () {
        return confirm('از انجام این عملیات اطمینان دارید ؟');
    });
</script>
@endsection