@extends('layouts.dashboard') @section('header')
<link href="/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/backend/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen"> @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" id="main_form" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>نام فارسی</label>
                <input type="text" name="name_fa" class="form-control" value={{$brand->name_fa}}>
              </div>
              <br>
              <div class="form-group">
                <label>نام انگلیسی</label>
                <input type="text" name="name_en" class="form-control" value={{$brand->name_en}}>
              </div>
              <br>
              <div class="form-group">
                <label>برجسته</label>
                <select name="famous" class="form-control" value={{$brand->famous}}>
                @php
                  foreach(['yes'=> 'بله', 'no' => 'خیر'] as $famousKey => $famousValue) :
                    if($famousKey == $brand->famous):
                @endphp
                  <option value="{{$famousKey}}" selected>{{$famousValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$famousKey}}">{{$famousValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
              <div class="form-group">
                <label>وضعیت</label>
                <select name="verified" class="form-control" value={{$brand->verified}}>
                @php
                  foreach(['yes'=> 'تایید شده', 'no' => 'تایید نشده'] as $typeKey => $typeValue) :
                    if($typeKey == $brand->verified):
                    @endphp
                      <option value="{{$typeKey}}" selected>{{$typeValue}}</option>
                    @php
                        else:
                    @endphp
                      <option value="{{$typeKey}}">{{$typeValue}}</option>
                    @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>دسته بندی های برند</label>
                <select name="category[]" class="form-control" dir="rtl" id="category" multiple>
              </select>
              </div>
              <br><br>

              <div class="form-group">
                @php if(!empty($brand->logo)): @endphp
                <img src="{{$brand->logo}}" alt="" width=100>
                <br><br> @php endif; @endphp

                <label>آپلود تصویر</label>
                <input type="file" name="logo" class="form-control">
              </div>
            </div>

            <div class="col-md-12">
              <h5>توضیحات</h5>
              <div class="summernote-wrapper">
                <textarea name="description" id="summernote">{!!$brand->description!!}</textarea >
              </div>
            </div>
          </div>
          <br>
      </div>
      <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
      <a href={{URL::to('/admin/brands')}} class="btn btn-primary btn-cons">بازگشت</a>
      <br><br>
      </form>
    </div>
  </div>
  <!-- END PANEL -->
</div>
<!-- END CONTAINER FLUID -->
@endsection @section('footer')

<script type="text/javascript" src="/backend/plugins/select2/dist/js/select2.min.js"></script>
<script type="text/javascript" src="/backend/plugins/select2/fa.js"></script>
<script src="/backend/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
<script src="/backend/summer_note_lang.js" type="text/javascript"></script>

<script>
  var categories = $('#category').select2({
    dir: "rtl",
    language: "fa",
    ajax: {
      url: "/admin/categories/get",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
        };
      },
      processResults: function (data, params) {
        return {
          results: data.items,
        };
      },
      cache: true
    },
  });

  @php
  if (!empty($categories) && sizeof($categories)):
    foreach($categories as $category):
    @endphp
  categories.append('<option value="{{$category->id}}" selected="selected">{{$category->name}}</option>');
  categories.trigger('change');
  @php
  endforeach;
  endif;
  @endphp
</script>

<script>
  $(document).ready(function () {
    function sendFile(file, el) {
      data = new FormData();
      data.append("file", file);
      $.ajax({
        data: data,
        type: "POST",
        url: "{{URL::to('/admin/upload')}}",
        cache: false,
        contentType: false,
        processData: false,
        success: function (url) {
          $(el).summernote('editor.insertImage', url);
        }
      });
    }

    $('#summernote').summernote({
        lang: "fa-IR",
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['insert', ['hr', 'picture']],
          ['misc', ['fullscreen', 'undo', 'redo']]
        ],
        callbacks: {
          onImageUpload: function (files, editor, welEditable) {
            sendFile(files[0], this);
          },
      },
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
        height: 200,
    });
  });

</script>
@endsection