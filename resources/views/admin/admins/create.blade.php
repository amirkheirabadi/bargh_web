@extends('layouts.dashboard') @section('header') @endsection @section('content')
@php
    use App\Helper\Web;
    $user = Web::user('admin');
@endphp
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data">
          <div class="row">
              <div class="form-group">
                <label>نام</label>
                <input type="text" name="first_name" class="form-control" value={{$admin->first_name}}>
              </div>
              <br>
              <div class="form-group">
                <label>نام خانوادگی</label>
                <input type="text" name="last_name" class="form-control" value={{$admin->last_name}}>
              </div>
              <br>
              <div class="form-group">
                <label>ایمیل</label>
                <input type="email" name="email" class="form-control" value={{$admin->email}}>
              </div>
              <br><br>

              @if($admin->id)
              <div class="form-group">
                <label>رمز عبور کنونی</label>
                <input type="password" name="current_password" class="form-control">
              </div>
              <br>
              @endif
              <div class="form-group">
                <label>رمز عبور</label>
                <input type="password" name="password" class="form-control">
              </div>
              <br>
              <div class="form-group">
                <label>تکرار رمز عبور</label>
                <input type="password" name="password_confirmation" class="form-control">
              </div>
              <br><br>


             @if ($user['super_admin'] == "yes")
               <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value={{$admin->status}}>
                @php
                  foreach(['active'=> 'فعال', 'suspend' => 'مسدود'] as $statusKey => $statusValue) :
                    if($statusKey == $admin->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>

               <div class="form-group">
                <label>مدیر اصلی</label>
                <select name="super_admin" class="form-control" value={{$admin->super_admin}}>
                @php
                  foreach(['yes'=> 'بلی', 'no' => 'خیر'] as $statusKey => $statusValue) :
                    if($statusKey == $admin->super_admin):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
              <br>
             @endif
          </div>
          <br><br>
          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/admins')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer')

  @endsection