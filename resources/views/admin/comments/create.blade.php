@extends('layouts.dashboard') @section('header') @endsection @section('content')
<div class="jumbotron  no-margin" data-pages="parallax">
  <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <h4 class="">{{$title}}</h4>
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">
      </div>
      <div class="export-options-container pull-right"></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <div class="panel-body">
        <form role="form" method="post" enctype="multipart/form-data">
          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label>فرستنده پیام</label>
                <br>
                <span>
                  <a href="{{URL::to('/admins/users/edit/' . $comment->sender->id)}}">{{$comment->sender->first_name . ' ' . $comment->sender->last_name}}</a>
                </span>
              </div>
              <br>
              <div class="form-group">
                <label>تاریخ ارسال</label>
                <br>
                <span>
                  {{$comment->created_at}}
                </span>
              </div>
              <br>
              <div class="form-group">
                <label>وضعیت</label>
                <select name="status" class="form-control" value={{$comment->status}}>
                @php
                  foreach(['active'=> 'فعال', 'suspend' => 'مسدود', 'pending' => 'در انتظار تایید'] as $statusKey => $statusValue) :
                    if($statusKey == $comment->status):
                @endphp
                  <option value="{{$statusKey}}" selected>{{$statusValue}}</option>
                @php
                    else:
                @endphp
                  <option value="{{$statusKey}}">{{$statusValue}}</option>
                @php
                    endif;
                  endforeach;
                @endphp
              </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>دریافت کننده پیام</label>
                <br>
                <span>
                  <a href="{{URL::to('/admins/users/edit/' . $comment->reciver->id)}}">{{$comment->reciver->first_name . ' ' . $comment->reciver->last_name}}</a>
                </span>
              </div>
              <br>
              <div class="form-group">
                <label>محل ارسال</label>
                <br>
                <span>{{$comment->place}}</span>
              </div>
            </div>
            <br><br>
            <div class="form-group">
              <label>متن دیدگاه</label>
              <textarea name="text" class="form-control">{{$comment->text}}</textarea>
            </div>
            <br>
            <div class="form-group">
              <label>پاسخ دیدگاه</label>
              <textarea name="answer" class="form-control">{{$comment->answer}}</textarea>
            </div>
            <br>
          </div>
          <br><br>
          <button type "submit" class="btn btn-complete btn-cons">ثبت</button>
          <a href={{URL::to('/admin/comments')}} class="btn btn-primary btn-cons">بازگشت</a>
          <br><br>
        </form>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <!-- END CONTAINER FLUID -->
  @endsection @section('footer') @endsection