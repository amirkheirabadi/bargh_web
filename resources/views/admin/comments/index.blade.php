@extends('layouts.dashboard')

@section('header')
    <link href="/backend/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/backend/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('content')
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
          <div class="inner" style="transform: translateY(0px); opacity: 1;">
            <h4 class="">{{$title}}</h4>
          </div>
        </div>
      </div>

      <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg bg-white">
        <!-- START PANEL -->
        <div class="panel panel-transparent">
          <div class="panel-heading">
            <div class="panel-title">
            </div>
            <div class="export-options-container pull-right"></div>
            <div class="clearfix"></div>
          </div>
          <div class="panel-body">


            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>وضیعت</label>
                        <select name="status" class="form-control" id="status">
                            <option value="all" selected>نمایش همه</option>
                            <option value="pending">در انتظار تایید</option>
                            <option value="suspend">مسدود شده</option>
                            <option value="publish">منتشر شده</option>
                    </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="datepicker4">ایجاد شده از تاریخ</label>
                            <input type="text"  id="from_date" class="form-control" />
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="datepicker4">ایجاد شده تا تاریخ</label>
                        <input type="text" id="to_date" class="form-control" />
                </div>
            </div>
            </div>
            <table class="table table-striped" id="mainTable">

            </table>
          </div>
        </div>
        <!-- END PANEL -->
      </div>
      <!-- END CONTAINER FLUID -->
@endsection

@section('footer')
    <script src="/backend/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="/backend/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="/backend/plugins/datatables-responsive/js/lodash.min.js"></script>
    <script type="text/javascript" src="/backend/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/js/bootstrap-datepicker.fa.min.js"></script>
    <script type="text/javascript">
    var settings = {
        "pProcessing": true,
        "bFilter": true,
		"bServerSide": true,
        "scrollCollapse": true,
        "oLanguage": {
            "sUrl": "{{URL::to('/backend/plugins/jquery-datatable/persian.js')}}"
        },
        "iDisplayLength": 10,
        "ajax": {
            'type': 'post',
        },
        "aoColumns": [
            { "mData": "id",  "aTargets": 0 ,'sTitle':'شناسه'},
            { "mData": "entity", "aTargets": 1 ,'sTitle':'محل درج دیدگاه'},
            { "mData": "user_sender", "aTargets": 2 ,'sTitle':'فرستنده'},
            { "mData": "status", "aTargets": 3 ,'sTitle':'وضعیت'},
            { "mData": "date", "aTargets": 4 ,'sTitle':'تاریخ ارسال'},
            { "mData": "action",  "aTargets": 5 ,'sortable':false},
        ]
    };

    var table = $('#mainTable').DataTable(settings);

    $(document).on('click','.btn-delete', function () {
        return confirm('از انجام این عملیات اطمینان دارید ؟');
    });

     $(document).on('click','.action_publish', function () {
        var id = $(this).attr('id');
        $.ajax({
            url: "/admin/comments/change/" + id + "/publish",
        }).done(function() {
             table.draw();
        });
    });

    $(document).on('click','.action_suspend', function () {
        var id = $(this).attr('id');
        $.ajax({
            url: "/admin/comments/change/" + id + "/suspend",
        }).done(function() {
             table.draw();
        });
    });

    $('#status ,#from_date, #to_date').on('change keyup', function() {
        table.column(1).search($('#status').val(), true, false).column(2).search($('#from_date').val(), true, false).column(3).search($('#to_date').val(), true, false).draw();
    });

     $("#from_date").datepicker({
        changeMonth: true,
        changeYear: true
    });
    $("#to_date").datepicker({
        changeMonth: true,
        changeYear: true
    });
    </script>
@endsection
