
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="/backend/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/backend/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/backend/pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    <link rel="stylesheet" href="/backend/custome.css" media="screen" title="no title">
  </head>
  <body class="fixed-header   ">
    <!-- START PAGE-CONTAINER -->
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="/backend/img/demo/pexels-photo.jpeg" data-src="/backend/img/demo/pexels-photo.jpeg" data-src-retina="/backend/img/demo/pexels-photo.jpeg" alt="" class="lazy">
        <!-- END Background Pic-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <a href="{{URL::to('/')}}"><img src="/logo.png" alt="logo" data-src="/logo.png" width="100" id="auth_logo"></a>
          @yield('content')
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <!-- END PAGE CONTAINER -->
    <!-- BEGIN VENDOR JS -->
    <script src="/backend/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="/backend/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="/backend/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script src="/backend/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/backend/plugins/jquery-validation/js/localization/messages_fa.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="/backend/pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="/backend/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <script>

    $(function() {
      $('#form-login').validate({
          onfocusout: false,
          onkeyup: false,
          onclick: false
      });
    })
    @if(Session::has('flash.alerts'))
      @foreach(Session::get('flash.alerts') as $alert)
        @php
            $thumb = '';
            switch ($alert['level']) {
                case 'warning':
                    $thumb = '<i class="fa fa-warning"></i>';
                    break;

                case 'success':
                    $thumb = '<i class="fa fa-check"></i>';
                    break;

                case 'danger':
                    $thumb = '<i class="fa fa-exclamation"></i>';
                    break;

                case 'info':
                    $thumb = '<i class="fa fa-bullhorn"></i>';
                    break;
            }
        @endphp
        $('body').pgNotification({
            style: 'circle',
            message: '{{$alert['message']}}',
            position: 'bottom-left',
            timeout: 0,
            type: '{{$alert['level']}}',
            thumbnail: '<?=$thumb?>'
        }).show();
        @endforeach
    @endif
    </script>
  </body>
</html>
