<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" integrity="sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD" crossorigin="anonymous">
	
	<style type="text/css">
		body, html, #height-calculator {
			margin: 0;
			padding: 0;
		}
		#height-calculator {
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
		}
		@font-face {
		    font-family: irsans;
		    src: url('/fonts/eot/IRANSansWeb(FaNum).eot');
		     src: url('/fonts/woff2/IRANSansWeb(FaNum).woff2') format('woff2'),
		          url('/fonts/woff/IRANSansWeb(FaNum).woff') format('woff'), /* Pretty Modern Browsers */
		          url('/fonts/ttf/IRANSansWeb(FaNum).ttf')  format('truetype');
		}

		#mainWrapper {
			font-family: irsans;
			direction: rtl;
			text-align: justify;
			font-size: 10pt;
		}
	</style>
	@yield('header')
</head>
<body>
	
	<div class="pure-g">
	    <div class="pure-u-1-1" id="mainWrapper">
			@yield('content')
	    </div>
	</div>

	@yield('footer')

<script>
window.location.hash = 1;
 var calculator = document.createElement("div");
 calculator.id = "height-calculator";
 while (document.body.firstChild) {
    calculator.appendChild(document.body.firstChild);
 }
 document.body.appendChild(calculator);
 document.title = calculator.clientHeight;
</script>
</body>
</html>