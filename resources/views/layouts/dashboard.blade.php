<!DOCTYPE html>
@php
    use App\Helper\Web;
    $user = Web::user('admin');
@endphp
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="/backend/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/backend/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/backend/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/backend/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="/backend/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/backend/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/backend/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/backend/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/backend/pages/css/pages.rtl.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
	<link href="/backend/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
    @yield('header')
    <link rel="stylesheet" href="/backend/custome.css" media="screen" title="no title">
  </head>
  <body class="fixed-header rtl menu-pin menu-behind">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-40"><img src="/backend/img/demo/social_app.svg" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-10"><img src="/backend/img/demo/email_app.svg" alt="socail">
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-40"><img src="/backend/img/demo/calendar_app.svg" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-10"><img src="/backend/img/demo/add_more.svg" alt="socail">
            </a>
          </div>
        </div>
      </div>
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
       
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{URL::to('/admin')}}" class="detailed">
              <span class="title">داشبورد  </span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/products')}}" class="detailed">
              <span class="title">محصولات</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-cube"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/categories')}}" class="detailed">
              <span class="title">دسته بندی ها</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="pg-folder"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/brands')}}" class="detailed">
              <span class="title">برند ها</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-fire"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/articles')}}" class="detailed">
              <span class="title">مقالات </span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-file-text-o"></i></span>
          </li>

          <li class="">
            <a href="{{URL::to('/admin/comments')}}" class="detailed">
              <span class="title">دیدگاه ها </span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-comment-o"></i></span>
          </li>


          <li class="">
            <a href="{{URL::to('/admin/news')}}" class="detailed">
              <span class="title">اخبار </span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-send"></i></span>
          </li>


          <li class="">
            <a href="{{URL::to('/admin/messages')}}" class="detailed">
              <span class="title">گفتگو ها </span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-comments-o"></i></span>
          </li>


          <li class="">
            <a href="{{URL::to('/admin/ads')}}" class="detailed">
              <span class="title">تبلیغات</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-hashtag"></i></span>
          </li>

          <li class=" ">
            <a href="javascript:;"><span class="title"> کسب و کارها</span>
            <span class=" open  arrow"></span></a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-diamond"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{URL::to('/admin/companies')}}">شرکت / فروشنده ها</a>
                <span class="icon-thumbnail">Co</span>
              </li>
              <li class="">
                <a href="{{URL::to('/admin/producers')}}">تولید کننده ها</a>
                <span class="icon-thumbnail">Pr</span>
              </li>
              <li class="">
                <a href="{{URL::to('/admin/importers')}}">وارد کننده ها</a>
                <span class="icon-thumbnail">Im</span>
              </li>
              <li class="">
                <a href="{{URL::to('/admin/technicians')}}">تکنسین ها</a>
                <span class="icon-thumbnail">Te</span>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/plans')}}" class="detailed">
              <span class="title">پلن های اشتراکی</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-flask"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/transactions')}}" class="detailed">
              <span class="title">تراکنش ها</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-money"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/users')}}" class="detailed">
              <span class="title">کاربران</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-user"></i></span>
          </li>
          @if($user['super_admin'] == "yes")
          <li class="">
            <a href="{{URL::to('/admin/admins')}}" class="detailed">
              <span class="title">مدیران</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-user-md"></i></span>
          </li>
          @endif
          <li class="">
            <a href="{{URL::to('/admin/contacts')}}" class="detailed">
              <span class="title">گزارشات و ارتباط با ما</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-support"></i></span>
          </li>
          <li class="">
            <a href="{{URL::to('/admin/settings')}}" class="detailed">
              <span class="title">تنظیمات سایت</span>
            </a>
            <span class="bg-primary icon-thumbnail"><i class="fa fa-gear"></i></span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
          <div class="pull-center hidden-md hidden-lg">
            <div class="header-inner">
              <div class="brand inline">
                <img src="/logo.png" alt="logo" data-src="/logo.png" data-src-retina="/backend/img/logo_2x.png" width="50" >
              </div>
            </div>
          </div>
          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline">
              <img src="/logo.png" alt="logo" data-src="/logo.png" data-src-retina="/backend/img/logo_2x.png" width="50" >
            </div>
             </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
             <span class="text-master">{{$user['name']}}</span>
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="/default_avatar.png" alt="" width="40" height="40">
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="{{URL::to('/admin/admins/edit/'. $user['id'])}}"><i class="fa fa-user"></i>  {{$user->first_name . ' ' . $user->last_name}}</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="{{URL::to('/admin/auth/signout')}}" class="clearfix">
                    <span class="pull-left">خروج</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
            @yield('content')
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright &copy; 2014 </span>
              <span class="font-montserrat">REVOX</span>.
              <span class="hint-text">تمام حقوق برای محفوظ است .</span>
              <span class="sm-block"><a href="#" class="m-l-10 m-r-10">رد کردن همه چیز</a> | <a href="#" class="m-l-10">قوانین سایت</a></span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
  
    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    <script src="/backend/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="/backend/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="/backend/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="/backend/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="/backend/plugins/classie/classie.js"></script>
    <script src="/backend/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="/backend/js/notifications.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="/backend/pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="/backend/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    @yield('footer')

    <script type="text/javascript">
    @if(Session::has('flash.alerts'))
    @foreach(Session::get('flash.alerts') as $alert)
        @php
            $thumb = '';
            switch ($alert['level']) {
                case 'warning':
                    $thumb = '<i class="fa fa-warning"></i>';
                    break;

                case 'success':
                    $thumb = '<i class="fa fa-check"></i>';
                    break;

                case 'danger':
                    $thumb = '<i class="fa fa-fire"></i>';
                    break;

                case 'info':
                    $thumb = '<i class="fa fa-bullhorn"></i>';
                    break;
            }
        @endphp
        $('body').pgNotification({
            style: 'circle',
            message: '{{$alert['message']}}',
            position: 'bottom-right',
            timeout: 0,
            type: '{{ $alert['level'] }}',
            thumbnail: '<?= $thumb ?>'
        }).show();
        @endforeach
    @endif

    </script>
  </body>
</html>
