<?php
use App\Province;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::group(['domain' => '{admin}.barghchin.dev'], function () {


Route::group(['prefix' => 'admin'], function () {
    Route::any('/auth/signout', 'Admin\AuthController@signout');
    Route::group(['middleware' => ['guest_check:admin']], function () {
        Route::any('/auth', 'Admin\AuthController@signin');
        Route::any('/auth/forget', 'Admin\AuthController@forget');
        Route::any('/auth/forgetcheck/{email}/{token}', 'Admin\AuthController@forgetcheck');
    });

    Route::group(['middleware' => ['auth_check:admin']], function () {
        Route::any('/categories', 'Admin\CategoryController@index');
        Route::any('/categories/get', 'Admin\CategoryController@get');
        Route::any('/categories/create', 'Admin\CategoryController@create');
        Route::any('/categories/edit/{id}', 'Admin\CategoryController@edit');
        Route::any('/categories/delete/{id}', 'Admin\CategoryController@delete');

        Route::any('/brands', 'Admin\BrandController@index');
        Route::any('/brands/create', 'Admin\BrandController@create');
        Route::any('/brands/edit/{id}', 'Admin\BrandController@edit');
        Route::any('/brands/get', 'Admin\BrandController@get');
        Route::any('/brands/delete/{id}', 'Admin\BrandController@delete');

        Route::any('/skills', 'Admin\SkillController@index');
        Route::any('/skills/create', 'Admin\SkillController@create');
        Route::any('/skills/edit/{id}', 'Admin\SkillController@edit');
        Route::any('/skills/get', 'Admin\SkillController@get');
        Route::any('/skills/delete/{id}', 'Admin\SkillController@delete');

        Route::any('/plans', 'Admin\PlanController@index');
        Route::any('/plans/create', 'Admin\PlanController@create');
        Route::any('/plans/edit/{id}', 'Admin\PlanController@edit');
        Route::any('/plans/delete/{id}', 'Admin\PlanController@delete');

        Route::any('/transactions', 'Admin\TransactionController@index');
        Route::any('/transactions/show/{id}', 'Admin\TransactionController@show');

        Route::any('/admins/create', 'Admin\AdminController@create');
        Route::any('/admins/edit/{id}', 'Admin\AdminController@edit');
        Route::any('/admins/delete/{id}', 'Admin\AdminController@delete');
        Route::any('/admins/{id?}', 'Admin\AdminController@index');

        Route::any('/users/create', 'Admin\UserController@create');
        Route::any('/users/edit/{id}', 'Admin\UserController@edit');
        Route::any('/users/delete/{id}', 'Admin\UserController@delete');
        Route::any('/users/{id?}', 'Admin\UserController@index');

        Route::any('/ads', 'Admin\AdsController@index');
        Route::any('/ads/create', 'Admin\AdsController@create');
        Route::any('/ads/edit/{id}', 'Admin\AdsController@edit');
        Route::any('/ads/delete/{id}', 'Admin\AdsController@delete');

        Route::any('/technicians', 'Admin\TechniciansController@index');
        Route::any('/technicians/edit/{id}', 'Admin\TechniciansController@edit');
        Route::any('/technicians/delete/{id}', 'Admin\TechniciansController@delete');
        Route::any('/technicians/highlight/{type}/{id}/{entity_id}/{data}', 'Admin\TechniciansController@highlight');
        Route::any('/technicians/skill/{id}', 'Admin\TechniciansController@skill');

        Route::any('/importers', 'Admin\ImporterController@index');
        Route::any('/importers/edit/{id}', 'Admin\ImporterController@edit');
        Route::any('/importers/category/{id}', 'Admin\ImporterController@category');
        Route::any('/importers/brand/{id}', 'Admin\ImporterController@brand');
        Route::any('/importers/delete/{id}', 'Admin\ImporterController@delete');
        Route::any('/importers/highlight/{type}/{id}/{entity_id}/{data}', 'Admin\ImporterController@highlight');

        Route::any('/producers', 'Admin\ProducerController@index');
        Route::any('/producers/edit/{id}', 'Admin\ProducerController@edit');
        Route::any('/producers/delete/{id}', 'Admin\ProducerController@delete');
        Route::any('/producers/highlight/{type}/{id}/{entity_id}/{data}', 'Admin\ProducerController@highlight');
        Route::any('/producers/category/{id}', 'Admin\ProducerController@category');

        Route::any('/companies', 'Admin\CompanyController@index');
        Route::any('/companies/edit/{id}', 'Admin\CompanyController@edit');
        Route::any('/companies/delete/{id}', 'Admin\CompanyController@delete');
        Route::any('/companies/highlight/{type}/{id}/{entity_id}/{data}', 'Admin\CompanyController@highlight');
        Route::any('/companies/brand/{id}', 'Admin\CompanyController@brand');

        Route::any('/agents', 'Admin\AgentController@index');
        Route::any('/agents/edit/{id}', 'Admin\AgentController@edit');
        Route::any('/agents/delete/{id}', 'Admin\AgentController@delete');
    
        Route::any('/products', 'Admin\ProductController@index');
        Route::any('/products/edit/{id}', 'Admin\ProductController@edit');
        Route::any('/products/delete/{id}', 'Admin\ProductController@delete');

        Route::any('/ads', 'Admin\AdsController@index');
        Route::any('/ads/edit/{id}', 'Admin\AdsController@edit');
        Route::any('/ads/delete/{id}', 'Admin\AdsController@delete');

        Route::any('/contacts', 'Admin\ContactController@index');
        Route::any('/contacts/show/{id}', 'Admin\ContactController@show');
        Route::any('/contacts/delete/{id}', 'Admin\ContactController@delete');

        Route::any('/comments', 'Admin\CommentController@index');
        Route::any('/comments/edit/{id}', 'Admin\CommentController@edit');
        Route::any('/comments/delete/{id}', 'Admin\CommentController@delete');
        Route::any('/comments/change/{id}/{status}', 'Admin\CommentController@change');

        Route::any('/messages', 'Admin\MessageController@index');
        Route::any('/messages/edit/{id}', 'Admin\MessageController@edit');
        Route::any('/messages/delete/{id}', 'Admin\MessageController@delete');

        Route::any('/pictures/{entity}/{entity_id}', 'Admin\PictureController@index');
        Route::any('/pictures/create/{entity}/{entity_id}', 'Admin\PictureController@create');
        Route::any('/pictures/delete/{entity}/{entity_id}/{id}', 'Admin\PictureController@delete');

        Route::any('/news', 'Admin\NewsController@index');
        Route::any('/news/edit/{id}', 'Admin\NewsController@edit');
        Route::any('/news/create/', 'Admin\NewsController@create');
        Route::any('/news/delete/{id}', 'Admin\NewsController@delete');

        Route::any('/articles', 'Admin\ArticleController@index');
        Route::any('/articles/edit/{id}', 'Admin\ArticleController@edit');
        Route::any('/articles/create/', 'Admin\ArticleController@create');
        Route::any('/articles/delete/{id}', 'Admin\ArticleController@delete');

        Route::any('/settings', 'Admin\SettingController@index');
        Route::post('/upload', 'Admin\DashboardController@upload');
        
        Route::any('/', 'Admin\DashboardController@dashboard');
        
    });


});

Route::post('/pay/check', 'Site\HomeController@payCheck');
Route::any('/', 'Site\HomeController@index');
