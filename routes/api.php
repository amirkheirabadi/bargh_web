<?php

use Illuminate\Http\Request;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::post('/home', 'HomeController@index');

    Route::get('/page/{page}', 'ApiController@page');
    Route::get('/news/show/{type}/{page}', 'ApiController@newsShow');    

    Route::get('/news/{id}', 'NewsController@show');
    Route::get('/news', 'NewsController@index');

    Route::get('/articles/{id}', 'ArticleController@show');
    Route::get('/articles', 'ArticleController@index');


    Route::post('/search', 'ApiController@search');
    Route::post('/provinces', 'ApiController@province');
    Route::post('/cities', 'ApiController@city');
    Route::get('/countries', 'ApiController@country');

    // categories
    Route::post('/categories', 'CategoryController@index');

    // Brands
    Route::post('/brands', 'BrandController@index');

    // Skills
    Route::post('/skills', 'SkillController@index');

    // Skills
    Route::post('/products', 'ProductController@index');

    // Auth
    Route::post('/auth/signin', 'AuthController@signin');
    Route::post('/auth/signup', 'AuthController@signup');
    Route::post('/auth/check', 'AuthController@signupCheck');
    Route::post('/auth/compilation', 'AuthController@compilation');
    Route::post('/auth/forget', 'AuthController@forget');
    Route::post('/auth/forget_check', 'AuthController@forgetCheck');
    Route::post('/auth/forget_compilation', 'AuthController@forgetCompilation');

    Route::post('/init', 'ApiController@init');
    Route::any('/plan/check/', 'PlanController@check');
    Route::any('/plan/result/', 'PlanController@result');

        Route::post('/messages/send', 'MessageController@messageSend');
        Route::post('/messages', 'MessageController@messageList');


    Route::group(['middleware' => 'auth_check:api'], function () {
        Route::post("/product/create", "ProductController@create");
        Route::post("/product/edit/{id}", "ProductController@edit");
        Route::post("/product/update/{id}", "ProductController@update");
        Route::post("/product/delete/{id}", "ProductController@delete");
        Route::get("/product/show/{id}", "ProductController@show");

        Route::post("/comments", "CommentController@index");

        Route::post('/user/password', 'UserController@password');
        Route::post('/user/change', 'UserController@change');
        Route::any('/user/profile', 'UserController@profile');
        Route::post('/plan/subscribe/{id}', 'PlanController@subscribe');


        Route::post('/user/company', 'UserController@company');
        Route::post('/user/importer', 'UserController@importer');
        Route::post('/user/producer', 'UserController@producer');
        Route::post('/user/technician', 'UserController@technician');
        Route::post('/user/file', 'UserController@file');
        Route::post('/user/delete/{entity}', 'UserController@delete');

        Route::post('/messages/contacts/{status}', 'MessageController@contacts');

        Route::get('/auth/signout', 'AuthController@signout');
    });
});
