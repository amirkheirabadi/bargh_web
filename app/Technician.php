<?php

namespace App;

use App\Skill;
use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $table = "technicians";
    protected $primaryKey = "id";

    public $timestamps = false;

    public function address()
    {
        if (!$this->id) {
            return;
        }
        $this->address = Address::where('entity', 'technician')->where('entity_id', $this->id)->first();
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill', 'technicians_skill', 'skill_id' ,'technicians_id')->withPivot('endrose', 'highlight', 'id');
    }

    public function setSkill($skills)
    {
        $this->skills()->detach();
        foreach ($skills as $skill) {
            $sk = Skill::where('name', $skill)->first();
            if (!$sk) {
                $sk = new Skill;
                $sk->name = $skill;
                $sk->save();
            }
            $this->skills()->attach($sk->id);
        }
    }
}
