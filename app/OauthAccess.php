<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthAccess extends Model
{
    protected $table = "oauth_access_tokens";
    protected $primaryKey = "id";
}
