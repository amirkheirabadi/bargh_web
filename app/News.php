<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";
    protected $primaryKey = "id";

    public function owner()
    {
        return $this->belongsTo('App\Admin', 'admin_id', 'id');
    }
}
