<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Address;

class Producer extends Model
{
    protected $table = "producers";
    protected $primaryKey = "id";

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_producers', 'producer_id', 'category_id')->withPivot('endrose', 'highlight', 'id');
    }

    public function address()
    {
        if (!$this->id) {
            return;
        }
        $this->address = Address::where('entity', 'producer')->where('entity_id', $this->id)->first();
    }

    public function setCategories($categories)
    {
        $this->categories()->detach();
        foreach ($categories as $cat) {
            $br = Category::where('name', $cat)->first();
            if (!$cat) {
                $cat = new Category;
                $cat->name = $cat;
                $cat->verified = "no";
                $cat->save();
            }
            $this->categories()->attach($cat->id);
        }
   }
}
