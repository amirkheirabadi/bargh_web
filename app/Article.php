<?php

namespace App;

use App\Admin;
use App\Company;
use App\Importer;
use App\Producer;
use App\Technician;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";
    protected $primaryKey = 'id';

    function owner() {
        $rules = [
            'admin' => 'مدیر',
            'company' => 'فروشنده / شرکت',
            'importer' => 'وارد کننده',
            'producer' => 'تولید کننده',
            'technician' => 'تکنسین'
        ];

        switch ($this->entity) {
            case 'admin':
                $this->owner = Admin::where('id', $this->entity_id)->first();
                break;
            case 'company':
                $this->owner = Company::where('id', $this->entity_id)->first();
                break;
            case 'importer':
                $this->owner = Importer::where('id', $this->entity_id)->first();
                break;
            case 'producer':
                $this->owner = Producer::where('id', $this->entity_id)->first();
                break;
           case 'technician':
                $this->owner = Technician::where('id', $this->entity_id)->first();
                break;
        }
        $this->writter = $this->owner->name;
        if ($this->entity == "admin") {
            $this->writter = $this->owner->first_name . ' ' . $this->owner->last_name;
            $this->role = $rules[$this->entity];
        }
    }
}
