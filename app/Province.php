<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "province";
    protected $primarykey = "id";

    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany('App\City', 'province_id', 'id');
    }
}
