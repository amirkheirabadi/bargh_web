<?php

namespace App;

use App\Comment;
use App\Importer;
use App\Producer;
use App\Company;
use App\Technician;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    protected $primaryKey = "id";

    public function sender() {
        return $this->belongsTo('App\User', 'user_sender', 'id');
    }

    public function reciver() {
        return $this->belongsTo('App\User', 'user_sender', 'id');
    }

    public function place()
    {
        switch ($this->entity) {
            case 'product':
                $place = Product::where('id', $this->entity_id)->first();
                $this->place = 'محصول ' . $place->name;
            break;
            
            case 'company':
                $place = Company::where('id', $this->entity_id)->first();
                $this->place = 'فروشنده - ' . $place->name;
            break;

            case 'importer':
                $place = Importer::where('id', $this->entity_id)->first();
                $this->place = 'وارد کننده - ' . $place->name;
            break;

            case 'producer':
                $place = Producer::where('id', $this->entity_id)->first();
                $this->place = 'تولید کننده - ' . $place->name;
            break;

            case 'technician':
                $place = Technician::where('id', $this->entity_id)->first();
                $this->place = 'تکنسین - ' . $place->name;
            break;
        }
    }
}
