<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function brands()
    {
        return $this->belongsToMany('App\Brand', 'categories_brands', 'category_id', 'brand_id');
    }

    public function producers()
    {
        return $this->belongsToMany('App\Producer', 'categories_producers', 'category_id', 'producer_id');
    }

    public function importers()
    {
        return $this->belongsToMany('App\Producer', 'categories_importers', 'category_id', 'importer_id');
    }

    public function products()
    {
        return $this->hasMany('App\Products', 'category_id', 'id');
    }
}
