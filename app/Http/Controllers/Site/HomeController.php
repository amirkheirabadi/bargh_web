<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Web;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('welcome');
    }

    public function payCheck(Request $request)
    {
        # code...
    }
}
