<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Web;
use App\Admin;
use Auth;
use Flash;
use Hash;
use Mail;
use App\Mail\AdminForget;

class AuthController extends Controller
{
    public function signin(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin/auth/signin', [
                'title' => 'ورود به بخش مدیریت'
            ]);
        }

        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];
        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request, $rules, '/admin/auth');
        }

        if (Auth::guard('admin')->attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ], $request->has('remember'))) {
            $admin = Auth::guard('admin')->user();
            switch ($admin->status) {
                case 'suspend':
                    Flash::error('متاسفانه حساب کاربری شما مسدود می باشد .');
                    Auth::logout();

                    return redirect('/admin/auth');
                break;
            }
            return redirect('/admin');
        }
        Flash::error('ایمیل و یا رمز عبور صحیح نمی باشد .');
        return redirect('/admin/auth');
    }

    public function signout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/auth');
    }


    public function forget(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin/auth/forget', [
                'title' => 'تغییر رمز عبور'
            ]);
        }
        $rules = [
            'email' => 'email|required',
        ];
        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request, $rules, '/admin/auth/forget');
        }
        $admin = Admin::where('email', $request->get('email'))->first();
        if (!$admin) {
            Flash::error('کاربری با این مشخصات در سیستم ثبت نشده است .');
            return redirect('/admin/auth/forget');
        }

        $admin->forget_token = rand(10000,99999);
        $admin->save();

        Mail::to($request->get('email'))->send(new AdminForget($admin->email, $admin->forget_token));

        Flash::success('لطفا ایمیل خود را بررسی کنید .');
        return redirect('/admin/auth/');
    }

    public function forgetCheck(Request $request, $email, $token)
    {
        $admin = Admin::where('email', $email)->first();
        if (!$admin) {
            Flash::error('کاربری با این مشخصات در سیستم ثبت نشده است .');
            return redirect('/admin/auth');
        }
        if ($admin->forget_token != $token) {
            Flash::error('کد وارد شده صحیح نمی باشد .');
            return redirect('/admin/auth/forget');
        }
        if ($request->isMethod('post')) {
             $rules = [
                'password' => 'required|confirmed'
            ];
            if (Web::validationCheck($request ,$rules)) {
                return Web::validation($request, $rules, '/admin/auth/forgetcheck/' . $email . '/' . $token);
            }
            $admin->password = Hash::make($request->get('password'));
            $admin->forget_token = "";
            $admin->save();
            Flash::success('رمز عبور شما با موفقیت تغییر کرد .');
            return redirect('/admin/auth');
        }
        return view('admin/auth/forgetcheck', [
            'title' => 'تغییر رمز عبور'
        ]);
    }
}
