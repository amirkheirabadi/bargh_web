<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Importer;
use App\City;
use App\Province;
use App\Country;
use App\Address;
use App\Helper\Web;
use DB;
use Flash;
use URL;
use Image;
use Storage;

class ImporterController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.importers.index', [
                'title' => 'لیست وارد کننده ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          case '2':
              $order_item = 'user_id';
              break;

          case '3':
              $order_item = 'mobile';
              break;

          case '4':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $importers = Importer::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($importers as $importer) {
            $status = [
                'pending' => 'در انتظار تایید',
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $extra = '<div class="btn-group dropdown-default">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        اطلاعات
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/comments?entity=importer&entity_id=' . $importer->id . '">لیست دیدگاه ها</a></li>
                    <li><a href="/admin/articles?entity=importer&entity_id=' . $importer->id . '">لیست مقالات</a></li>
                    <li><a href="/admin/agents?entity=importer&entity_id=' . $importer->id . '">لیست نمایندگی ها</a></li>
                    </ul>
                </div>';

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/importers/delete/'.$importer->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                    <a href="'. URL::to('/admin/pictures/importer/'.$importer->id) .'" class="btn btn-success"><i class="fa fa-picture-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/importers/edit/'.$importer->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>
                    ' . $extra;

            $user = $importer->user;
            array_push($data, array(
              'id' => $importer->id,
              'name' => $importer->name,
              'user' => '<a href="/admin/users/'. $importer->user_id .'">'.$user['first_name'] . ' ' . $user['last_name'].'</a>' ,
              'mobile' => $importer->mobile,
              'status' => $status[$importer->status],
              'action' => $action,
          ));
        }

        $importers_count = Importer::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $importers_count,
          'recordsFiltered' => $importers_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $importer = Importer::find($id);
        if (!$importer) {
            Flash::error('وارد کننده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/importers');
        }
        $provinces = Province::all();
        $importer->address();
        $address_city = City::find($importer->address->city_id);
        $countries = Country::all();
        if (!$request->isMethod('post')) {
            return view('admin.importers.create',[
                'title' => 'ویرایش وارد کننده',
                'importer' => $importer,
                'provinces' => $provinces,
                'address_city' => $address_city,
                'countries' => $countries,
            ]);
        }
        
        $rules = [
            'name' => 'required',
            'mobile' => 'required|phone:IR,mobile',
            'phone' => 'min:4,max:8',
            'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'owner' => 'required',
            'status' => 'required',
            'address_city' => 'required',
            'address_province' => 'required',
            'address' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:1000',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];

        $importer->name = $request->get('name');
        $importer->mobile = Web::phoneFormat($request->get('mobile'));
        $importer->phone = Web::phoneFormat($request->get('phone'));
        $importer->website = $request->get('website');
        $importer->owner = $request->get('owner');
        $importer->description = $request->get('description');
        $importer->status = $request->get('status');

        $importer->address->province_id = $request->get('address_province');
        $importer->address->city_id = $request->get('address_city');
        $importer->address->address = $request->get('address');
        $importer->address->lat = $request->get('lat');
        $importer->address->long = $request->get('long');
        
        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.importers.create',[
                'title' => 'ویرایش وارد کننده',
                'importer' => $importer,
                'provinces' => $provinces,
                'address_city' => $address_city,
                'countries' => $countries,
            ]);
        }
        

        if(!empty($request->get('brands'))){
            $importer->brands()->sync($request->get('brands'));    
        }

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/users/' . $name . '.jpg');

            unlink(public_path() .'/temp/' . $name . '.jpg');
            if (!empty($importer->avatar) && Storage::exists(public_path() . $importer->avatar)) {
                unlink(public_path() . $importer->avatar);
            }
            $importer->avatar = '/files/users/' . $name . '.jpg';            
        }

        if ($request->hasFile('catalog') && $request->file('catalog')->isValid()) {
            $name = str_random(40);
            $request->file('catalog')->move('files/catalog/', $name.'.pdf');

            if (!empty($importer->catalog) && Storage::exists(public_path() . $importer->catalog)) {
                unlink(public_path() . $importer->catalog);
            }
            $importer->catalog = '/files/catalog/' . $name . '.pdf';
        }
        

        $importer->address->save();
        unset($importer->address);
        $importer->save();
        Flash::success('ویرایش وارد کننده با موفقیت انجام شد .');
        return redirect('/admin/importers/');
    }

    public function delete($id)
    {        
        $importer = importer::find($id);
        if (!$importer) {
            Flash::error('وارد کننده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/importers');
        }

        Address::where('entity', 'importer')->where('entity_id', $importer->id)->delete();
        $importer->brands()->sync([]);

        if (!empty($importer->avatar) && Storage::exists(public_path() . $importer->avatar)) {
            unlink(public_path() . $importer->avatar);
        }

        if (!empty($importer->catalog) && Storage::exists(public_path() . $importer->catalog)) {
            unlink(public_path() . $importer->catalog);
        }
        $importer->delete();

        Flash::success('وارد کننده مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/importers/');
    }

    public function category(Request $request, $id)
    {
        if (!$request->isMethod('post')) {
            if(!$request->get('id')) {
                return redirect('/admin/importers/edit/' . $id);
            }

            DB::delete('delete from categories_importers where importer_id = ?  AND id = ?', array($id,$request->get('id')));

            return redirect('/admin/importers/edit/' . $id);
        }

         $rules = [
            'category' => 'required',
            'country' => 'required',
            'highlight' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/importers/edit/'. $id);
        }

        $importer = Importer::find($id);
        $importer->categories()->attach([$request->get('category') => ['country' => $request->get('country'), 'highlight' => $request->get('highlight')]]);
        $importer->save();
        return redirect('/admin/importers/edit/' . $id);
    }

    public function brand(Request $request, $id)
    {
        if (!$request->isMethod('post')) {
            if(!$request->get('id')) {
                return redirect('/admin/importers/edit/' . $id);
            }

            DB::delete('delete from importers_brands where importer_id = ?  AND id = ?', array($id,$request->get('id')));

            return redirect('/admin/importers/edit/' . $id);
        }

         $rules = [
            'brand' => 'required',
            'highlight' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/importers/edit/'. $id);
        }

        $importer = Importer::find($id);
        $importer->brands()->attach([$request->get('brand') => ['highlight' => $request->get('highlight')]]);
        $importer->save();
        return redirect('/admin/importers/edit/' . $id);
    }

    public function highlight($type, $id, $entity_id, $data) {
        switch ($type) {
            case 'category':
                Importer::find($id)->categories()->updateExistingPivot($entity_id, ['highlight' => $data]);
                break;
            
            case 'brand':
                Importer::find($id)->brands()->updateExistingPivot($entity_id, ['highlight' => $data]);
                break;
        }

        return 'ok';
    }
}
?>
