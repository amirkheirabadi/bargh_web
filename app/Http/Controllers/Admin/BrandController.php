<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Category;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class BrandController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.brands.index', [
                'title' => 'لیست برندها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name_fa';
              break;

          case '2':
              $order_item = 'name_en';
              break;

          case '3':
              $order_item = 'famous';
              break;

          case '4':
              $order_item = 'verified';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $brands = Brand::where(function ($query) use ($request) {
              $query->where('name_fa', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('name_en', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($brands as $brand) {
            $famous = [
                'yes' => 'برجسته',
                'no' => 'معمولی'
            ];

            $verified = [
                'yes' => 'تایید شده',
                'no' => 'تایید نشده'
            ];

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/brands/delete/'.$brand->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/brands/edit/'.$brand->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $brand->id,
              'name_fa' => $brand->name_fa,
              'name_en' => $brand->name_en,
              'famous' => $famous[$brand->famous],
              'verified' => $verified[$brand->verified],
              'action' => $action,
          ));
        }

        $brands_count = Brand::where(function ($query) use ($request) {
              $query->where('name_fa', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('name_en', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $brands_count,
          'recordsFiltered' => $brands_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $brand = new Brand();
        if (!$request->isMethod('post')) {
            return view('admin.brands.create', [
                'title' => 'افزودن برند جدید',
                'brand' => $brand
            ]);
        }

        $rules = [
            'name_fa' => 'required',
            'name_en' => 'required',
            'famous' => 'required',
            'verified' => 'required',
            'logo' => 'file|mimes:jpg,png,jpeg|max:2000',
        ];

        $brand->name_fa = $request->get('name_fa');
        $brand->name_en = $request->get('name_en');
        $brand->description = $request->get('description');
        $brand->famous = $request->get('famous');
        $brand->verified = $request->get('verified');

        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);

            return view('admin.brands.create', [
                'title' => 'افزودن برند جدید',
                'brand' => $brand
            ]);
        }

  
        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            $name = str_random(40);
            $request->file('logo')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.brand_size');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/brands/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');
            $brand->logo = '/files/brands/' . $name .'.jpg';
        }

        $brand->save();

        if(!empty($request->get('category'))) {
            $brand->categories()->sync($request->get('category'));
        }


        Flash::success('برند جدید با موفقیت افزوده شد .');
        return redirect('/admin/brands/');
    }


    public function edit(Request $request, $id)
    {
        $brand = Brand::find($id);
        if (!$brand) {
            Flash::error('برند با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/brands');
        }
        if (!$request->isMethod('post')) {
            return view('admin.brands.create',[
                'title' => 'ویرایش برند',
                'brand' => $brand,
                'categories' => $brand->categories
            ]);
        }
        $rules = [
            'name_fa' => 'required',
            'name_en' => 'required',
            'famous' => 'required',
            'verified' => 'required',
            'logo' => 'file|mimes:jpg,png,jpeg|max:2000',
        ];

        $brand->name_fa = $request->get('name_fa');
        $brand->name_en = $request->get('name_en');
        $brand->description = $request->get('description');
        $brand->famous = $request->get('famous');
        $brand->verified = $request->get('verified');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);
            return view('admin.brands.create',[
                'title' => 'ویرایش برند',
                'brand' => $brand,
                'categories' => $brand->categories
            ]);
        }
        
        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            if (!empty($brand->logo) && Storage::exists(public_path() . $brand->logo)) {
                unlink(public_path() . $brand->logo);
            }
            $name = str_random(40);
            $request->file('logo')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.brand_size');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/brands/' . $name . '.jpg');

            $brand->logo = '/files/brands/' . $name . '.jpg';
            unlink(public_path() .'/temp/' . $name . '.jpg');
        }
        
        if(!empty($request->get('category'))) {
            $brand->categories()->sync($request->get('category'));
        }
        $brand->save();
        Flash::success('ویرایش برند با موفقیت انجام شد .');
        return redirect('/admin/brands/');
    }

    public function delete($id)
    {        
        $brand = Brand::find($id);
        if (!$brand) {
            Flash::error('برند با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/brands');
        }
        $brand->delete();
                       
        Flash::success('برند مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/brands/');
    }

    public function get(Request $request)
    {
        $brands = Brand::where('name_fa', 'like', '%' . $request->get('q') . '%')->orWhere('name_en', 'like', '%' . $request->get('q') . '%')->get();
        $results = [];

        foreach ($brands as $brand) {
            $results[] = [
                'id' => $brand->id,
                'text' => $brand->name_fa . ' - ' . $brand->name_en
            ];
        }

        return [
            'items' => $results
        ];
    }
}
 ?>
