<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Picture;
use App\Producer;
use App\Company;
use App\Importer;
use App\Technician;
use App\Product;
use App\Helper\Web;
use Flash;
use Image;
use Storage;
use URL;

class PictureController extends Controller
{
    public function index(Request $request,$entity ,$entity_id)
    {
        $pictures = Picture::where('entity', $entity)->where('entity_id', $entity_id)->get();

        return view('admin.pictures.index', [
           'pictures' => $pictures,
           'title' => 'مدیریت تصاویر',
           "entity" => $entity,
           "entity_id" => $entity_id
        ]);
    }

    public function create(Request $request, $entity, $entity_id)
    {
        $mainEntity = $entity;
        $rules = [
            'picture' => 'required|file|mimes:jpg,png,jpeg|max:5000',
        ];

        if (Web::validationCheck($request, $rules)) {
            return Web::validation($request, $rules, '/admin/pictures/' . $entity . '/' . $entity_id);
        }

        $picture = new Picture;
        $picture->entity = $entity;
        $picture->entity_id = $entity_id;

        switch ($entity) {
            case 'product':
                $product = Product::find($entity_id);
                $picture->user_id = $product->user_id;
                break;
            
            case 'company':
                $entity = Company::find($entity_id);
                $picture->user_id = $entity->user_id;
                break;

            case 'importer':
                $entity = Importer::find($entity_id);
                $picture->user_id = $entity->user_id;
                break;

            case 'producer':
                $entity = Producer::find($entity_id);
                $picture->user_id = $entity->user_id;
                break;


            case 'technician':
                $entity = Technician::find($entity_id);
                $picture->user_id = $entity->user_id;
                break;
        }

        if ($request->file('picture')->isValid()) {
            $name = str_random(40);
            $request->file('picture')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.product_gallery_size');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/products/gallery/' . $name . '.jpg');

            $picture->url = '/files/products/gallery/' . $name . '.jpg';
            unlink(public_path() .'/temp/' . $name . '.jpg');

            $picture->save();
        }

        Flash::success('تصویر جدید با موفقیت افزوده شد .');
        return redirect('/admin/pictures/' . $mainEntity . '/' . $entity_id);
    }

    public function delete($entity, $entity_id, $id)
    {        
        $picture = Picture::find($id);
        if (!$picture) {
            Flash::error('تصویر با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/pictures');
        }
        if (!empty($picture->url) && Storage::exists(public_path() . $picture->url)) {
            unlink(public_path() . $picture->url);
        }
        $picture->delete();
                 
        Flash::success('تصویر مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/pictures/'  . $entity . '/' . $entity_id);
    }
}
?>
