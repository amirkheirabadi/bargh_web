<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\Agent;
use App\City;
use App\Province;
use App\Country;
use App\Product;
use App\Address;
use App\Comment;
use App\Picture;
use App\Endrose;
use App\Helper\Web;
use Flash;
use URL;
use DB;
use Image;
use Storage;
use Config;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.agents.index', [
                'title' => 'لیست نمایندگی ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'entity_id';
              break;

          case '2':
              $order_item = 'name';
              break;

          case '3':
              $order_item = 'mobile';
              break;

          case '4':
              $order_item = 'type';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $agents = Agent::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'));

        if($request->get('entity') && $request->get('entity_id')) {
            $agents = $agents->where('entity', $request->get('entity'))->where('entity_id', $request->get('entity_id'));
        }

        $agents = $agents->get();

        foreach ($agents as $agent) {
            $type = [
                'sell' => 'نمایندگی فروش',
                'service' => 'نمایندگی خدمات',
            ];

            $entity = [
                'importer' => 'وارد کننده',
                'producer' => 'تولید کننده',
            ];

            $action = '<div class="btn-group">
                    <a href="'. URL::to('/admin/agents/delete/'.$agent->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                    </a>
                </div>
            ';

            $agent->owner();
            array_push($data, array(
              'id' => $agent->id,
              'owner' => '<a href="/admin/' . $agent->entity . 's/edit/' . $agent->entity_id . '">' . $agent->place . '</a>',
              'name' => $agent->name,
              'mobile' => $agent->mobile,
              'type' => $type[$agent->type],
              'action' => $action,
          ));
        }

        $agents_count = Agent::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $agents_count,
          'recordsFiltered' => $agents_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $agent = Agent::find($id);
        if (!$agent) {
            Flash::error('فروشنده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/agents');
        }
        $agent->address();

        if (!$request->isMethod('post')) {
            return view('admin.agents.create',[
                'title' => 'ویرایش نمایندگی',
                'agent' => $agent,
            ]);
        }

        $rules = [
            'name' => 'required',
            'mobile' => 'required|phone:IR,mobile',
            'type' => 'min:4,max:8',
            'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'owner' => 'required',
            'email' => 'email',
            'status' => 'required',
            'address_city' => 'required',
            'address_province' => 'required',
            'address' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:2000',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];

        $company->name = $request->get('name');
        $company->mobile = Web::phoneFormat($request->get('mobile'));
        $company->phone = Web::phoneFormat($request->get('phone'));
        $company->owner = $request->get('owner');
        $company->email = $request->get('email');
        $company->fax = $request->get('fax');
        $company->website = $request->get('website');
        $company->description = $request->get('description');
        $company->status = $request->get('status');
        $company->highlight = $request->get('hightlight');
        $company->address->province_id = $request->get('address_province');
        $company->address->city_id = $request->get('address_city');
        $company->address->address = $request->get('address');
        
        if (!empty($request->get('lat'))){
            $company->address->lat = $request->get('lat');
            $company->address->long = $request->get('long');
        }

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.agents.create',[
                'title' => 'ویرایش فروشنده',
                'company' => $company,
                'provinces' => $provinces,
                'address_city' => $address_city,
            ]);
        }
        
        if(!empty($request->get('brands'))) {
            $company->brands()->sync($request->get('brands'));    
        }

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/users/' . $name . '.jpg');

            unlink(public_path() .'/temp/' . $name . '.jpg');
            if (!empty($company->avatar) && Storage::exists(public_path() . $company->avatar)) {
                unlink(public_path() . $company->avatar);
            }
            $company->avatar = '/files/users/' . $name . '.jpg';            
        }

         if ($request->hasFile('catalog') && $request->file('catalog')->isValid()) {
            $name = str_random(40);
            $request->file('catalog')->move('temp/', $name.'.pdf');

            $img = Image::make(public_path().'/temp/' . $name . '.pdf');
            $size= config('barghchin.user_catalog');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/catalog/' . $name . '.pdf');

            unlink(public_path() .'/temp/' . $name . '.pdf');
            if (!empty($company->catalog) && Storage::exists(public_path() . $company->catalog)) {
                unlink(public_path() . $company->catalog);
            }
            $company->catalog = '/files/catalog/' . $name . '.pdf';

        }
        
        $company->address->save();
        unset($company->address);
        $company->save();
        Flash::success('ویرایش فروشنده با موفقیت انجام شد .');
        return redirect('/admin/agents/');
    }

    public function delete($id)
    {        
        $agent = Agent::find($id);
        if (!$agent) {
            Flash::error(' نمایندگی با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/agents');
        }

        Address::where('entity', 'agent')->where('entity_id', $agent->id)->delete();

        $agent->delete();

        Flash::success(' نمایندگی مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/agents');
    }

}
?>
