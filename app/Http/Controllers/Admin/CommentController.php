<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Helper\Web;
use App\Comment;
use App\User;
use Flash;
use URL;
use Image;
use Storage;

class CommentController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.comments.index', [
                'title' => 'لیست دیدگاه ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'entity';
              break;

          case '2':
              $order_item = 'user_sender';
              break;

          case '3':
              $order_item = 'text';
              break;

          case '4':
              $order_item = 'created_at';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $comments = Comment::where(function ($query) use ($request) {
            $query->where('text', 'LIKE', '%'.$request->get('search')['value'].'%')
             ->orWhere('answer', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'));

        if ($request->query('user_sender')) {
            $comments = $comments->where('user_sender', $request->get('user_sender'));
        }

        if (!empty($request->get('user_reciver'))) {
            $comments = $comments->where('user_reciver', $request->get('user_reciver'));
        }

        if (!empty($request->get('entity')) && !empty($request->get('entity_id'))) {
            $comments = $comments->where('entity', $request->get('entity'))->where('entity_id', $request->get('entity_id'));
        }

        if(!empty($request->get('columns')['1']['search']['value']) && $request->get('columns')['1']['search']['value'] != "all") {
            $comments = $comments->where('status', $request->get('columns')['1']['search']['value']);
        }

        if(!empty($request->get('columns')['2']['search']['value'])) {
            $comments = $comments->where('created_at', '>', Web::toGregorian($request->get('columns')['2']['search']['value']));
        }

        if(!empty($request->get('columns')['3']['search']['value'])) {
            $comments = $comments->where('created_at', '<', Web::toGregorian($request->get('columns')['3']['search']['value']));
        }

        $comments = $comments->get();

        foreach ($comments as $comment) {
            $entity = [
                'product' => 'محصول',
                'company' => 'فروشنده',
                'importer' => 'وارد کننده',
                'producer' => 'ت,لید کننده',
                'technician' => 'تکنسین',
            ];

            $links = [
                'company' => 'companies',
                'importer' => 'importers',
                'product' => 'products',
                'technician' => 'technicians',
                'company' => 'companies'
            ];

            $status = [
                'pending' => 'در انتظار تایید',
                'publish' => 'در حال نمایش',
                'suspend' => 'مسدود شده',
            ];

            $user_sender = User::where('id', $comment->user_sender)->first();
            $user_sender = $user_sender->first_name . ' ' . $user_sender->last_name;
            $comment->place(); 
            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/comments/delete/'.$comment->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/comments/edit/'.$comment->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    ';
            
            switch ($comment->status) {
                case 'pending':
                    $action = $action . '<a id="'.$comment->id.'" class="action_publish btn btn-success"><i class="fa fa-check" title="تایید دیدگاه"></i>
                      </a>
                      <a id="'.$comment->id.'" class="btn btn-success action_suspend"><i class=" fa fa-times" title="مسدود کردن دیدگاه"></i>
                      </a>';
                    break;
                
                case 'publish':
                    $action = $action . '<a id="'.$comment->id.'" class="btn btn-success action_suspend"><i class=" fa fa-times" title="مسدود کردن دیدگاه"></i>
                      </a>';
                    break;

                case 'suspend':
                    $action = $action . '<a id="'.$comment->id.'" class="action_publish btn btn-success"><i class="fa fa-check" title="تایید دیدگاه"></i>
                      </a>';
                    break;
            }

            $action = $action . '</div>';
            
            $date = Web::jalali($comment);
            array_push($data, array(
              'id' => $comment->id,
              'entity' => '<a href="/admin/' . $links[$comment->entity] .'/edit/' . $comment->entity_id . '">' . $comment->place . '</a>',
              'user_sender' => '<a href="/admin/users/edit/' . $comment->user_sender . '">' . $user_sender . '</a>',
              'status' => $status[$comment->status],
              'date' => $date['created_at']['time'],
              'action' => $action,
          ));
        }

        $comments_count = Category::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('type', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $comments_count,
          'recordsFiltered' => $comments_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $comment = Comment::where('id', $id)->with(['sender', 'reciver'])->first();
        $date = Web::jalali($comment);
        $comment->created_at = $date['created_at']['time'];
        $comment->updated_at = $date['updated_at']['time'];
        $comment->answer_at = $date['answer_at']['time'];
        $comment->place();

        if (!$comment) {
            Flash::error('دیدگاه با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/comments');
        }
        if (!$request->isMethod('post')) {
            return view('admin.comments.create',[
                'title' => 'ویرایش دیدگاه',
                'comment' => $comment,
            ]);
        }
        $rules = [
            'text' => 'required',
            'answer' => 'required',
            'status' => 'required',
        ];
        $comment->text = $request->get('text');
        $comment->answer = $request->get('answer');
        $comment->status = $request->get('status');
        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);
            return view('admin.comments.create',[
                'title' => 'ویرایش دیدگاه',
                'comment' => $comment,
            ]);
        }
        unset($comment->place);
        $comment->save();
        Flash::success('ویرایش دیدگاه با موفقیت انجام شد .');
        return redirect('/admin/comments/');
    }

    public function change($id, $status)
    {        
        $comment = Comment::find($id);
        if ($comment) {
            $comment->status = $status;
            $comment->save();
        }
        return 'ok';
    }

    public function delete($id)
    {        
        $comment = Comment::find($id);
        if (!$comment) {
            Flash::error('دیدگاه با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/comments');
        }
        $comment->delete();
                       
        Flash::success('دیدگاه مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/comments/');
    }
}
 ?>
