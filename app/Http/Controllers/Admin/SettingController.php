<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Helper\Web;
use Flash;
use URL;

class SettingController extends Controller
{
    public function index(Request $request)
    {   
        $settings = Setting::all();

        $viewInput = [
            'commercial' => 0,
            'rule' => '',
            'aboutus' => '',
        ];
        foreach ($settings as $setting) {
            $viewInput[$setting->setting_key] = $setting->setting_value;
        }

        if (!$request->isMethod('post')) {
            return view('admin.settings.index', [
                'title' => 'تنظیمات',
                'data' => $viewInput
            ]);
        }

       foreach ($request->all() as $setting_key => $setting_value) {
            $setting = Setting::where('setting_key', $setting_key)->first();
            if(!$setting) {
                $setting = new Setting();
                $setting->setting_key = $setting_key;
            }

            $setting->setting_value = $setting_value;
            $setting->save();
       }

       return redirect('/admin/settings');
    }
}
 ?>
