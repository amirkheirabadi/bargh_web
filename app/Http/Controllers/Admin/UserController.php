<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class UserController extends Controller
{
    public function index(Request $request, $id = null)
    {
        if (!$request->isMethod('post')) {
            return view('admin.users.index', [
                'title' => 'لیست کاربران',
                'id' => $id
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'first_name';
              break;

          case '2':
              $order_item = 'mobile';
              break;

          case '3':
              $order_item = 'register_from';
              break;

          case '4':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $users = User::where(function ($query) use ($request) {
              $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'));

        if ($id) {
            $users = $users->where('id', $id);
        }

        $users = $users->get();

        foreach ($users as $user) {
            $status = [
                'pending' => 'در انتظار تایید',
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $regiser_from = [
                'app' => 'اپلیکیشن موبایل',
                'site' => 'سایت'
            ];
            

            $bussines = [];

            $user->profile();
            if($user->company) {
                $bussines['فروشنده'] = URL::to('/admin/companies/edit/' . $user->company->id);
            }
        
            if($user->importer) {
                $bussines['وارد کننده'] = URL::to('/admin/importers/edit/' . $user->importer->id);
            }
            if($user->producer) {
                $bussines['تولید کننده'] = URL::to('/admin/producers/edit/' . $user->producer->id);
            }
            if($user->technician) {
                $bussines['تکنسین'] = URL::to('/admin/technicians/edit/' . $user->technician->id);
            }
            $extra = '<div class="btn-group dropdown-default">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        اطلاعات
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/comments?user_sender=' . $user->id . '">دیدگاه های ارسالی</a></li>';
           
            if(sizeof($bussines)) {
                $extra = $extra . '<li><a href="/admin/comments?user_reciver=' . $user->id . '">دیدگاه های دریافتی</a></li>';
            }
                    
            foreach ($bussines as $key => $value) {
                $extra = $extra . '<li><a href="' . $value . '">' . $key . '</a></li>';
            }
                
            $extra = $extra . '</ul>
                </div>';

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/users/delete/'.$user->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/users/edit/'.$user->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>
                    ' . $extra;

            array_push($data, array(
              'id' => $user->id,
              'first_name' => $user->first_name . ' ' . $user->last_name,
              'email' => $user->email,
              'mobile' => $user->mobile,
              'register_from' => $regiser_from[$user->register_from],
              'status' => $status[$user->status],
              'action' => $action,
          ));
        }

        $users_count = User::where(function ($query) use ($request) {
              $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $users_count,
          'recordsFiltered' => $users_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $user = new User();
        if (!$request->isMethod('post')) {
            return view('admin.users.create', [
                'title' => 'افزودن کاربر جدید',
                'user' => $user
            ]);
        }
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'phone:IR,mobile|unique:users,mobile',
            'email' => 'email|unique:users,email',
            'status' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:2000',
            'password' => 'confirmed|min:6'
        ];

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        if($user->email) {
            $user->email_confirm = 1;
        }
        $user->mobile = Web::phoneFormat($request->get('mobile'));
        if($user->mobile) {
            $user->mobile_confirm = 1;
        }
        $user->status = $request->get('status');
       
        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);
            
            return view('admin.users.create', [
                'title' => 'افزودن کاربر جدید',
                'user' => $user
            ]);
        }

        $user->password = bcrypt($request->get('password'));
    
        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/avatar/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');
            $user->avatar = '/files/avatar/' . $name .'.jpg';
        }

        $user->save();
        Flash::success('کاربر جدید با موفقیت افزوده شد .');
        return redirect('/admin/users/');
    }


    public function edit(Request $request,$id)
    {
        $user = User::find($id);
        if (!$user) {
            Flash::error('کاربر با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/users');
        }
        if (!$request->isMethod('post')) {
            $user->date = Web::jalali($user);
            return view('admin.users.create',[
                'title' => 'ویرایش کاربر',
                'user' => $user,
            ]);
        }
        if($request->get('mobile') != $user->mobile) {
            $rules['mobile'] = 'phone:IR,mobile|unique:users,mobile';
        }

        if($request->get('email') != $user->email) {
            $rules['email'] = 'email|unique:users,email';
        }

        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'status' => 'required',
            'email' => 'email',
            'mobile' => 'phone:IR,mobile',
            'status' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:2000',
            'password' => 'confirmed|min:6'
        ];
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        if($request->get('email') != $user->email) {
            $rules['email'] = 'email|unique:users,email';
            $user->email = $request->get('email');
            $user->email_confirm = 1;
        }

        if($request->get('mobile') != $user->mobile) {
            $rules['mobile'] = 'phone:IR,mobile|unique:users,mobile';
            $user->mobile = Web::phoneFormat($request->get('mobile'));
            $user->mobile_confirm = 1;
        }
        $user->status = $request->get('status');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);
            return view('admin.users.create',[
                'title' => 'ویرایش کاربر',
                'user' => $user,
            ]);
        }
        
        if(!empty($request->get('password'))) {
            $user->password = bcrypt($request->get('password'));
        }

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/avatar/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');
            if (!empty($user->avatar) && Storage::exists(public_path() . $user->avatar)) {
                unlink(public_path() . $user->avatar);
            }
            $user->avatar = '/files/avatar/' . $name .'.jpg';
        }

        $user->save();
        Flash::success('ویرایش کاربر با موفقیت انجام شد .');
        return redirect('/admin/users/');
    }

    public function delete($id)
    {        
        $user = User::find($id);
        if (!$user) {
            Flash::error('کاربر با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/users');
        }
        $user->delete();
                       
        Flash::success('کاربر مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/users/');
    }
}
?>
