<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\skill;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class SkillController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.skills.index', [
                'title' => 'لیست تخصص ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $skills = Skill::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($skills as $skill) {

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/skills/delete/'.$skill->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/skills/edit/'.$skill->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $skill->id,
              'name' => $skill->name,
              'action' => $action,
          ));
        }

        $skills_count = skill::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $skills_count,
          'recordsFiltered' => $skills_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $skill = new skill();
        if (!$request->isMethod('post')) {
            return view('admin.skills.create', [
                'title' => 'افزودن تخصص جدید',
                'skill' => $skill
            ]);
        }

        $rules = [
            'name' => 'required',
        ];

        if (Web::validationCheck($request, $rules)) {
            return Web::validation($request, $rules, '/admin/skills/create');
        }
        $skill->name = $request->get('name');

        $skill->save();

        Flash::success('تخصص جدید با موفقیت افزوده شد .');
        return redirect('/admin/skills/');
    }


    public function edit(Request $request, $id)
    {
        $skill = Skill::find($id);
        if (!$skill) {
            Flash::error('تخصص با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/skills');
        }
        if (!$request->isMethod('post')) {
            return view('admin.skills.create',[
                'title' => 'ویرایش تخصص',
                'skill' => $skill,
            ]);
        }
        $rules = [
            'name' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/skills/edit/'. $skill->id);
        }
        $data = $request->all();
        skill::where('id', $skill->id)->update($data);
        Flash::success('ویرایش تخصص با موفقیت انجام شد .');
        return redirect('/admin/skills/');
    }

    public function delete($id)
    {        
        $skill = skill::find($id);
        if (!$skill) {
            Flash::error('تخصص با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/skills');
        }
        $skill->delete();
                       
        Flash::success('تخصص مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/skills/');
    }


    public function get(Request $request)
    {
        $skills = Skill::where('name', 'like', '%' . $request->get('q') . '%')->get();
        $results = [];

        foreach ($skills as $skill) {
            $results[] = [
                'id' => $skill->id,
                'text' => $skill->name
            ];
        }

        return [
            'items' => $results
        ];
    }
}
 ?>
