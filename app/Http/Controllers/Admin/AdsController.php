<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ads;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;
use Config;

class AdsController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.ads.index', [
                'title' => 'لیست تبلیغات'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          case '2':
              $order_item = 'url';
              break;

          case '3':
              $order_item = 'type';
              break;

          case '4':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $ads = Ads::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('url', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($ads as $ad) {
            $type = [
                'home' => 'صفحه اصلی',
                'company' => 'صفحه شرکت',
                'importer' => 'صفحه وارد کننده',
                'producer' => 'صفحه تولید کننده',
                'technician' => 'صفحه تکنسین',
            ];
            $status = [
                'pending' => 'عدم نمایش',
                'publish' => 'نمایش'
            ];
            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/ads/delete/'.$ad->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/ads/edit/'.$ad->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $ad->id,
              'name' => $ad->name,
              'url' => str_limit($ad->url, 25),
              'type' => $type[$ad->type],
              'status' => $status[$ad->status],
              'action' => $action,
          ));
        }

        $ads_count = Ads::count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $ads_count,
          'recordsFiltered' => $ads_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $ads = new Ads();
        if (!$request->isMethod('post')) {
            return view('admin.ads.create', [
                'title' => 'افزودن تبلیغات جدید',
                'ads' => $ads
            ]);
        }
        $rules = [
            'picture' => 'file|mimes:jpg,png,jpeg|max:1000',
            'url' => 'required|url',
            'type' => 'required',
            'status' => 'required'
        ];
        $ads->url = $request->get('url');
        $ads->name = $request->get('name');
        $ads->type = $request->get('type');
        $ads->status = $request->get('status');

        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);
            return view('admin.ads.create', [
                'title' => 'افزودن تبلیغات جدید',
                'ads' => $ads
            ]);
        }
  
        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $size = config('barghchin.ads_size');
            $name = str_random(40);
            $request->file('picture')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/ads/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name . '.jpg');
            $ads->picture = '/files/ads/' . $name . '.jpg';
        }

        $ads->save();
        Flash::success('تبلیغات جدید با موفقیت افزوده شد .');
        return redirect('/admin/ads/');
    }

    public function edit(Request $request,$id)
    {
        $ads = Ads::find($id);
        if (!$ads) {
            Flash::error('تبلیغ با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/ads');
        }

        if (!$request->isMethod('post')) {
            return view('admin.ads.create',[
                'title' => 'ویرایش تبلیغ',
                'ads' => $ads,
            ]);
        }

        $rules = [
            'picture' => 'file|mimes:jpg,png,jpeg|max:1000',
            'type'=> 'required',
            'url' => 'required|url',
            'status' => 'required'
        ];
        $ads->url = $request->get('url');
        $ads->name = $request->get('name');
        $ads->type = $request->get('type');
        $ads->status = $request->get('status');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);
            return view('admin.ads.create',[
                'title' => 'ویرایش تبلیغ',
                'ads' => $ads,
            ]);
        }
  
        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $size = config('barghchin.ads_size');
            $name = str_random(40);
            $request->file('picture')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/ads/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name . '.jpg');

            if (!empty($ads->picture) && Storage::exists(public_path() . $ads->picture)) {
                unlink(public_path() . $ads->picture);
            }
            $ads->picture = '/files/ads/' . $name . '.jpg';
        }

       $ads->save();
        Flash::success('ویرایش تبلیغ با موفقیت انجام شد .');
        return redirect('/admin/ads/');
    }

    public function delete($id)
    {        
        $ads = Ads::find($id);
        if (!$ads) {
            Flash::error(' تبلیغ با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/ads');
        }
      
        if (!empty($ads->picture) && Storage::exists(public_path() . $ads->picture)) {
            unlink(public_path() . $ads->picture);
        }

        $ads->delete();

        Flash::success(' تبلیغ مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/ads/');
    }
}
?>
