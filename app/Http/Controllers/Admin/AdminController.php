<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Helper\Web;
use Flash;
use URL;
use Auth;
use Hash;

class AdminController extends Controller
{
    public function index(Request $request,$id = null)  {
        $user = $request->user;
        if($user['super_admin'] != "yes") {
            return redirect('/admin');
        }
        if (!$request->isMethod('post')) {
            return view('admin.admins.index', [
                'title' => 'لیست مدیران',
                'id' => $id
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'first_name';
              break;

          case '2':
              $order_item = 'email';
              break;

          case '3':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $admins = Admin::where(function ($query) use ($request) {
              $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'));

        if($id) {
            $admins = $admins->where('id', $id);
        }

        $admins = $admins->get(); 

        foreach ($admins as $admin) {
            $status = [
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/admins/delete/'.$admin->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/admins/edit/'.$admin->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $admin->id,
              'first_name' => $admin->first_name . ' ' . $admin->last_name,
              'email' => $admin->email,
              'status' => $status[$admin->status],
              'action' => $action,
          ));
        }

        $admins_count = Admin::where(function ($query) use ($request) {
              $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $admins_count,
          'recordsFiltered' => $admins_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $user = $request->user;
        if($user->super_admin != "yes") {
            return redirect('/admin');
        }
        $admin = new Admin();
        if (!$request->isMethod('post')) {
            return view('admin.admins.create', [
                'title' => 'افزودن مدیر جدید',
                'admin' => $admin
            ]);
        }
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:admins,email',
            'status' => 'required',
            'password' => 'confirmed|min:6',
            'super_admin' => 'required',
        ];

        $admin->first_name = $request->get('first_name');
        $admin->last_name = $request->get('last_name');
        $admin->email = $request->get('email');
        $admin->status = $request->get('status');
        $admin->super_admin = $request->get('super_admin');

        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);
            return view('admin.admins.create', [
                'title' => 'افزودن مدیر جدید',
                'admin' => $admin
            ]);
        }

        $admin->password = bcrypt($request->get('password'));
  
        $admin->save();
        Flash::success('مدیر جدید با موفقیت افزوده شد .');
        return redirect('/admin/admins/');
    }


    public function edit(Request $request,$id)
    {
        $user = $request->user;
        $admin = Admin::find($id);
        if (!$admin) {
            Flash::error('مدیر با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/admins');
        }

        if($user['super_admin'] != "yes" && $admin->id != $user['id']) {
            return redirect('/admin');
        }
        if (!$request->isMethod('post')) {
            return view('admin.admins.create',[
                'title' => 'ویرایش مدیر',
                'admin' => $admin,
            ]);
        }
        $rules = [
            'email' => 'email',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'confirmed|min:6'
        ];

        $admin->first_name = $request->get('first_name');
        $admin->last_name = $request->get('last_name');
        $admin->email = $request->get('email');

        if($user['super_admin'] == "yes") {
            $admin->status = $request->get('status');
            $admin->super_admin = $request->get('super_admin');
        }

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.admins.create',[
                'title' => 'ویرایش مدیر',
                'admin' => $admin,
            ]);
        }
        $admin->save();
        if(!empty($request->get('password'))) {
            if (Hash::check($request->get('current_password'), $admin->password)) {
                 $admin->password = bcrypt($request->get('password'));
            } else {
                Flash::error('رمز عبور کنونی به درستی وارد نشده است .');
            }
           
        }

        $admin->save();
        Flash::success('ویرایش مدیر با موفقیت انجام شد .');
        return redirect('/admin/admins/');
    }

    public function delete(Request $request, $id)
    {      
        $user = $request->user;
        if($user->super_admin != "yes") {
            return redirect('/admin');
        }  
        $admin = Admin::find($id);
        if (!$admin) {
            Flash::error('مدیر با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/admins');
        }
        $current = Auth::guard('admin')->user();
        if($admin->id == $current->id) {
            Flash::error("شما قادر به حذف اکانت خود نمی باشید ");
        } else {
            $admin->delete();
        }
                       
        Flash::success('مدیر مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/admins/');
    }
}
?>
