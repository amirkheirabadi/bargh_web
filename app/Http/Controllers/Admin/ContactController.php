<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Admin;
use App\Helper\Web;
use Flash;
use URL;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.contacts.index', [
                'title' => 'لیست گزارشات'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;
 
          case '2':
              $order_item = 'email';
              break;

          case '3':
              $order_item = 'type';
              break;

          case '4':
              $order_item = 'created_at';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $contacts = Contact::where(function ($query) use ($request) {
              $query->where('email', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($contacts as $contact) {
            $type = [
                'contact' => 'ارتباط با ما',
                'grievance' => 'شکایت از کسب و کار',
            ];

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/contacts/delete/'.$contact->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/contacts/show/'.$contact->id) .'" class="btn btn-success"><i class="fa fa-eye"></i>
                      </a>
                    </div>';

            $jalali = Web::jalali($contact);

            array_push($data, array(
              'id' => $contact->id,
              'name' => $contact->name,
              'email' => $contact->email,
              'type' => $type[$contact->type],
              'created_at' => $jalali['created_at']['time'],
              'action' => $action,
          ));
        }


        $contacts_count = Contact::where(function ($query) use ($request) {
              $query->where('mobile', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $contacts_count,
          'recordsFiltered' => $contacts_count,
          'data' => $data,
        );
    }

    public function show(Request $request,$id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            Flash::error('گزارش با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/contacts');
        }
        $contact->status = "read";
        $contact->save();
       
        return view('admin.contacts.show',[
            'title' => 'نمایش گزارش',
            'contact' => $contact,
        ]);
    }

    public function delete($id)
    {        
        $contact = Contact::find($id);
        if (!$contact) {
            Flash::error('گزارش با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/contacts');
        }
        $contact->delete();
                       
        Flash::success('گزارش مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/contacts/');
    }
}
?>
