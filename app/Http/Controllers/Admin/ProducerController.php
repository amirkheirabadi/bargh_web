<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Producer;
use App\City;
use App\Province;
use App\Country;
use App\Picture;
use App\Product;
use App\Comment;
use App\Endrose;
use App\Helper\Web;
use Flash;
use DB;
use URL;
use Image;
use Storage;
use Config;

class ProducerController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.producers.index', [
                'title' => 'لیست تولید کننده ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          case '2':
              $order_item = 'user_id';
              break;

          case '3':
              $order_item = 'mobile';
              break;

          case '4':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $producers = Producer::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($producers as $producer) {
            $status = [
                'pending' => 'در انتظار تایید',
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $extra = '<div class="btn-group dropdown-default">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    اطلاعات
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                <li><a href="/admin/comments?entity=producer&entity_id=' . $producer->id . '">لیست دیدگاه ها</a></li>
                <li><a href="/admin/articles?entity=producer&entity_id=' . $producer->id . '">لیست مقالات</a></li>
                <li><a href="/admin/agents?entity=producer&entity_id=' . $producer->id . '">لیست نمایندگی ها</a></li>
                </ul>
            </div>';

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/producers/delete/'.$producer->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                        <a href="'. URL::to('/admin/pictures/producer/'.$producer->id) .'" class="btn btn-success"><i class="fa fa-picture-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/producers/edit/'.$producer->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>
                ' . $extra;
            $user = $producer->user;
            array_push($data, array(
              'id' => $producer->id,
              'name' => $producer->name,
              'user' => '<a href="/admin/users/'. $producer->user_id .'">'.$user['first_name'] . ' ' . $user['last_name'].'</a>' ,
              'mobile' => $producer->mobile,
              'status' => $status[$producer->status],
              'action' => $action,
          ));
        }

        $producers_count = Producer::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $producers_count,
          'recordsFiltered' => $producers_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $producer = Producer::find($id);
        if (!$producer) {
            Flash::error('تولید کننده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/producers');
        }
        $provinces = Province::all();
        $producer->address();

        $address_city = City::find($producer->address->city_id);
        if (!$request->isMethod('post')) {
            return view('admin.producers.create',[
                'title' => 'ویرایش تولید کننده',
                'producer' => $producer,
                'provinces' => $provinces,
                'address_city' => $address_city,
            ]);
        }

        $rules = [
            'name' => 'required',
            'mobile' => 'required|phone:IR,mobile',
            'phone' => 'min:4,max:8',
            'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'owner' => 'required',
            'status' => 'required',
            'address_city' => 'required',
            'address_province' => 'required',
            'address' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:2000',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];

        $producer->name = $request->get('name');
        $producer->mobile = Web::phoneFormat($request->get('mobile'));
        $producer->phone = Web::phoneFormat($request->get('phone'));
        $producer->owner = $request->get('owner');
        $producer->website = $request->get('website');
        $producer->description = $request->get('description');
        $producer->status = $request->get('status');
        
        if ($request->get('categories')) {
            $producer->categories()->sync($request->get('categories'));   
        }
       
        $producer->address->province_id = $request->get('address_province');
        $producer->address->city_id = $request->get('address_city');
        $producer->address->address = $request->get('address');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.producers.create',[
                'title' => 'ویرایش تولید کننده',
                'producer' => $producer,
                'provinces' => $provinces,
                'address_city' => $address_city,
            ]);
        } 

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/users/' . $name . '.jpg');

            unlink(public_path()  .'/temp/' . $name . '.jpg');
            if (!empty($producer->avatar) && Storage::exists(public_path() . $producer->avatar)) {
                unlink(public_path()  . $producer->avatar);
            }
            $producer->avatar = '/files/users/' . $name . '.jpg';            
        }

        if ($request->hasFile('catalog') && $request->file('catalog')->isValid()) {
            $name = str_random(40);
            $request->file('catalog')->move('files/catalog/', $name.'.pdf');

            if (!empty($producer->catalog) && Storage::exists(public_path() . $producer->catalog)) {
                unlink(public_path() . $producer->catalog);
            }
            $producer->catalog = '/files/catalog/' . $name . '.pdf';
        }
        
        if(!empty($request->get('lat'))) {
            $producer->address->lat = $request->get('lat');
            $producer->address->long = $request->get('long');
        }

        $producer->address->save();
        unset($producer->address);
        $producer->save();
        Flash::success('ویرایش تولید کننده با موفقیت انجام شد .');
        return redirect('/admin/producers/');
    }

    public function delete($id)
    {        
        $producer = producer::find($id);
        if (!$producer) {
            Flash::error(' تولید کننده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/producers');
        }

        Address::where('entity', 'producer')->where('entity_id', $producer->id)->delete();
        $producer->brands()->sync([]);

        if (!empty($producer->avatar) && Storage::exists(public_path() . $producer->avatar)) {
            unlink(public_path()  . $producer->avatar);
        }

        if (!empty($producer->catalog) && Storage::exists(public_path() . $producer->catalog)) {
            unlink(public_path()  . $producer->catalog);
        }

        $pictures  = Picture::where('entity', 'company')->where('entity_id', $producer->id)->get();
        foreach ($pictures as $picture) {
            if (Storage::exists(public_path(). $picture->url)) {
                unlink(public_path() . $picture->url);
            }
            $picture->delete();
        }

        $products = Product::where('user_id', $producer->user_id)->get();

        $sizeConfig = Config::get('baghchin.product_thumb_sizes');
        foreach ($products as $product) {
            if (!empty($product->thumb)) {
                foreach ($sizeConfig as $key => $value) {
                    if (Storage::exists(public_path() . $product->thumb . '_' . $key . '.jpg')){
                        unlink(public_path()  . $product->thumb . '_' . $key . '.jpg');
                    }
                }
            }
            Comment::where('entity', 'product')->where('entity_id', $product->id)->delete();
        }
        Comment::where('entity', 'producer')->where('entity_id', $producer->id)->delete();
        Endrose::where('entity', 'producer')->where('entity_id', $producer->id)->delete();
        
        $producer->delete();

        Flash::success(' تولید کننده مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/producers/');
    }

    public function category(Request $request, $id)
    {
        if (!$request->isMethod('post')) {
            if(!$request->get('id')) {
                return redirect('/admin/producers/edit/' . $id);
            }

            DB::delete('delete from categories_producers where producer_id = ?  AND id = ?', array($id,$request->get('id')));

            return redirect('/admin/producers/edit/' . $id);
        }

         $rules = [
            'category' => 'required',
            'highlight' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/producers/edit/'. $id);
        }

        $producer = Producer::find($id);
        $producer->categories()->attach([$request->get('category') => ['highlight' => $request->get('highlight')]]);
        $producer->save();
        return redirect('/admin/producers/edit/' . $id);
    }

    public function highlight($type, $id, $entity_id, $data) {
        switch ($type) {
            case 'category':
                Producer::find($id)->categories()->updateExistingPivot($entity_id, ['highlight' => $data]);
                break;
        }

        return 'ok';
    }
}
?>
