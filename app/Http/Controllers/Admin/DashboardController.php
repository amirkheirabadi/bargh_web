<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Storage;
use App\Category;
use App\Brand;
use App\Admin;
use App\User;
use App\Company;
use App\Importer;
use App\Producer;
use App\Technician;

use App\Helper\Web;

class DashboardController extends Controller
{
    public function dashboard()
    {   
        $stat = [];

        $stat['admins'] = Admin::count();
        $stat['users'] = User::count();
        $stat['categories'] = Category::count();
        $stat['brands'] = Brand::count();

        $stat['companies'] = Company::count();
        $stat['importers'] = Importer::count();
        $stat['producers'] = Producer::count();
        $stat['technicians'] = Technician::count();

        return view('admin.dashboard', [
            'title' => 'داشبورد برق چین',
            'stat' => $stat
        ]);
    }

    public function companies()
    {
        return $this->belongsToMany('App\Company', 'company_brand', 'brand_id', 'company_id');
    }

    public function upload(Request $request)
    {
        $rules = [
            'file' => 'required|file|mimes:jpg,png,jpeg|max:5000',
        ];
    
        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return '';
        }
        $name = "";
         if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $name = str_random(40);;


            $request->file('file')->move('files/others/', $name.'.jpg');

            $name = config('app.url') . '/files/others/' . $name . '.jpg';
        }
        return $name;
    }
}
