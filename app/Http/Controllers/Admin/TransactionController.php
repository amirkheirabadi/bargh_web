<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.transactions.index', [
                'title' => 'لیست تراکنش ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'user_id';
              break;

            case '2':
              $order_item = 'price';
              break;

            case '3':
              $order_item = 'status';
              break;

            case '4':
              $order_item = 'created_at';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $transactions = Transaction::orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($transactions as $transaction) {
            $status = [
                'pending' => 'در انتظار پرداخت',
                'paid' => 'پرداخت شده'
            ];
            // <a href="'. URL::to('/admin/transactions/show/'.$transaction->id) .'" class="btn btn-success"><i class="fa fa-eye"></i> </a>

            $action = '<div class="btn-group">
                      
                     
                    </div>';
            $user = $transaction->owner;
            $date = Web::jalali($transaction);

            array_push($data, array(
              'id' => $transaction->id,
              'user' => '<a href="/admin/users/' . $user['id'] . '">' . $user['first_name'] . ' ' . $user['last_name']  . '</a>',
              'price' => $transaction->price . ' تومان ',
              'status' => $status[$transaction->status],
              'date' => $date['created_at']['time'],
              'action' => $action,
          ));
        }

        $transactions_count = Transaction::count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $transactions_count,
          'recordsFiltered' => $transactions_count,
          'data' => $data,
        );
    }

    public function show(Request $request, $id)
    {
        $transaction = Transaction::find($id);
        if (!$transaction) {
            Flash::error('تراکنش با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/transactions');
        }
        return view('admin.transactions.show',[
            'title' => 'نمایش تراکنش',
            'transaction' => $transaction,
        ]);
    }
}
 ?>
