<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
use App\News;
use Auth;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.news.index', [
                'title' => 'لیست اخبار'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'title';
              break;

          case '2':
              $order_item = 'owner';
              break;

          case '3':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $newses = News::where(function ($query) use ($request) {
              $query->where('title', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        if(!empty($request->get('columns')['1']['search']['value']) && $request->get('columns')['1']['search']['value'] != "all") {
            $newses = $newses->where('status', $request->get('columns')['1']['search']['value']);
        }  

        foreach ($newses as $news) {
            $status = [
                'pending' => 'در انتظار انتشار',
                'publish' => 'منتشر شده'
            ];

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/news/delete/'.$news->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/news/edit/'.$news->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $news->id,
              'title' => $news->title,
              'owner' => $news->owner->first_name . ' ' . $news->owner->last_name,
              'status' => $status[$news->status],
              'action' => $action,
          ));
        }

        $news_count = News::where(function ($query) use ($request) {
              $query->where('title', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $news_count,
          'recordsFiltered' => $news_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $news = new News();
        if (!$request->isMethod('post')) {
            return view('admin.news.create', [
                'title' => 'افزودن خبر جدید',
                'news' => $news
            ]);
        }
        $rules = [
            'title' => 'required',
            'text' => 'required',
            'status' => 'required',
            'thumb' => 'file|mimes:jpg,png,jpeg|max:1000',
        ];

        $news->title = $request->get('title');
        $news->text = $request->get('text');
        $news->admin_id = Auth::guard('admin')->user()->id;
        $news->status = $request->get('status');

        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);
            return view('admin.news.create', [
                'title' => 'افزودن خبر جدید',
                'news' => $news
            ]);
        }

        if ($request->hasFile('thumb') && $request->file('thumb')->isValid()) {
            $name = str_random(40);
            $request->file('thumb')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.news_thumb');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/news/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');
            $news->thumb = '/files/news/' . $name .'.jpg';
        }
  
        $news->save();
        Flash::success('خبر جدید با موفقیت افزوده شد .');
        return redirect('/admin/news/');
    }


    public function edit(Request $request,$id)
    {
        $news = News::find($id);
        if (!$news) {
            Flash::error('خبر با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/news');
        }
        if (!$request->isMethod('post')) {
            return view('admin.news.create',[
                'title' => 'ویرایش خبر',
                'news' => $news,
            ]);
        }
        $rules = [
            'title' => 'required',
            'text' => 'required',
            'status' => 'required',
            'thumb' => 'file|mimes:jpg,png,jpeg|max:1000',
        ];
        $news->title = $request->get('title');
        $news->text = $request->get('text');
        $news->status = $request->get('status');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.news.create',[
                'title' => 'ویرایش خبر',
                'news' => $news,
            ]);
        }

        if(!empty($request->get('password'))) {
            $admin->password = bcrypt($request->get('password'));
        }

       if ($request->hasFile('thumb') && $request->file('thumb')->isValid()) {
            $name = str_random(40);
            $request->file('thumb')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.news_thumb');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/news/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');

            if (!empty($news->thumb) && Storage::exists(public_path() . $news->thumb)) {
                unlink(public_path() . $news->thumb);
            }
            $news->thumb = '/files/news/' . $name .'.jpg';
        }

        $news->save();
        Flash::success('ویرایش خبر با موفقیت انجام شد .');
        return redirect('/admin/news/');
    }

    public function delete($id)
    {        
        $news = News::find($id);
        if (!$news) {
            Flash::error('خبر با این شناسه در سیستم یافت نشد .');
            return redirect('/news/news');
        }
        if (!empty($news->thumb) && Storage::exists(public_path() . $news->thumb)) {
            unlink(public_path() . $news->thumb);
        }
        $news->delete();
                       
        Flash::success('خبر مورد نظر با موفقیت حذف شد .');
        return redirect('/news/news/');
    }
}
?>
