<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\City;
use App\Province;
use App\Country;
use App\Product;
use App\Address;
use App\Comment;
use App\Picture;
use App\Endrose;
use App\Helper\Web;
use Flash;
use URL;
use DB;
use Image;
use Storage;
use Config;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.companies.index', [
                'title' => 'لیست فروشنده ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          case '2':
              $order_item = 'user_id';
              break;

          case '3':
              $order_item = 'mobile';
              break;

          case '4':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $companies = Company::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($companies as $company) {
            $status = [
                'pending' => 'در انتظار تایید',
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $extra = '<div class="btn-group dropdown-default">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        اطلاعات
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/comments?entity=company&entity_id=' . $company->id . '">لیست دیدگاه ها</a></li>
                    <li><a href="/admin/articles?entity=company&entity_id=' . $company->id . '">لیست مقالات</a></li>
                    </ul>
                </div>';

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/companies/delete/'.$company->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                        <a href="'. URL::to('/admin/pictures/company/'.$company->id) .'" class="btn btn-success"><i class="fa fa-picture-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/companies/edit/'.$company->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>
                    ' . $extra;
            $user = $company->user;
            array_push($data, array(
              'id' => $company->id,
              'name' => $company->name,
              'user' => '<a href="/admin/users/'. $company->user_id .'">'.$user['first_name'] . ' ' . $user['last_name'].'</a>' ,
              'mobile' => $company->mobile,
              'status' => $status[$company->status],
              'action' => $action,
          ));
        }

        $companies_count = Company::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $companies_count,
          'recordsFiltered' => $companies_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $company = Company::find($id);
        if (!$company) {
            Flash::error('فروشنده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/companies');
        }
        $provinces = Province::all();
        $company->address();

        $address_city = City::find($company->address->city_id);
        if (!$request->isMethod('post')) {
            return view('admin.companies.create',[
                'title' => 'ویرایش فروشنده',
                'company' => $company,
                'provinces' => $provinces,
                'address_city' => $address_city,
            ]);
        }


        $rules = [
            'name' => 'required',
            'mobile' => 'required|phone:IR,mobile',
            'phone' => 'min:4,max:8',
            'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'owner' => 'required',
            'email' => 'email',
            'status' => 'required',
            'address_city' => 'required',
            'address_province' => 'required',
            'address' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:2000',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];
        $company->name = $request->get('name');
        $company->mobile = Web::phoneFormat($request->get('mobile'));
        $company->phone = Web::phoneFormat($request->get('phone'));
        $company->owner = $request->get('owner');
        $company->email = $request->get('email');
        $company->fax = $request->get('fax');
        $company->website = $request->get('website');
        $company->description = $request->get('description');
        $company->status = $request->get('status');
        $company->highlight = $request->get('hightlight');
        $company->address->province_id = $request->get('address_province');
        $company->address->city_id = $request->get('address_city');
        $company->address->address = $request->get('address');
        
        if (!empty($request->get('lat'))){
            $company->address->lat = $request->get('lat');
            $company->address->long = $request->get('long');
        }

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.companies.create',[
                'title' => 'ویرایش فروشنده',
                'company' => $company,
                'provinces' => $provinces,
                'address_city' => $address_city,
            ]);
        }
        
        if(!empty($request->get('brands'))) {
            $company->brands()->sync($request->get('brands'));    
        }

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/users/' . $name . '.jpg');

            unlink(public_path() .'/temp/' . $name . '.jpg');
            if (!empty($company->avatar) && Storage::exists(public_path() . $company->avatar)) {
                unlink(public_path() . $company->avatar);
            }
            $company->avatar = '/files/users/' . $name . '.jpg';
        }

        if ($request->hasFile('catalog') && $request->file('catalog')->isValid()) {
            $name = str_random(40);
            $request->file('catalog')->move('files/catalog/', $name.'.pdf');

            if (!empty($company->catalog) && Storage::exists(public_path() . $company->catalog)) {
                unlink(public_path() . $company->catalog);
            }
            $company->catalog = '/files/catalog/' . $name . '.pdf';
        }
        
        $company->address->save();
        unset($company->address);
        $company->save();
        Flash::success('ویرایش فروشنده با موفقیت انجام شد .');
        return redirect('/admin/companies/');
    }

    public function delete($id)
    {        
        $company = Company::find($id);
        if (!$company) {
            Flash::error(' فروشنده با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/companies');
        }

        Address::where('entity', 'company')->where('entity_id', $company->id)->delete();
        $company->brands()->sync([]);

        if (!empty($company->avatar) && Storage::exists(public_path() . $company->avatar)) {
            unlink(public_path() . $company->avatar);
        }

        if (!empty($company->catalog) && Storage::exists(public_path() . $company->catalog)) {
            unlink(public_path() . $company->catalog);
        }

        $pictures  = Picture::where('entity', 'company')->where('entity_id', $company->id)->get();
        foreach ($pictures as $picture) {
            if (Storage::exists(public_path(). $picture->url)) {
                unlink(public_path(). $picture->url);
            }
            $picture->delete();
        }

        $products = Product::where('user_id', $company->user_id)->get();

        $sizeConfig = Config::get('baghchin.product_thumb_sizes');
        foreach ($products as $product) {
            if (!empty($product->thumb)) {
                foreach ($sizeConfig as $key => $value) {
                    if (Storage::exists(public_path() . $product->thumb . '_' . $key . '.jpg')){
                        unlink(public_path() . $product->thumb . '_' . $key . '.jpg');
                    }
                }
            }
            Comment::where('entity', 'product')->where('entity_id', $product->id)->delete();
        }
        Comment::where('entity', 'company')->where('entity_id', $company->id)->delete();
        Endrose::where('entity', 'company')->where('entity_id', $company->id)->delete();
        

        $company->delete();

        Flash::success(' فروشنده مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/companies/');
    }

    public function brand(Request $request, $id)
    {
        if (!$request->isMethod('post')) {
            if(!$request->get('id')) {
                return redirect('/admin/companies/edit/' . $id);
            }

            DB::delete('delete from companies_brands where company_id = ?  AND id = ?', array($id,$request->get('id')));

            return redirect('/admin/companies/edit/' . $id);
        }

         $rules = [
            'brand' => 'required',
            'highlight' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/companies/edit/'. $id);
        }

        $company = Company::find($id);
        $company->brands()->attach([$request->get('brand') => ['highlight' => $request->get('highlight')]]);
        $company->save();
        return redirect('/admin/companies/edit/' . $id);
    }

    public function highlight($type, $id, $entity_id, $data) {
        switch ($type) {
            case 'brand':
                Company::find($id)->brands()->updateExistingPivot($entity_id, ['highlight' => $data]);
                break;
        }

        return 'ok';
    }
}
?>
