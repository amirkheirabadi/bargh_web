<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Message;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;
use DB;
use jDate;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.messages.index', [
                'title' => 'لیست گفتگو ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = 'DESC';
        if($order['dir']) {
            $order_direction = $order['dir'];
        }
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'user_sender';
              break;

          case '2':
              $order_item = 'user_reciver';
              break;

          case '3':
              $order_item = 'created_at';
              break;

          default:
            $order_item = 'updated_at';
              break;
        }
        $data = [];

        $messages = DB::select("SELECT * FROM  `messages` as `a`
        INNER JOIN (
            SELECT MAX(  `id` ) AS id
        FROM  `messages` AS  `alt` 
        GROUP BY  least(`user_sender` ,  `user_reciver`), greatest(`user_sender` ,  `user_reciver`)
        )b ON a.id = b.id ORDER BY a.{$order_item} {$order_direction} LIMIT {$request->get('length')} OFFSET {$request->get('start')}");

        foreach ($messages as $message) {
       
            $action = '<div class="btn-group">
                    <a href="'. URL::to('/admin/messages/delete/'.$message->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                    </a>
                    <a href="'. URL::to('/admin/messages/edit/'.$message->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                    </a>
                </div>';

            $date = jDate::forge($message->created_at);

            $userSender = User::find($message->user_sender);
            $userReciver = User::find($message->user_reciver);

            array_push($data, array(
              'id' => $message->id,
              'user_sender' => $userSender->first_name . ' ' . $userSender->last_name,
              'user_reciver' => $userReciver->first_name . ' ' . $userReciver->last_name,
              'created_at' => $date->format('datetime'),
              'action' => $action,
          ));
        }

        $messages_count = Message::where(function ($query) use ($request) {
              $query->where('text', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $messages_count,
          'recordsFiltered' => $messages_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $message = Message::find($id);
        if (!$message) {
            Flash::error('پیامی با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/message');
        }
        if (!$request->isMethod('post')) {
            $messages = Message::where(function ($query) use ($message) {
              $query->where('user_sender', $message->user_sender)->where('user_reciver', $message->user_reciver);
            })->orWhere(function ($query) use ($message) {
              $query->where('user_sender', $message->user_reciver)->where('user_reciver', $message->user_sender);
            })->get();
            return view('admin.messages.create',[
                'title' => 'ویرایش پیام',
                'message' => $message,
                'messages' => $messages
            ]);
        }
        $rules = [
            'text' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/message/edit/'. $message->id);
        }
        $data = $request->all();

        Message::where('id', $message->id)->update($data);
        Flash::success('ویرایش پیام با موفقیت انجام شد .');
        return redirect('/admin/messages/');
    }

    public function delete($id)
    {        
        $message = Message::find($id);
        if (!$message) {
            Flash::error('پیامی با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/messages');
        }
        $message->delete();
                       
        Flash::success('پیام مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/messages/');
    }
}
 ?>
