<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\City;
use App\Company;
use App\Producer;
use App\Province;
use App\Country;
use App\Picture;
use App\Category;
use App\Helper\Web;
use DB;
use Flash;
use URL;
use Image;
use Storage;
use jDate;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.products.index', [
                'title' => 'لیست محصولات'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;
        

          case '4':
              $order_item = 'category_id';
              break;


          case '5':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $products = Product::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($products as $product) {
            $status = [
                'pending' => 'در انتظار تایید',
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $type = [
                'company' => 'فروشنده',
                'producer' => 'تولید کننده'
            ];

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/products/delete/'.$product->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/pictures/product/'.$product->id) .'" class="btn btn-success"><i class="fa fa-picture-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/products/edit/'.$product->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';
            
            switch ($product->entity) {
                case 'company':
                        $owner = Company::find($product->entity_id);
                        $owner = $owner->name;
                        $link = '<a href="/admin/companies/edit/' . $product->entity_id . '">' . $owner . '( ' . $type[$product->entity] . ' )</a>';
                    break;
                
                case 'producer':
                    $owner = Producer::find($product->entity_id);
                    $owner = $owner->name;
                    $link = '<a href="/admin/producers/edit/' . $product->entity_id . '">' . $owner . '( ' . $type[$product->entity] . ' )</a>';
                break;
            }
            
            array_push($data, array(
              'id' => $product->id,
              'name' => $product->name,
              'owner' => $link,
              'category_id' => '<a href="/admin/categories/edit/' . $product->category->id . '">'. $product->category->name .'</a>',
              'status' => $status[$product->status],
              'action' => $action,
          ));
        }

        $products_count = Product::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $products_count,
          'recordsFiltered' => $products_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {

        $product = Product::find($id);
        if (!$product) {
            Flash::error('محصول با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/products');
        }
        $category = Category::find($product->category_id);
        if (!$request->isMethod('post')) {

            if(!empty($product->price_expire)) {
                $date = jDate::forge($product->price_expire);
                $product->price_expire = $date->format('datetime');
            }

            return view('admin.products.create',[
                'title' => 'ویرایش محصول',
                'product' => $product,
                'category' => $category
            ]);
        }
        $rules = [
            'name' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'price_type' => 'required',
            'status' => 'required',
            'price_expire' => 'date',
            'thumb' => 'file|mimes:jpg,png,jpeg|max:1000',
        ];

        $product->name = $request->get('name');
        $product->category_id = $request->get('category_id');
        $product->description = $request->get('description');
        $product->price_type = $request->get('price_type');
        $product->price_expire = $request->get('price_expire');
        $product->status = $request->get('status');
    
        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.products.create',[
                'title' => 'ویرایش محصول',
                'product' => $product,
                'category' => $category
            ]);
        }

        switch ($product->price_type) {
            case 'notknow':
                $product->price = 0;
                $product->price_to = 0;
                break;

             case 'relative':
                $product->price = (int) $request->get('price');
                $product->price_to = (int) $request->get('price_to');
                break;

            case 'absolute':
                $product->price = (int) $request->get('price');
                $product->price_to = 0;
                break;           

        }        

        if (!empty($product->price_expire)) {
            $date = explode("-", $request->get("price_expire"));
            $date = \Morilog\Jalali\jDateTime::toGregorian($date[0], $date[1], $date[2]);
            $product->price_expire = implode("-",$date);
        }

        if ($request->hasFile('thumb') && $request->file('thumb')->isValid()) {
            $name = str_random(40);
            $request->file('thumb')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $sizes= config('barghchin.product_thumb_sizes');
            foreach ($sizes as $key => $size) {
                $img->resize($size[0], $size[1]);
                $img->save(public_path().'/files/products/' . $name . '_' . $key . '.jpg');
            }
            

            unlink(public_path() .'/temp/' . $name . '.jpg');
            if (!empty($product->thumb) && Storage::exists(public_path() . $product->thumb)) {
                unlink(public_path() . $product->thumb);
            }
            $product->thumb = '/files/products/' . $name;            
        }

        $product->save();
        Flash::success('ویرایش محصول با موفقیت انجام شد .');
        return redirect('/admin/products/');
    }

    public function delete($id)
    {        
        $product = product::find($id);
        if (!$product) {
            Flash::error('محصول با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/products');
        }

        $pictures = Picture::where('entity', 'product')->where('entity_id', $product->id)->get();

        foreach ($pictures as $pic) {
            if (!empty($pic->url) && Storage::exists(public_path() . $pic->url)) {
                unlink(public_path() . $pic->url);
            }
        }
        
        $sizes= config('barghchin.product_thumb_sizes');
        if(!empty($product->url)) {
            foreach ($sizes as $key => $size) {
                if (Storage::exists(public_path() . $pic->url . '_' . $key . '.jpg')) {
                    unlink(public_path() . $pic->url . '_' . $key . '.jpg');
                }
            }
        }
            
        $product->delete();

        Flash::success('محصول مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/products/');
    }
}
?>
