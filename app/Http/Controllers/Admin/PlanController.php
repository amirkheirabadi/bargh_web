<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Plan;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class PlanController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.plans.index', [
                'title' => 'لیست پلن های اشتراک'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

            case '2':
              $order_item = 'duration';
              break;

            case '3':
              $order_item = 'price';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $plans = Plan::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($plans as $plan) {

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/plans/delete/'.$plan->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/plans/edit/'.$plan->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $plan->id,
              'name' => $plan->name,
              'duration' => $plan->duration,
              'price' => $plan->price . ' ریال',
              'action' => $action,
          ));
        }

        $plans_count = Plan::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $plans_count,
          'recordsFiltered' => $plans_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $plan = new Plan();
        if (!$request->isMethod('post')) {
            return view('admin.plans.create', [
                'title' => 'افزودن پلن جدید',
                'plan' => $plan
            ]);
        }

        $rules = [
            'name' => 'required',
            'duration' => 'required',
            'price' => 'required',
        ];

        if (Web::validationCheck($request, $rules)) {
            return Web::validation($request, $rules, '/admin/plans/create');
        }
        $plan->name = $request->get('name');
        $plan->duration = $request->get('duration');
        $plan->price = $request->get('price');

        $plan->save();

        Flash::success('پلن جدید با موفقیت افزوده شد .');
        return redirect('/admin/plans/');
    }


    public function edit(Request $request, $id)
    {
        $plan = Plan::find($id);
        if (!$plan) {
            Flash::error('پلن با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/plans');
        }
        if (!$request->isMethod('post')) {
            return view('admin.plans.create',[
                'title' => 'ویرایش پلن',
                'plan' => $plan,
            ]);
        }
        $rules = [
            'name' => 'required',
            'duration' => 'required',
            'price' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/plans/edit/'. $plan->id);
        }
        $plan->name = $request->get('name');
        $plan->duration = $request->get('duration');
        $plan->price = $request->get('price');
        $plan->save();
        Flash::success('ویرایش پلن با موفقیت انجام شد .');
        return redirect('/admin/plans');
    }

    public function delete($id)
    {        
        $plan = Plan::find($id);
        if (!$plan) {
            Flash::error('پلن با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/plans');
        }
        $plan->delete();
                       
        Flash::success('پلن مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/plans/');
    }
}
 ?>
