<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Technician;
use App\Address;
use App\City;
use App\Province;
use App\Helper\Web;
use Flash;
use URL;
use DB;
use Image;
use Storage;

class TechniciansController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.technicians.index', [
                'title' => 'لیست تکنسین ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          case '2':
              $order_item = 'user_id';
              break;

          case '3':
              $order_item = 'created_at';
              break;

          case '4':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
            break;
        }
        $data = [];

        $technicians = Technician::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('phone', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($technicians as $technician) {
            $status = [
                'pending' => 'در انتظار تایید',
                'suspend' => 'مسدود',
                'active' => 'فعال'
            ];

            $extra = '<div class="btn-group dropdown-default">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    اطلاعات
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                <li><a href="/admin/comments?entity=technician&entity_id=' . $technician->id . '">لیست دیدگاه ها</a></li>
                <li><a href="/admin/articles?entity=technician&entity_id=' . $technician->id . '">لیست مقالات</a></li>
                </ul>
            </div>';
    
            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/technicians/delete/'.$technician->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/pictures/technician/'.$technician->id) .'" class="btn btn-success"><i class="fa fa-picture-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/technicians/edit/'.$technician->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>
                    ' . $extra;

            $date = Web::jalali($technician);
            $user = $technician->user;
            array_push($data, array(
              'id' => $technician->id,
              'name' => $technician->name,
              'user' => '<a href="/admin/users/'. $technician->user_id .'">'.$user['first_name'] . ' ' . $user['last_name'].'</a>' ,
              'created_at' => $date['created_at']['time'],
              'status' => $status[$technician->status],
              'action' => $action,
          ));
        }

        $technicians_count = Technician::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('phone', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $technicians_count,
          'recordsFiltered' => $technicians_count,
          'data' => $data,
        );
    }

    public function edit(Request $request,$id)
    {
        $technician = Technician::find($id);
        if (!$technician) {
            Flash::error('تکنسین با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/technicians');
        }
        $city = 0;
        $province = 0;
        $provinces = Province::all();
        $technician->address();
        if ($technician->city_id) {
            $city = City::find($technician->city_id);
            $province = province::find($city->province_id); 
        }
        $address_city = City::find($technician->address->city_id);
        if (!$request->isMethod('post')) {
            return view('admin.technicians.create',[
                'title' => 'ویرایش تکنسین',
                'technician' => $technician,
                'city' => $city,
                'provinces' => $provinces,
                'province' => $province,
                'skills' => $technician->skills,
                'address_city' => $address_city
            ]);
        }

        $rules = [
            'name' => 'required',
            'mobile' => 'required|phone:IR,mobile',
            'phone' => 'min:4,max:8',
            'city_id' => 'required',
            'status' => 'required',
            'address_city' => 'required',
            'address_province' => 'required',
            'address' => 'required',
            'avatar' => 'file|mimes:jpg,png,jpeg|max:1000',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];

        $technician->name = $request->get('name');
        $technician->mobile = Web::phoneFormat($request->get('mobile'));
        $technician->phone = Web::phoneFormat($request->get('phone'));
        $technician->city_id = $request->get('city_id');
        $technician->experience = $request->get('experience');
        $technician->description = $request->get('description');
        $technician->status = $request->get('status');

        $technician->address->province_id = $request->get('address_province');
        $technician->address->city_id = $request->get('address_city');
        $technician->address->address = $request->get('address');

        if(!empty($request->get('lat'))) {
            $technician->address->lat = $request->get('lat');
            $technician->address->long = $request->get('long');
        }

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.technicians.create',[
                'title' => 'ویرایش تکنسین',
                'technician' => $technician,
                'city' => $city,
                'provinces' => $provinces,
                'province' => $province,
                'skills' => $technician->skills,
                'address_city' => $address_city
            ]);
        }

        if(!empty($request->get('skills'))){
            $technician->skills()->sync($request->get('skills'));    
        }

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $name = str_random(40);
            $request->file('avatar')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.user_avatar');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/users/' . $name . '.jpg');

            unlink(public_path() .'/temp/' . $name . '.jpg');
            if (!empty($technician->avatar) && Storage::exists(public_path() . $technician->avatar)) {
                unlink(public_path() . $technician->avatar);
            }
            $technician->avatar = '/files/users/' . $name . '.jpg';
        }

        if ($request->hasFile('catalog') && $request->file('catalog')->isValid()) {
            $name = str_random(40);
            $request->file('catalog')->move('files/catalog/', $name.'.pdf');

            if (!empty($technician->catalog) && Storage::exists(public_path() . $technician->catalog)) {
                unlink(public_path() . $technician->catalog);
            }
            $technician->catalog = '/files/catalog/' . $name . '.pdf';
        }

        $technician->address->save();
        unset($technician->address);
        $technician->save();
        Flash::success('ویرایش تکنسین با موفقیت انجام شد .');
        return redirect('/admin/technicians/');
    }

    public function delete($id)
    {        
        $technician = Technician::find($id);
        if (!$technician) {
            Flash::error('تکنسین با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/technician');
        }


        Address::where('entity', 'technicians')->where('entity_id', $technician->id)->delete();
        $technician->skills()->sync([]);


        if (!empty($technician->avatar) && Storage::exists(public_path() . $technician->avatar)) {
            unlink(public_path() . $technician->avatar);
        }

        if (!empty($technician->catalog) && Storage::exists(public_path() . $technician->catalog)) {
            unlink(public_path() . $technician->catalog);
        }

        $technician->delete();
                       
        Flash::success('تکنسین مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/technicians/');
    }

    public function highlight($type, $id, $entity_id, $data) {
        switch ($type) {
            case 'skill':
                Technician::find($id)->skills()->updateExistingPivot($entity_id, ['highlight' => $data]);
                break;
        }

        return 'ok';
    }

    public function skill(Request $request, $id)
    {
        if (!$request->isMethod('post')) {
            if(!$request->get('id')) {
                return redirect('/admin/technicians/edit/' . $id);
            }

            DB::delete('delete from technicians_skill where technicians_id = ?  AND id = ?', array($id,$request->get('id')));

            return redirect('/admin/technicians/edit/' . $id);
        }

         $rules = [
            'skill' => 'required',
            'highlight' => 'required',
        ];

        if (Web::validationCheck($request ,$rules)) {
            return Web::validation($request ,$rules, '/admin/technicians/edit/'. $id);
        }

        $technician = Technician::find($id);
        $technician->skills()->attach([$request->get('skill') => ['highlight' => $request->get('highlight')]]);
        $technician->save();
        return redirect('/admin/technicians/edit/' . $id);
    }
}
?>
