<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Article;
use Auth;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.articles.index', [
                'title' => 'لیست مقالات'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'title';
              break;

          case '2':
              $order_item = 'user_id';
              break;

          case '3':
              $order_item = 'status';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $newses = Article::where(function ($query) use ($request) {
             $query->where('title', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'));

        if(!empty($request->get('columns')['1']['search']['value']) && $request->get('columns')['1']['search']['value'] != "all") {
            $newses = $newses->where('entity', $request->get('columns')['1']['search']['value']);
        }
        
        if($request->get('entity_id') && $request->get('entity')) {
            $newses = $newses->where('entity', $request->get('entity'))->where('entity_id', $request->get('entity_id'));
        }

        $newses = $newses->get();

        $entity = [
            'technician' => 'تکنیسین',
            'company' => 'فروشنده / شرکت',
            'producer' => 'تولید کننده',
            'importer' => 'وارد کننده',
            'admin' => 'مدیر'
        ];

        foreach ($newses as $news) {
            $status = [
                'pending' => 'در انتظار انتشار',
                'publish' => 'منتشر شده',
                'suspend' => 'مسدود شده'
            ];

            $news->owner();

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/articles/delete/'.$news->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/articles/edit/'.$news->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
                'id' => $news->id,
                'title' => $news->title,
                'owner' => '<a href="/admin/'.$news->entity.'s/'.$news->entity_id.'">' . $news->{$news->entity}['first_name'] . ' ' . $news->{$news->entity}['last_name'] . ' ( ' . $entity[$news->entity] . ' )</a>',
                'status' => $status[$news->status],
                'action' => $action,
            ));
        }

        $news_count = Article::where(function ($query) use ($request) {
              $query->where('title', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $news_count,
          'recordsFiltered' => $news_count,
          'data' => $data,
        );
    }

    public function create(Request $request)
    {
        $news = new Article();
        if (!$request->isMethod('post')) {
            return view('admin.articles.create', [
                'title' => 'افزودن مقاله جدید',
                'news' => $news
            ]);
        }
        $rules = [
            'title' => 'required',
            'text' => 'required',
            'status' => 'required',
            'thumb' => 'file|mimes:jpg,png,jpeg|max:1000',
        ];
        $user = $request->user;
        $news->entity = "admin";
        $news->entity_id = $user['id'];
        $news->title = $request->get('title');
        $news->text = $request->get('text');
        $news->status = $request->get('status');

        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);

            return view('admin.articles.create', [
                'title' => 'افزودن مقاله جدید',
                'news' => $news
            ]);
        }


        if ($request->hasFile('thumb') && $request->file('thumb')->isValid()) {
            $name = str_random(40);
            $request->file('thumb')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.news_thumb');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/articles/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');
            $news->thumb = '/files/articles/' . $name .'.jpg';
        }
  
        $news->save();
        Flash::success('مقاله جدید با موفقیت افزوده شد .');
        return redirect('/admin/articles/');
    }


    public function edit(Request $request,$id)
    {
        $news = Article::find($id);
        if (!$news) {
            Flash::error('مقاله با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/news');
        }
        if (!$request->isMethod('post')) {
            return view('admin.articles.create',[
                'title' => 'ویرایش مقاله',
                'news' => $news,
            ]);
        }
        $rules = [
            'title' => 'required',
            'text' => 'required',
            'status' => 'required',
            'thumb' => 'file|mimes:jpg,png,jpeg|max:1000',
        ];
        $user = $request->user;
        $news->entity = "admin";
        $news->entity_id = $user['id'];
        $news->title = $request->get('title');
        $news->text = $request->get('text');
        $news->status = $request->get('status');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.articles.create',[
                'title' => 'ویرایش مقاله',
                'news' => $news,
            ]);
        }

       if ($request->hasFile('thumb') && $request->file('thumb')->isValid()) {
            $name = str_random(40);
            $request->file('thumb')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.news_thumb');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/articles/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name.'.jpg');

            if (!empty($news->thumb) && Storage::exists(public_path() . $news->thumb)) {
                unlink(public_path() . $news->thumb);
            }
            $brand->thumb = '/files/articles/' . $name .'.jpg';
        }

        $news->save();
        Flash::success('ویرایش مقاله با موفقیت انجام شد .');
        return redirect('/admin/articles/');
    }

    public function delete($id)
    {        
        $news = Article::find($id);
        if (!$news) {
            Flash::error('مقاله با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/articles');
        }
        if (!empty($news->thumb) && Storage::exists(public_path() . $news->thumb)) {
            unlink(public_path() . $news->thumb);
        }
        $news->delete();
                       
        Flash::success('مقاله مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/articles/');
    }
}
?>
