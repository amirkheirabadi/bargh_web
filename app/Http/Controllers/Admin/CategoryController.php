<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Helper\Web;
use Flash;
use URL;
use Image;
use Storage;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('post')) {
            return view('admin.categories.index', [
                'title' => 'لیست دسته بندی ها'
            ]);
        }
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
          case '0':
              $order_item = 'id';
              break;

          case '1':
              $order_item = 'name';
              break;

          case '2':
              $order_item = 'type';
              break;

          case '3':
              $order_item = 'verified';
              break;

          default:
            $order_item = 'id';
              break;
        }
        $data = [];

        $categories = Category::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('type', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($categories as $cat) {
            $type = [
                'product' => 'محصول',
                'brand' => 'برند'
            ];

            $verified = [
                'yes' => 'تایید شده',
                'no' => 'تایید نشده'
            ];

            $action = '<div class="btn-group">
                      <a href="'. URL::to('/admin/categories/delete/'.$cat->id) .'" class="btn btn-success btn-delete"><i class="fa fa-trash-o"></i>
                      </a>
                      <a href="'. URL::to('/admin/categories/edit/'.$cat->id) .'" class="btn btn-success"><i class="fa fa-pencil"></i>
                      </a>
                    </div>';

            array_push($data, array(
              'id' => $cat->id,
              'name' => $cat->name,
              'type' => $type[$cat->type],
              'verified' => $verified[$cat->verified],
              'action' => $action,
          ));
        }

        $categories_count = Category::where(function ($query) use ($request) {
              $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('type', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $categories_count,
          'recordsFiltered' => $categories_count,
          'data' => $data,
        );
    }

    public function get(Request $request)
    {
        $categories = Category::where('name', 'like', '%' . $request->get('q') . '%')->get();
        $results = [];

        foreach ($categories as $category) {
            $type = '';
            switch ($category->type) {
                case 'brand':
                    $type = 'برند';
                break;
                case 'product':
                    $type = 'محصول';
                break;
            }
            $results[] = [
                'id' => $category->id,
                'text' => $category->name . ' ( ' . $type . ' ) '
            ];
        }

        return [
            'items' => $results
        ];
    }

    public function create(Request $request)
    {
        $category = new Category();
        if (!$request->isMethod('post')) {
            return view('admin.categories.create', [
                'title' => 'افزودن دسته بندی جدید',
                'category' => $category
            ]);
        }
        $rules = [
            'name' => 'required',
            'type' => 'required',
            'verified' => 'required',
            'image' => 'file|mimes:jpg,png,jpeg|max:2000',
        ];

        $category->name = $request->get('name');
        $category->type = $request->get('type');
        $category->verified = $request->get('verified');
        $category->parent = $request->get('parent');
        $category->description = $request->get('description');

        if (Web::validationCheck($request, $rules)) {
            Web::validation($request, $rules);

            return view('admin.categories.create', [
                'title' => 'افزودن دسته بندی جدید',
                'category' => $category
            ]);
        }
  
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $name = str_random(40);
            $request->file('image')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.category_size');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/categories/' . $name . '.jpg');
            unlink(public_path() .'/temp/' . $name . '.jpg');
            $category->image = '/files/categories/' . $name . '.jpg';
        }

        $category->save();
        Flash::success('دسته بندی جدید با موفقیت افزوده شد .');
        return redirect('/admin/categories/');
    }


    public function edit(Request $request,$id)
    {
        $category = Category::find($id);
        if (!$category) {
            Flash::error('دسته بندی با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/categories');
        }
        $parent = false;
        if($category->parent > 0) {
            $parent = Category::find($category->parent);
            if(!$parent) {
                $parent = false;
            }
        } 
        if (!$request->isMethod('post')) {
            return view('admin.categories.create',[
                'title' => 'ویرایش دسته بندی',
                'category' => $category,
                'parent' => $parent
            ]);
        }
        $rules = [
            'name' => 'required',
            'type' => 'required',
            'verified' => 'required',
            'image' => 'file|mimes:jpg,png,jpeg|max:2000',
        ];

        $category->name = $request->get('name');
        $category->type = $request->get('type');
        $category->verified = $request->get('verified');
        $category->parent = $request->get('parent');
        $category->description = $request->get('description');

        if (Web::validationCheck($request ,$rules)) {
            Web::validation($request ,$rules);

            return view('admin.categories.create',[
                'title' => 'ویرایش دسته بندی',
                'category' => $category,
                'parent' => $parent
            ]);
        }

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $name = str_random(40);
            $request->file('image')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size= config('barghchin.category_size');

            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/categories/' . $name . '.jpg');

            unlink(public_path() .'/temp/' . $name . '.jpg');
            if (!empty($category->image) && Storage::exists(public_path() . $category->image)) {
                unlink(public_path() . $category->image);
            }
            $category->image = '/files/categories/' . $name . '.jpg';
        }
        $category->save();
        Flash::success('ویرایش دسته بندی با موفقیت انجام شد .');
        return redirect('/admin/categories/');
    }

    public function delete($id)
    {        
        $cat = Category::find($id);
        if (!$cat) {
            Flash::error('دسته بندی با این شناسه در سیستم یافت نشد .');
            return redirect('/admin/categories');
        }
        if (!empty($category->image) && Storage::exists(public_path() . $category->image)) {
            unlink(public_path() . $category->image);
        }
        Category::where('parent', $cat->id)->update(['parent' => $cat->parent]); 
        $cat->delete();
                       
        Flash::success('دسته بندی مورد نظر با موفقیت حذف شد .');
        return redirect('/admin/categories/');
    }
}
 ?>
