<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Importer;

class ImporterController extends Controller
{
    public function search(Request $request)
    {
        $importers = Importer::where('status', 'publish');

        if ($request->get('category')) {
            $importers = $request->get('category');
            $importers = $importers->whereHas('categories', function ($query) use ($category)
            {
                $query->where('id', $category);
            });
        }

        if ($request->get('city')) {
            $city = $request->get('city');
            $importers = $importers->whereHas('address', function ($query) use ($city)
            {
                $query->where('entity' ,'importer')->where('city_id', $city);
            });
        }

        if ($request->get('brand')) {
            $brand = $request->get('brand');
            $importers = $importers->whereHas('brands', function ($query) use ($brand)
            {
                $query->where('id', $brand);
            });
        }

        if ($request->get('limit')) {
            $importers = $importers->limit($request->get('limit'));
        }

        if ($request->get('skip')) {
            $importers = $importers->skip($request->get('skip'));
        }

        return [
            'message' => [],
            'data' => $importers
        ];
    }
}
