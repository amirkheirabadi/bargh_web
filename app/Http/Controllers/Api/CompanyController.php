<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;

class CompanyController extends Controller
{
    public function index(Request $request)
    {

    }

    public function search(Request $request)
    {
        $companies = Company::where('status', 'publish');

        if ($request->get('category')) {
            $companies = $request->get('category');
            $companies = $companies->whereHas('categories', function ($query) use ($category)
            {
                $query->where('id', $category);
            });
        }

        if ($request->get('city')) {
            $city = $request->get('city');
            $companies = $companies->whereHas('address', function ($query) use ($city)
            {
                $query->where('entity' ,'importer')->where('city_id', $city);
            });
        }

        if ($request->get('brand')) {
            $brand = $request->get('brand');
            $companies = $companies->whereHas('brands', function ($query) use ($brand)
            {
                $query->where('id', $brand);
            });
        }

        if ($request->get('limit')) {
            $companies = $companies->limit($request->get('limit'));
        }

        if ($request->get('skip')) {
            $companies = $companies->skip($request->get('skip'));
        }

        return [
            'message' => [],
            'data' => $companies
        ];
    }
}
