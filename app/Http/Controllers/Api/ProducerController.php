<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Producer;

class ProducerController extends Controller
{
    public function search(Request $request)
    {
        $producers = Producer::where('status', 'publish');

        if ($request->get('category')) {
            $category = $request->get('category');
            $producers = $producers->whereHas('categories', function ($query) use ($category)
            {
                $query->where('id', $category);
            });
        }

        if ($request->get('city')) {
            $city = $request->get('city');
            $producers = $producers->whereHas('address', function ($query) use ($city)
            {
                $query->where('entity' ,'producer')->where('city_id', $city);
            });
        }

        if ($request->get('limit')) {
            $producers = $producers->limit($request->get('limit'));
        }

        if ($request->get('skip')) {
            $producers = $producers->skip($request->get('skip'));
        }

        return [
            'message' => [],
            'data' => $producers
        ];
    }
}
