<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ads;
use App\Product;
use App\Category;
use App\Company;
use App\Importer;
use App\Producer;
use App\Technician;
use App\Helper\Web;

class HomeController extends Controller
{
    public function index(Request $request) {
        $result = [];
        // Home Page
        $result['home']['slider'] = Ads::where('status', 'publish')->where('type', 'home')->orderBy('created_at', 'DESC')->limit(5)->get();
        foreach ($result['home']['slider'] as $ads) {
            $ads->picture = config('app.url') . $ads->picture;
        }

        $result['home']['popular_product'] = Web::popular('product', 8);
        foreach ($result['home']['popular_product'] as $product) {
            $sizes = config('barghchin.product_thumb_sizes');
            $thumb = [];
            foreach ($sizes as $sizeName => $size) {
                $thumb[$sizeName] = config('app.url') . $product->thumb . '_' . $sizeName . '.jpg';
            }
            $product->thumb = $thumb;
        }
        $result['home']['last_product'] = Product::where('status', 'publish')->orderBy('created_at', 'DESC')->limit(8)->get();
        foreach ($result['home']['last_product'] as $product) {
            $sizes = config('barghchin.product_thumb_sizes');
            $thumb = [];
            foreach ($sizes as $sizeName => $size) {
                $thumb[$sizeName] = config('app.url') . $product->thumb . '_' . $sizeName . '.jpg';
            }
            $product->thumb = $thumb;
        }
    
        // Company Page
        // $result['company']['slider'] = Ads::where('status', 'publish')->where('type', 'company')->orderBy('created_at', 'DESC')->limit(5)->get();
        // foreach ($result['company']['slider'] as $ads) {
        //     $ads->picture = config('app.url') . '/' . $ads->picture;
        // }

        // $result['company']['last'] = Company::where('status', 'publish')->orderBy('created_at', 'DESC')->limit(8)->get();
        // foreach ($result['company']['last'] as $company) {
        //     $company->thumb = config('app.url') . '/' . $company->thumb;
        //     $company->cover = config('app.url') . '/' . $company->cover;
        // }
        // $result['company']['popular'] = Company::where('status','publish')->orderBy('rating', 'DESC')->limit(8)->get();
        // foreach ($result['company']['popular'] as $company) {
        //     $company->thumb = config('app.url') . '/' . $company->thumb;
        //     $company->cover = config('app.url') . '/' . $company->cover;
        // }

        // $result['company']['random'] = Company::where('status','publish')->limit(10)->inRandomOrder()->get();
        // foreach ($result['company']['random'] as $company) {
        //     $company->thumb = config('app.url') . '/' . $company->thumb;
        //     $company->cover = config('app.url') . '/' . $company->cover;
        // }

        // // producer Page
        // $result['producer']['slider'] = Ads::where('status', 'publish')->where('type', 'producer')->orderBy('created_at', 'DESC')->limit(5)->get();
        // foreach ($result['producer']['slider'] as $ads) {
        //     $ads->picture = config('app.url') . '/' . $ads->picture;
        // }

        // $result['producer']['last'] = Producer::where('status', 'publish')->orderBy('created_at', 'DESC')->limit(8)->get();
        // foreach ($result['producer']['last'] as $producer) {
        //     $producer->thumb = config('app.url') . '/' . $producer->thumb;
        //     $producer->cover = config('app.url') . '/' . $producer->cover;

        // }
        // $result['producer']['popular'] = Producer::where('status','publish')->orderBy('rating', 'DESC')->limit(8)->get();
        // foreach ($result['producer']['popular'] as $producer) {
        //     $producer->thumb = config('app.url') . '/' . $producer->thumb;
        //     $producer->cover = config('app.url') . '/' . $producer->cover;
        // }
        // $result['producer']['random'] = Producer::where('status','publish')->limit(10)->inRandomOrder()->get();
        // foreach ($result['producer']['random'] as $producer) {
        //     $producer->thumb = config('app.url') . '/' . $producer->thumb;
        //     $producer->cover = config('app.url') . '/' . $producer->cover;
        // }

        // // importer Page
        // $result['importer']['slider'] = Ads::where('status', 'publish')->where('type', 'importer')->orderBy('created_at', 'DESC')->limit(5)->get();
        // foreach ($result['importer']['slider'] as $ads) {
        //     $ads->picture = config('app.url') . '/' . $ads->picture;
        // }

        // $result['importer']['last'] = Importer::where('status', 'publish')->orderBy('created_at', 'DESC')->limit(8)->get();
        // foreach ($result['importer']['last'] as $importer) {
        //     $importer->thumb = config('app.url') . '/' . $importer->thumb;
        //     $importer->cover = config('app.url') . '/' . $importer->cover;

        // }
        // $result['importer']['popular'] = Importer::where('status','publish')->orderBy('rating', 'DESC')->limit(8)->get();
        // foreach ($result['importer']['popular'] as $importer) {
        //     $importer->thumb = config('app.url') . '/' . $importer->thumb;
        //     $importer->cover = config('app.url') . '/' . $importer->cover;
        // }
        // $result['importer']['random'] = Importer::where('status','publish')->limit(10)->inRandomOrder()->get();
        // foreach ($result['importer']['random'] as $importer) {
        //     $importer->thumb = config('app.url') . '/' . $importer->thumb;
        //     $importer->cover = config('app.url') . '/' . $importer->cover;
        // }

        // // technician Page
        // $result['technician']['slider'] = Ads::where('status', 'publish')->where('type', 'technician')->orderBy('created_at', 'DESC')->limit(5)->get();
        // foreach ($result['technician']['slider'] as $ads) {
        //     $ads->picture = config('app.url') . '/' . $ads->picture;
        // }

        // $result['technician']['last'] = Technician::where('status', 'publish')->orderBy('created_at', 'DESC')->limit(8)->get();
        // foreach ($result['technician']['last'] as $technician) {
        //     $technician->thumb = config('app.url') . '/' . $technician->thumb;
        //     $technician->cover = config('app.url') . '/' . $technician->cover;

        // }
        // $result['technician']['popular'] = Technician::where('status','publish')->orderBy('rating', 'DESC')->limit(8)->get();
        // foreach ($result['technician']['popular'] as $technician) {
        //     $technician->thumb = config('app.url') . '/' . $technician->thumb;
        //     $technician->cover = config('app.url') . '/' . $technician->cover;
        // }
        // $result['technician']['random'] = Technician::where('status','publish')->limit(10)->inRandomOrder()->get();
        // foreach ($result['technician']['random'] as $technician) {
        //     $technician->thumb = config('app.url') . '/' . $technician->thumb;
        //     $technician->cover = config('app.url') . '/' . $technician->cover;
        // }

        // Product Category
        $result['category_product']['categories'] = Category::where('type', 'product')->where('verified', 'yes')->where('parent', 0)->orderBy('highlight', 'DESC')->get();
        foreach ($result['category_product']['categories'] as $category) {
            $category->image = config('app.url') . '/' . $category->image;
        }

        // Brand Category
        $result['category_brand']['categories'] = Category::where('type', 'brand')->where('verified', 'yes')->where('parent', 0)->orderBy('highlight', 'DESC')->get();
        foreach ($result['category_brand']['categories'] as $category) {
            $category->image = config('app.url') . '/' . $category->image;
        }

        return response([
            'data' => $result,
            'message' => []
        ], 200);
    }
}
