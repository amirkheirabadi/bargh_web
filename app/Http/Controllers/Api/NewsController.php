<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Helper\Web;
use Config;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $skip = 0;
        if(!empty($request->get('page')) && $request->get('page') > 1) {
            $skip = Config::get('barghchin.page_limit') * ((int) $request->get('page') - 1);
        }
        $news = News::where('status', 'publish')->select(['id', 'title', 'admin_id', 'thumb', 'created_at', 'updated_at'])->orderBy('created_at')->skip($skip)->limit(Config::get('barghchin.page_limit'));
        if ($request->get('limit')) {
            $news = $news->limit($request->get('limit'));
        }

        if ($request->get('skip')) {
            $news = $news->skip($request->get('skip'));
        }

        $news = $news->get();

        foreach ($news as $article) {
            $owner = $article->owner;
            $article->writter = $owner->first_name . ' ' . $owner->last_name;
            unset($article->owner);
            unset($article->admin_id);
            $date = Web::jalali($article);
            
            // $news->thumb = Config::get('app.url') . '' . $news->thumb;

            $article->created_at = $date['created_at']['time'];
        }

        return response([
            'data' => $news,
            'message' => []
        ], 200);
    }

    public function show($id)
    {
        $news = News::where('status', 'publish')->where('id', $id)->first();
        if (!$news) {
            return response([
                'message' => [],
                'data' => []
            ], 404);
        }

        $owner = $news->owner;
        $news->writter = $owner->first_name . ' ' . $owner->last_name;
        unset($news->text);
        unset($news->owner);
        unset($news->admin_id);
        $date = Web::jalali($news);
        
        // // $news->thumb = Config::get('app.url') . '' . $news->thumb;

        $news->created_at = $date['created_at']['time'];
        return response([
            'message' => [],
            'data' => compact('news')
        ], 200);
    }
}
