<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Company;
use App\Category;
use App\Picture;
use App\Producer;
use App\Helper\Api;
use App\Helper\Web;
use jDate;
use Config;
class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::where('status' ,'publish');

        if (!empty($request->get('category'))) {
            $products = $products->where('category_id', $request->get('category'));
        }

        $products = $products->where('name', 'like' , '%' . $request->get('name') . '%')->skip(((int) $request->get('page') - 1) * 20)->orderBy('created_at', 'DESC')->limit(20)->get();

        return response([
            'data' => $products,
            'message' => []
        ], 200);
    }

    public function create(Request $request)
    {
        $rules = [
            'name' => 'required',
            'category' => 'required',
            'entity' => 'required',
            'description' => 'required',
            'price' => 'required',
            'thumb' => 'required',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();

        switch ($request->get('entity')) {
            case 'producer':
                $entity_id = Producer::where("user_id", $user->id)->first(); 
                $entity_id = $entity_id->id;    
            break;
            
            case 'company':
                $entity_id =  Company::where("user_id", $user->id)->first(); 
                $entity_id = $entity_id->id;    
            break;
        }

        $product = new Product();
        $product->name = $request->get("name");
        $product->entity = $request->get("entity");
        $product->entity_id = $entity_id;
        $product->user_id = $user->id;
        $product->status = "pending";

        $category = Category::where('name', $request->get('category'))->first();
        if (!$category) {
            $category = new Category();
            $category->name = $request->get('name');
            $category->verified = "no";
            $category->type = "product";
            $category->save();
        }
        $product->category_id = $category->id;
        $product->description = $request->get("description");
        $product->price = $request->get("price");
        $product->save();

        if ($request->hasFile('thumb')) {
            $product->setThumb($request, 'thumb');
        }

        $product->setGallery($request, "pictures");
        
        return response([
            'data' => [],
            'message' => []
        ], 200);
    }

    public function edit($id, Request $request) {
        $user = $request->user();
        $product = Product::where('id',(int) $id)->where("user_id", $user->id)->first();

        if (!$product) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }

        if ($product->status == "suspend") {
            return response([
                'data' => [],
                'message' => ['محصول مورد نظر قابل دسترسی نمی باشد .']
            ], 400);
        }
        
        $product->category;
        $product->pictures = Picture::where('entity', 'product')->where('entity_id', $product->id)->get();

        return response([
            'data' => $product,
            'message' => []
        ], 200);
    }

    public function update($id, Request $request) {
        $rules = [
            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'price' => 'required',
            'pictures' => 'array',
            'thumb' => 'required'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();

        $product = Product::where('id',(int) $id)->where("user_id", $user->id)->first();

        if (!$product) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }

        if ($product->status == "suspend") {
            return response([
                'data' => [],
                'message' => ['محصول مورد نظر قابل دسترسی نمی باشد .']
            ], 400);
        }

        $product->name = $request->get("name");
        $product->status = "pending";
        $category = Category::where('name', $request->get('category'))->first();
        if (!$category) {
            $category = new Category();
            $category->name = $request->get('name');
            $category->verified = "no";
            $category->type = "product";
            $category->save();
        }
        $product->category_id = $category->id;
        $product->description = $request->get("description");
        $product->price = $request->get("price");
        $product->save();

        if ($request->hasFile('thumb')) {
            $product->setThumb($request, 'thumb');
        }


        if (!empty($request->get('delete_gallery'))) {
            $ids = json_decode($request->get('delete_gallery'), true);
            Picture::where('entity', 'product')->where('entity_id', $product->id)->whereIn('id', $ids)->delete();
        }

        $product->setGallery($request, "pictures");
        
         return response([
            'data' => [],
            'message' => []
        ], 200);
    }

    public function delete(Request $request) {
        $user = $request->user();
        $product = Product::where('id',(int) $id)->where("user_id", $user->id)->first();

        if (!$product) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }

        if ($product->status == "suspend") {
            return response([
                'data' => [],
                'message' => ['محصول مورد نظر قابل دسترسی نمی باشد .']
            ], 400);
        }
        if (!empty($product->thumb)) {
            $sizes = config('barghchin.product_thumb_sizes');
            foreach ($sizes as $sizeName => $size) {
                if (Storage::exists(public_path(). $product->thumb . '_' . $sizeName . '.jpg')) {
                    unlink(public_path(). $product->thumb . '_' . $sizeName . '.jpg');
                }
            }    
        }

        $pictures = Picture::where("entity", "product")->where("entity_id", $product->id)->get();
        foreach ($pictures as $picture) {
            if (Storage::exists(public_path(). $picture->url)) {
                unlink(public_path(). $picture->url);
            }
            $picture->delete();
        }

        $comments = Comment::where('entity', 'product')->where('entity_id', $product->id)->delete();

        $product->delete();
        return response([
            'data' => $categories,
            'message' => []
        ], 200);
    }


    public function show($id, Request $request)
    {
        $user = $request->user();
        $product = Product::where('id',(int) $id)->where('status', 'publish')->first();

        if (!$product) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }
        $product->view = $product->view + 1;
        $product->timestamps = false;
        $product->save();
        $product->category;
        $product->pictures = Picture::where('entity', 'product')->where('entity_id', $product->id)->get();

        foreach ($product->pictures as $picture) {
            $picture->url = config('app.url') . $picture->url;
        }

        $sizes = config('barghchin.product_thumb_sizes');
        $thumb = [];
        foreach ($sizes as $sizeName => $size) {
            $thumb[$sizeName] = config('app.url') . $product->thumb . '_' . $sizeName . '.jpg';
        }
        $product->thumb = $thumb;
        if ($product->user_id == $user->id) {
            $product->mine = "yes";
        } else {
            $product->mine = "no";
        }
        unset($product->category_id);
        switch ($product->entity) {
            case 'producer':
                $product->owner = Producer::where("id", $product->entity_id)->first(); 
            break;
            
            case 'company':
                $product->owner = Company::where("id", $product->entity_id)->first(); 
            break;
        }

        if (!empty($product->owner->avatar)) {
            $product->owner->avatar = config('app.url') . $product->owner->avatar;
        } else {
            $product->owner->avatar = config('app.url') . config('barghchin.default_avatar_bussiness');
        }

        $jalali = Web::jalali($product);
        $product->created = $jalali['created_at'];
        $product->updated = $jalali['updated_at'];
        $product->view = Web::restyleText($product->view);
        $lastProduct = Product::where('id', '!=', $product->id)->where('entity' ,$product->entity)->where('entity_id', $product->entity_id)->limit(10)->orderBy('created_at', 'DESC')->where('status','publish')->get();
        $relatedProduct = Product::where('id', '!=', $product->id)->where('category_id', $product->category_id)->where('status', 'publish')->limit(10)->get();

        return response([
            'message' => [],
            'data' => [
                'product' => $product,
                'last_product' => $lastProduct,
                'related_product' => $relatedProduct 
            ]
        ]);
    }
}
