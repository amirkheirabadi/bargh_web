<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Technician;

class TechniciansController extends Controller
{
    public function search(Request $request)
    {
        $technicians = Technician::where('status', 'publish');

        if ($request->get('skill')) {
            $skill = $request->get('skill');
            $technicians = $technicians->whereHas('skills', function ($query) use ($skill)
            {
                $query->where('id', $skill);
            });
        }

        if ($request->get('city')) {
            $city = $request->get('city');
            $technicians = $technicians->whereHas('address', function ($query) use ($city)
            {
                $query->where('entity' ,'technician')->where('city_id', $city);
            });
        }

        if ($request->get('limit')) {
            $technicians = $technicians->limit($request->get('limit'));
        }

        if ($request->get('skip')) {
            $technicians = $technicians->skip($request->get('skip'));
        }

        return [
            'message' => [],
            'data' => $technicians
        ];
    }
}
