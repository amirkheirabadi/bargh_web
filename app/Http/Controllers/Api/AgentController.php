<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Agent;
use App\Technician;
use App\Company;
use App\Address;
use App\Importer;
use App\Producer;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    public $typeName = [
        'sell' => 'نمایندگی فروش',
        'service' => 'نمایندگی پس از فروش'  
    ];

	public function search(Request $request)
	{
		$rules = [
			'entity' => 'required',
            'mobile' => 'required|phone:IR,mobile',
		];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }

		switch ($request->get('entity')) {
			case 'sell':
                $record = Company::where('mobile', $request->get('mobile'))->where('status', 'active')->first();
		    break;
			
			case 'service':
				$record = Technician::where('mobile', $request->get('mobile'))->where('status', 'active')->first();
			break;
		}

        if ($record) {
            return response([
                'message' => [],
                'data' => $record,
            ], 200);
        }

        return response([
            'message' => [],
            'date' => []
        ], 400);
	}

    public function create(Request $request)
    {
        $rules = [
            'entity' => 'required',
            'mobile' => 'required|phone:IR,mobile',
            'type' => 'required'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user;
        switch ($request->get('entity')) {
            case 'importer':
                $record = Importer::where('status', 'active')->where('user_id', $user->id)->first();
                break;
            
            case 'producer':
                $record = Producer::where('status', 'active')->where('user_id', $user->id)->first();
                break;
        }
        if (!$record) {
            return response([
                'message' => [],
                'data' => []
            ], 404);
        }

        $alreadyStore = Agent::where('entity', $request->get('entity'))->where('entity_id', $record->id)->where('mobile', $request->get('mobile'))->where('type', $request->get('type'))->first();

        if ($alreadyStore) {
            return response([
                'message' => ['کاربر مورد نظر شما با شماره تلفن مشابه قبلا در سیستم ثبت شده است .'],
                'data' => []
            ], 400);
        }

        switch ($request->get('type')) {
            case 'sell':
                $typeOwner = Company::where('mobile', $request->get('mobile'))->where('status', 'active')->where('user_id', '!=', $user->id)->first();
            break;
            
            case 'service':
                $typeOwner = Technician::where('mobile', $request->get('mobile'))->where('status', 'active')->where('user_id', '!=', $user->id)->first();
            break;
        }

        $agent = new Agent;
        $agent->entity = $request->get('entity');
        $agent->entity_id = $record->id;
        $agent->mobile = $request->get('mobile');
        $agent->user_id = $user->id;
        $agent->type = $request->get('type');
        if ($typeOwner) {
            $typeOwner->address();
            $agent->type_id = $typeOwner->id;
            $agent->name = $typeOwner->name;
            $agent->address_id = $typeOwner->address->id;
            $agent->save();

            return response([
                'message' => [$this->typeName[$request->get('type')] . ' مورد نظر با موفقیت در سیستم ثبت شد .'],
                'data' => $agent
            ], 200);
        }
        $rules = [
            'name' => 'required',
            'address_province' => 'required',
            'address_city' => 'required',
            'address' => 'required',
            'geo' => 'required',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        } 
        $agent->name = $request->get('name');
        
        $address = new Address;
        $address->entity = "agent";
        $address->entity_id = $agent->id;

        $address->province_id = $request->get('address_province');
        $address->city_id = $request->get('address_city');
        $address->address = $request->get('address');
        if (!empty($request->get("geo"))) {
            $geo = json_decode($request->get('geo'), true);
            $address->lat = $geo['latitude'];
            $address->long = $geo['longitude'];
        }

        $address->save();
        $agent->address_id = $address->id;
        $agent->save();

        $address->entity_id = $agent->id;
        $address->save();

        return response([
            'message' => [$typeName[$request->get('type')] . ' مورد نظر با موفقیت در سیستم ثبت شد .'],
            'data' => $agent
        ], 200);
    }
    
    public function delete(Request $request)
    {
         $rules = [
            'id' => 'required',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user;

        $agent = Agent::where('id', $request->get('id'))->where('user_id', $user->id)->first();

        if (!$agent) {
            return response([
                'message' => [],
                'data' => []
            ], 404);
        }

        if ($agent->type_id == 0) {
            $address = Address::where('id', $agent->address_id)->first();

            $address->delete();
        }

        $agent->delete();

        return response([
            'message' => [$typeName[$request->get('type')] . ' مورد نظر شما با موفقیت از سیستم حذف شد .'],
            'data' => [],
        ], 200);
    }
}
