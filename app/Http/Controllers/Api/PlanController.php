<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Plan;
use App\Transaction;
use App\Helper\Pay;
use Carbon\Carbon;

class PlanController extends Controller
{
    public function subscribe(Request $request, $plan)
    {
        $plan = Plan::where('id', $plan)->first();
        if (!$plan) {
            return response([
                'message' => ['پلن مورد در نظر در سیستم یافت نشد .'],
                'data' => []
            ], 400);
        }

        $user = $request->user();
        $user = User::find(2);

        $trans = new Transaction;
        $trans->price = $plan->price;
        $trans->user_id = $user['id'];
        $trans->duration = $plan->duration;
        $trans->status = 'pending';
        $trans->plan = $plan->id;

        $gate = Pay::request($plan->price, 'خرید اشتراک زمانی برق چین پلن ' . $plan->name, config('app.url') . '/api/plan/check');

        if (!$gate['status']) {
            return response([
                'message' => ['خطا در اتصال به درگاه پرداخت .'],
                'data' => []
            ], 400);
        }
        $trans->ref_id = $gate['Authority'];
        $trans->save();
        $url = $gate['url'];
        return response([
            'message' => [],
            'data' => ['url' => $url] 
        ], 200);
    }


    public function check(Request $request)
    {
        $transaction = Transaction::where('ref_id', $request->get('Authority'))->where('status', 'pending')->first();
        
        if (!$transaction) {
            return redirect('/api/plan/result?id=failed');
        }

        $check = Pay::verify($transaction->ref_id, $transaction->price);

        if (!$check) {
            return redirect('/api/plan/result?id=failed');
        }

        $user = User::where('id', $transaction->user_id)->first();
        $date = Carbon::now();

        if (!empty($user->expire_premium) && $user->expire_premium > date("Y-m-d")) {
            $date = new Carbon($user->expire_premium);
        }

        $user->expire_premium = $date->addDays($transaction->duration)->toDateString();        
        $transaction->status = 'paid';

        $user->save();
        $transaction->save();

        return redirect('/api/plan/result?id=success');
    }

    public function result(Request $request)
    {
        return response([

        ],200);
    }
}
