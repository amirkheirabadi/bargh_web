<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Api;

use App\Company;
use App\Producer;
use App\Importer;
use App\Technician;
use App\Address;
use App\Province;
use App\City;
use App\Country;
use App\AppVersion;
use App\Setting;
use App\News;
use App\Article;

class ApiController extends Controller
{
    public function init(Request $request)
    {
        $rules = [
            'current' => 'version',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return response([
                'message' => [],
                'date' => []
            ], 400);
        }
    
        $version = $request->get('version');

        $version = AppVersion::where('version', $version)->first();
        if (!$version) {
             return response([
                'message' => [],
                'date' => []
            ], 400);
        }
        $versionLink = '';
        $newVersion = AppVersion::where('created_at', '>', $version->created_at)->orderBy('created_at', 'DESC')->where('id', '!=', $version->id)->first();
        if ($newVersion) {
             $versionLink = $newVersion->link;
        }
       
        $userStatus = $request->user();
        if (!$userStatus) {
            $userStatus = false;
        }

        return response([
            'message' => [],
            'data' => [
                'user' => $userStatus,
                'version_link' => $versionLink,
            ]
        ], 200);
    }
    
    public function search(Request $request) {
        $rule = [
            'current' => '', // current user lat & long'
            'distance' => '',
            'city' => '',
            'bussiness' => '',
            'brand' => '',
            'category' => '',
            'search' => '',
        ];

        $type = ['company', 'importer', 'producer', 'technician'];
        if ($request->get('bussiness') && $request->get('bussiness') !== "all") {
            $type = $request->get('bussiness');
        }

        if (!empty($request->get('city'))) {
            $address =  Address::whereIn('entity', $type)->where('city_id', $request->get('city'))->get();
            $address = $address->groupBy('entity');
        } else {
            $address =  Address::whereIn('entity', $type)->whereBetween('lat', [$request->get('current')[0] - ((int) $request->get('distance') * 0.0117), $request->get('current')[0] + ($request->get('distance')  * 0.0117)])
                ->whereBetween('long', [$request->get('current')[1] - ((int) $request->get('distance') * 0.0117), $request->get('current')[1] + ($request->get('distance')  * 0.0117)])->get();
        }

        $data = [
            'company' => [],
            'importer' => [],
            'producer' => [],
            'technician' => []
        ];
        foreach ($address as $addtype => $add) {
            $ids = [];
            foreach ($add as $ad) {
                $ids[] = $ad->entity_id;
            }
            $results = [];
            switch ($addtype) {
                case 'company':
                    $results = Company::whereIn('id' ,$ids)->where('name', 'like' ,'%' . $request->get('search') . '%');

                    if ($request->get('brand')) {
                        $results = $results->whereHas('brands', function ($query) use ($brands)
                        {
                            return $query->where('brands.id', $brands);
                        });
                    }
                break;

                case 'producer':
                    $results = Producer::whereIn('id' ,$ids)->where('name', 'like' ,'%' . $request->get('search') . '%');

                    if ($request->get('brand')) {
                        $results = $results->whereHas('brands', function ($query) use ($brands)
                        {
                            return $query->where('brands.id', $brands);
                        });
                    }
                break;

                case 'importer':
                    $results = Importer::whereIn('id' ,$ids)->where('name', 'like' ,'%' . $request->get('search') . '%');

                    if ($request->get('brand')) {
                        $results = $results->whereHas('brands', function ($query) use ($brands)
                        {
                            return $query->where('brands.id', $brands);
                        });
                    }

                    if ($request->get('category')) {
                        $results = $results->whereHas('categories', function ($query) use ($brands)
                        {
                            return $query->where('categories.id', $brands);
                        });
                    }
                break;

                case 'technician':
                    $results = Technicians::whereIn('id' ,$ids)->where('name', 'like' ,'%' . $request->get('search') . '%');

                    if ($request->get('skill')) {
                        $results = $results->whereHas('skills', function ($query) use ($brands)
                        {
                            return $query->where('skills.id', $brands);
                        });
                    }
                break;
            }
            $results = $results->get();
            foreach ($results as $res) {
                $res->address();
            }
            $data[$addtype][] = $results;
        }

        return response([
            'message' => [],
            'data' => $data
        ], 200);
    }

    public function product(Request $request)
    {
        $products = Product::where('status', 'publish');

        if (!empty($request->get('category'))) {
            $products = $products->where('category_id', $request->get('category'));
        }

        if (!empty($request->get('name'))) {
            $products = $products->where('name', 'like', '%' . $request->get('title') . '%');
        }

        $products = $products::skip(((int) $request->get('page') - 1 * 20))->limit(20)->orderBy('created_at', 'DESC');

        return response([
            'data' => $products,
            'message' => []
        ], 200);
    }

    public function province(Request $request)
    {
        $provinces = Province::all();
        return response([
            'message' => [],
            'data' => $provinces
        ], 200);
    }

    public function city(Request $request)
    {
        $city = City::where('province_id', $request->get('province'))->where('name', 'like', '%' . $request->get('name') . '%')->get();
        return response([
            'name' => json_encode($request->all()),
            'message' => [],
            'data' => $city
        ], 200);
    }

    public function country(Request $request)
    {
        $countries = Country::all();

        return response([
            'message' => [],
            'data' => $countries
        ], 200);
    }

    public function page($page)
    {
        $text = "";

        $setting = Setting::where('setting_key', $page)->first();
        if($setting) {
            $text = $setting->setting_value;
        }
        return view('api/page', compact('text'));
    }

    public function newsShow($type, $page)
    {
        $text = "";

        switch ($type) {
            case 'news':
                $news = News::where('id', $page)->first();
                if($news) {
                    $text = $news->text;
                }
                break;
            
            case 'article':
                $article = Article::where('id', $page)->first();
                if($article) {
                    $text = $article->text;
                }
                break;
        }

        return view('api/page', compact('text'));
    }

    public function add_gallery(Request $request) {
        if ($request->hasFile($input) && $request->file($input)->isValid()) {
            $name = str_random(40);
            $request->file($input)->move('thumb/', $name.'.jpg');

            $img = Image::make(public_path().'/thumb/' . $name . '.jpg');
                
            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/products/gallery/' . $name . '.jpg');

            $picture = Picture::create([
                "user_id" => $this->user_id,
                'entity' => "product",
                "entity_id" => $this->id,
                "url" => '/files/products/gallery/' . $name . '.jpg'
            ]);

            if (Storage::exists(public_path().'/thumb/' . $name . '.jpg')) {
                unlink(path().'/thumb/' . $name . '.jpg');
            }
        }
        $picture = Picture::create([
            "user_id" => $this->user_id,
            'entity' => "product",
            "entity_id" => $this->id,
            "url" => '/files/products/gallery/' . $name . '.jpg'
        ]);
    }

    public function remove_gallery($id) {
        $picture = Picture::where("entity", "product")->where("entity_id", (int) $id)->first();

        if (!$picture) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }
        if (Storage::exists(public_path(). $picture->url)) {
            unlink(path(). $picture->url);
        }

        $picture->delete();
        return response([
            'data' => $categories,
            'message' => []
        ], 200);
    }
}
