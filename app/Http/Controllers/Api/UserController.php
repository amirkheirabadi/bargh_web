<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Producer;
use App\Plan;
use App\Company;
use App\Technician;
use App\Address;
use App\Brands;
use App\Comment;
use App\Agent;
use App\picture;
use App\Importer;
use App\Verification;
use App\Helper\Api;
use App\Helper\Sms;
use App\Helper\Web;
use App\Mail\Change;
use Image;
use Hash;
use Storage;
use Config;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $user = $request->user();
        if ($request->isMethod('GET')) {
            $user = User::where('id', $user->id)->first();
            $user->profile();
            $plans = Plan::all();
            return response([
                'message' => [],
                'data' => [
                    'user' => $user,
                    'plans' => $plans
                ],
            ], 200);
        }
        $rules = [
            'type' => 'required',
        ];

        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        
        $type = $request->get('type');
        switch ($type) {
            case 'email':
                $request->merge(['email' => $request->get('value')]);
                $rules = [
                    'email' => 'required|email|unique:users,email',
                ];

                if (Api::validationCheck($request ,$rules)) {
                    return Api::validation($request, $rules);
                }
                Verification::where('type', 'email')->where('user_id', $user->id)->delete();
                $veri = new Verification;
                $veri->type = 'email';
                $veri->code = rand(10000,99999);
                $veri->email = $request->get('email');
                $veri->user_id = $user->id;
                $veri->save();  
                Mail::to($veri->email)->send(new Change($veri->code));
            break;

            case 'mobile':
                $request->merge(['mobile' => $request->get('value')]);
                $rules = [
                    'mobile' => 'required|phone:IR,mobile|unique:users,mobile',
                ];

                if (Api::validationCheck($request ,$rules)) {
                    return Api::validation($request, $rules);
                }
                Verification::where('type', 'mobile')->where('user_id', $user->id)->delete();
                $veri = new Verification;
                $veri->type = 'mobile';
                $veri->code = rand(10000,99999);
                $veri->mobile = Web::phoneFormat($request->get('mobile'));
                $veri->user_id = $user->id;
                $veri->save();  
                Sms::send($request->get('mobile'), 'کد تایید برای تغییر شماره تلفن همراه : ' . $veri->code);
            break;

            case 'first_name':
            case 'last_name':
                $request->merge([$type => $request->get('value')]);
                $rules = [
                    $type => 'required|between:1,30',
                ];

                if (Api::validationCheck($request ,$rules)) {
                    return Api::validation($request, $rules);
                }
                $user->{$request->get('type')} = $request->get('value');
                $user->save();
            break;

            case 'avatar':
                if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                    $name = str_random(40);
                    $request->file('avatar')->move('temp/', $name.'.jpg');

                    $img = Image::make(public_path().'/temp/' . $name . '.jpg');
                    $size= config('barghchin.user_avatar');

                    $img->resize($size[0], $size[1]);
                    $img->save(public_path().'/files/avatar/' . $name . '.jpg');
                    unlink(public_path() .'/temp/' . $name.'.jpg');
                    $user->avatar = '/files/avatar/' . $name .'.jpg';
                    $user->save();

                    return response([
                        'message' => [],
                        'data' => [
                            'avatar' => config('app.url') . $user->avatar 
                        ]
                    ], 200);
                }
            break;
        }

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function change(Request $request)
    {
        $rules = [
            'code' => 'required',
        ];

        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        $user = User::where('id', $user->id)->first();
        $veri = Verification::where('user_id', $user->id)->where('code' ,$request->get('code'))->first();
        if (!$veri) {
            return response([
                'message' => ['کد ورودی صحیح نمی باشد .'],
                'data' => []
            ], 400);
        }
        switch ($veri->type) {
            case 'mobile':
                $user->mobile = Web::phoneFormat($veri->mobile);
                $user->mobile_confirm = 1;
            break;
            
            case 'email':
                $user->email = $veri->email;
                $user->email_confirm = 1;
            break;
        }
        
        $user->save();
        $veri->delete();

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function password(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ];

        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        if (!Hash::check($request->get('old_password'), $user->password)) {
            return response([
                'message' => ['رمز عبور صحیح نمی باشد .'],
                'data' => []
            ] ,400);
        }

        $user->password = Hash::make($request->get('password'));
        $user->save();

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function company(Request $request)
    {
        $rules = [
            'name' => 'required',
            'owner' => 'required',
            'email' => 'email',
            'phone' => 'required|phone:IR',
            'mobile' => 'required|phone:IR,mobile|unique:companies,mobile',
            'website' => 'url',
            'address_province' => 'required',
            'address_city' => 'required',
            'address' => 'required',
            'geo' => 'required',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        $company = Company::where('user_id', $user->id)->first();
        if (!$company) {
            $company = new Company;
        }
        $company->name = $request->get('name');
        $company->owner = $request->get('owner');
        $company->user_id = $user->id;
        $company->email = $request->get('email');
        $company->mobile = Web::phoneFormat($request->get('mobile'));
        $company->phone = Web::phoneFormat($request->get('phone'));
        $company->website = $request->get('website');
        $company->fax = $request->get('fax');
        $company->status = 'pending';
        $company->description = $request->get('description');

        if (!empty($request->file('catalog')) && $request->hasFile('catalog') && $request->file('catalog')->isValid()) {
            $company->catalog = Api::catalog($request, 'catalog', $company->catalog);
        }

        $company->save();
        if (!empty($request->get("brands"))) {
            $company->setBrands(json_decode($request->get('brands'), true));
        }
        $address = Address::where('entity', 'company')->where('entity_id', $company->id)->first();
        if (!$address) {
            $address = new Address;
            $address->entity = "company";
            $address->entity_id = $company->id;
        }
        $address->province_id = $request->get('address_province');
        $address->city_id = $request->get('address_city');
        $address->address = $request->get('address');
        if (!empty($request->get("geo"))) {
            $geo = json_decode($request->get('geo'), true);
            $address->lat = $geo['latitude'];
            $address->long = $geo['longitude'];
        }

        $address->save();

        $agent = Agent::where('type', 'sell')->where('type_id', 0)->where('mobile', $company->mobile)->first();
        if ($agent) {
            $agent = $agent->type_id = $company->id;

            $agentAddress = Address::where('entity', 'agent')->where('id', $agent->address_id)->first();
            if ($agentAddress) {
                $agentAddress->delete();
            }

            $agent->address_id = $address->id;
            $agent->save();
        }

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function producer(Request $request)
    {
        $rules = [
            'name' => 'required',
            'owner' => 'required',
            'phone' => 'required|phone:IR',
            'mobile' => 'required|phone:IR,mobile',
            'website' => 'url',
            'address_province' => 'required',
            'address_city' => 'required',
            'address' => 'required',
            'geo' => 'required',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        $producer = Producer::where('user_id', $user->id)->first();
        if (!$producer) {
            $producer = new Producer;
        }
        $producer->name = $request->get('name');
        $producer->owner = $request->get('owner');
        $producer->user_id = $user->id;
        $producer->mobile = Web::phoneFormat($request->get('mobile'));
        $producer->phone = Web::phoneFormat($request->get('phone'));
        $producer->website = $request->get('website');
        $producer->status = 'pending';
        $producer->description = $request->get('description');
        $producer->save();

        if (!empty($request->get('categories'))) {
            $producer->setCategories(json_decode($request->get('categories'), true));
        }
        $address = Address::where('entity', 'producer')->where('entity_id', $producer->id)->first();
        if (!$address) {
            $address = new Address;
            $address->entity = "producer";
            $address->entity_id = $producer->id;
        }
        $address->province_id = $request->get('address_province');
        $address->city_id = $request->get('address_city');
        $address->address = $request->get('address');
        if (!empty($request->get("geo"))) {
            $geo = json_decode($request->get('geo'), true);
            $address->lat = $geo['latitude'];
            $address->long = $geo['longitude'];
        }

        return response([
            'message' => [],
            'data' => []
        ]);
    }

    public function importer(Request $request)
    {
        $rules = [
            'name' => 'required',
            'owner' => 'required',
            'phone' => 'required|phone:IR',
            'mobile' => 'required|phone:IR,mobile',
            'website' => 'url',
            'address_province' => 'required',
            'address_city' => 'required',
            'address' => 'required',
            'geo' => 'required',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        $importer = Importer::where('user_id', $user->id)->first();
        if (!$importer) {
            $importer = new Importer;
        }
        $importer->name = $request->get('name');
        $importer->owner = $request->get('owner');
        $importer->user_id = $user->id;
        $importer->phone = Web::phoneFormat($request->get('phone'));
        $importer->mobile = Web::phoneFormat($request->get('mobile'));
        $importer->website = $request->get('website');
        $importer->status = 'pending';
        $importer->description = $request->get('description');
        $importer->save();
        if (!empty($request->get('categories'))) {
            $importer->setCategories(json_decode($request->get('categories'), true));
        }
        if (!empty($request->get('brands'))) {
            $importer->setBrands(json_decode($request->get('brands'), true));
        }
        $address = Address::where('entity', 'importer')->where('entity_id', $importer->id)->first();
        if (!$address) {
            $address = new Address;
            $address->entity = "importer";
            $address->entity_id = $importer->id;
        }
        $address->province_id = $request->get('address_province');
        $address->city_id = $request->get('address_city');
        $address->address = $request->get('address');
        if (!empty($request->get("geo"))) {
            $geo = json_decode($request->get('geo'), true);
            $address->lat = $geo['latitude'];
            $address->long = $geo['longitude'];
        }

        return response([
            'message' => [],
            'data' => []
        ]);
    }

    public function technician(Request $request)
    {
        $rules = [
            'name' => 'required',
            'owner' => 'required',
            'phone' => 'required|phone:IR',
            'mobile' => 'required|phone:IR,mobile',
            'address_province' => 'required',
            'address_city' => 'required',
            'address' => 'required',
            'geo' => 'required',
            'catalog' => 'file|mimes:pdf|max:2000'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        $tech = Technician::where('user_id', $user->id)->first();
        if (!$tech) {
            $tech = new Technician;
        }
        $tech->name = $request->get('name');
        $tech->owner = $request->get('owner');
        $tech->user_id = $user->id;
        $tech->mobile = Web::phoneFormat($request->get('mobile'));
        $tech->phone = Web::phoneFormat($request->get('phone'));
        $tech->city_id = $request->get('city');
        $tech->experience = $request->get('experience');
        $tech->description = $request->get('description');
        $tech->save();
        if (!empty($request->get('skill'))) {
            $tech->setSkill(json_decode($request->get('skill') ,true));
        }

        $address = Address::where('entity', 'importer')->where('entity_id', $importer->id)->first();
        if (!$address) {
            $address = new Address;
            $address->entity = "technician";
            $address->entity_id = $tech->id;
        }
        $address->province_id = $request->get('address_province');
        $address->city_id = $request->get('address_city');
        $address->address = $request->get('address');
        if (!empty($request->get("geo"))) {
            $geo = json_decode($request->get('geo'), true);
            $address->lat = $geo['latitude'];
            $address->long = $geo['longitude'];
        }
        $address->save();

        $agent = Agent::where('type', 'service')->where('type_id', 0)->where('mobile', $company->mobile)->first();
        if ($agent) {
            $agent = $agent->type_id = $company->id;

            $agentAddress = Address::where('entity', 'agent')->where('id', $agent->address_id)->first();
            if ($agentAddress) {
                $agentAddress->delete();
            }

            $agent->address_id = $address->id;
            $agent->save();
        }

        return response([
            'message' => ['اطلاعات تکنسین با موفقیت در سیستم ثبت شد .'],
            'data' => []
        ]);
    }

    public function delete($entity, Request $request) {
        $user = $request->user();
        $class = "App\\" . $entity;
        $class = studly_case($class);
        $record = $class::where('user_id', $user->id)->first();

        if (!$record) {
            return response([
                'message' => [],
                'data' => []
            ], 404);
        }

        if (!empty($record->avatar) && Storage::exists(public_path(). $record->avatar)) {
            unlink(public_path(). $record->avatar);
        }

        if (!empty($record->cover) && Storage::exists(public_path(). $record->cover)) {
            unlink(public_path(). $record->cover);
        }

        $pictures = Picture::where('entity', $entity)->where('user_id', $user->id)->get();

        foreach ($pictures as $picture) {
            if (Storage::exists(public_path(). $picture->url)) {
                unlink(public_path(). $picture->url);
            }
            $picture->delete();
        }

        Address::where('entity', $entity)->where('entity_id', $record->id)->delete();

        Comment::where('entity', $entity)->where('entity_id', $record->id)->delete();
        $agents = Agent::where('entity', $entity)->where('entity_id', $record->id)->get();
        $agentID = [];
        foreach ($agents as $agent) {
            $agentID[] = $agent->id;
            $agent->delete();
        }

        Address::where('entity','agent')->whereIn('id', $agentID)->delete();

        switch ($entity) {
            case 'company':
                $record->brands()->detach();
            break;
            
            case 'producer':
                $record->categories()->detach();
            break;

            case 'importer':
                $record->brands()->detach();
                $record->categories()->detach();
            break;

            case 'variable':
                $record->skills()->detach();
            break;
        }        

        $record->delete();
        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function file(Request $request)
    {
        $rules = [
            'entity' => 'required',
            'type' => 'required',
            'file' => 'file|mimes:jpeg,jpg,png|max:2000'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $type = $request->get('type');
        $entity = $request->get('entity');
        $user = $request->user();
        $class = "App\\" . $entity;
        $class = studly_case($class);
        $record = $class::where('user_id', $user->id)->first();
        if (!$record) {
            return response([
                'message' => [],
                'data' => []
            ], 404);
        }

        if (!empty($record->{$type})) {
            if (Storage::exists(public_path(). $request->get('type'))) {
                unlink(public_path(). $request->get('type'));
            }
        }

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $name = str_random(40);
            $request->file('file')->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');
            $size = config('barghchin.user_' . $type);
            $img->resize($size[0], $size[1]);
            $img->save(public_path().'/files/users/' . $name . '.jpg');  

            $record->{$type} = '/files/users/' . $name . '.jpg';
        }

        $record->save();

        return response([
            'data' => [],
            'message' => []
        ], 200);
    }
}
