<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::where('name', 'like' , '%' . $request->get('name') . '%')->where('verified' , 'yes');

        if (!empty($request->get('highlight'))) {
            $categories = $categories->where('highlight', 1);
        }

        if (!empty($request->get('type'))) {
            $categories = $categories->where('type', $request->get('type'));
        }

        $categories = $categories->skip(((int) $request->get('page') - 1) * 20)->orderBy('highlight', 'DESC')->limit(20)->get();

        return response([
            'data' => $categories,
            'message' => []
        ], 200);
    }
}
