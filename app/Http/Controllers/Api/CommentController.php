<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Helper\Api;
use App\Helper\Web;

class CommentController extends Controller
{
    public function index(Request $request) {
        $rules = [
            'entity' => 'required',
            'id' => 'required',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $comments = Comment::where('entity', $request->get('entity'))->where('entity_id', $request->get('id'))->where('status', 'publish')->with(['sender', 'reciver'])->limit(config('barghchin.page_limit'));

        $comment_count = $comments->count();

        if ($request->get('page')) {
            $comments = $comments->skip(($request->get('page') - 1) * config('barghchin.page_limit'));
        }

        $comments = $comments->get();

        foreach ($comments as $comment) {
            $comment->sender->avatar = config('app.url') . config('barghchin.default_avatar_user');
        }

        foreach ($comments as $comment) {
            $jalali = Web::jalali($comment);
            $comment->created = array_get($jalali, 'created_at');
            $comment->updated = array_get($jalali, 'updated_at');
            $comment->answer_at = array_get($jalali, 'answer_at');
        }

        return response([
            'data' => [
                'comments' => $comments,
                'comment_count' => $comment_count,
                'has_more'
            ],
            'message' => []
        ], 200);
    }

    public function create(Request $request) {
        $rules = [
            'entity' => 'required',
            'entity_id' => 'required',
            'rate' => 'required',
            'message' => 'required',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();

        if (!in_array($request->get('entity'), ['company','importer','produter','technicians','product'])) {
            return response([
                'data' => [],
                'message' => ['خطا در نوع داده ورودی']
            ], 400);
        }

        $type = studly_case($request->get('entity'));

        $entity = $type::where('id', $request->get('entity_id'))->first();
        if (!$entity) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }
        if ($entity->user_id == $user->id) {
            return response([
                'data' => [],
                'message' => ['نمی توانید برای خود پیام ارسال کنید .']
            ], 400);
        }
        $comment = Comment::where('sender_id', $user->id)->where('entity', $request->get('entity'))->where('entity_id', $request->get('entity_id'))->first();

        if (!$comment) {
            $comment = new Comment();
            $comment->entity = $request->get('entity');
            $comment->entity_id = $request->get('entity_id');
            $comment->sender_id = $user->id;
            $comment->reciver_id = $entity->user_id;
        }

        $comment->rating = $request->get('rate');
        $comment->message = $request->get('message');
        $comment->status = "pending";
        $comment->save();

        return response([
            'data' => [],
            'message' => []
        ], 200);
    }

    public function answer() {
        $rules = [
            'comment_id' => 'required',
            'answer' => 'required',
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $user = $request->user();
        $comment = Comment::where('id', $request->get('comment_id'))->where('reciver_id', $user->id)->first();

        if (!$comment) {
            return response([
                'data' => [],
                'message' => []
            ], 404);
        }

        $comment->answer = $request->get('answer');
        $comment->save();
    }
}
