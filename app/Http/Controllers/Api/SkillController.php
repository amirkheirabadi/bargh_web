<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Skill;

class SkillController extends Controller
{
    public function index(Request $request)
    {
        $skills = Skill::where('name', 'like' , '%' . $request->get('name') . '%');

        $limig = 20;
        if ($request->get('limit')) {
            $limit = $request->get('limit');
        }

        $skills = $skills->limit($limit);

        if ($request->get('skip')) {
            $skills = $skills->skip($request->get('skip'));
        }

        $skills = $skills->get();

        return response([
            'data' => $skills,
            'message' => []
        ], 200);
    }
}
