<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Helper\Web;
use Config;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $skip = 0;
        if(!empty($request->get('page')) && $request->get('page') > 1) {
            $skip = Config::get('barghchin.page_limit') * ((int) $request->get('page') - 1);
        }
        
        $news = Article::where('status', 'publish')->select(['id', 'title', 'entity', 'entity_id', 'thumb', 'created_at', 'updated_at'])->orderBy('created_at')->skip($skip)->limit(Config::get('barghchin.page_limit'));
        if ($request->get('limit')) {
            $news = $news->limit($request->get('limit'));
        }

        if ($request->get('title')){
            $news = $news->where('title', 'like', '%' . $request->get('title') . '%');
        }
        
    
        if ($request->get('search')){
            $search = $request->get('search');
            $news = $news->where(function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('text' ,'like', '%' . $search . '%');
            });
        }

        if ($request->get('entity')){
            $news = $news->where('entity', $request->get('entity'));
        }

        if ($request->get('skip')) {
            $news = $news->skip($request->get('skip'));
        }

        $news = $news->get();

        foreach ($news as $article) {
            $article->owner();
            unset($article->owner);
            $date = Web::jalali($article);

            $article->thumb = Config::get('app.url') . $article->thumb;

            $article->created_at = $date['created_at']['time'];
        }

        return response([
            'data' => $news,
            'message' => []
        ], 200);
    }

    public function show($id)
    {
        $article = Article::where('status', 'publish')->where('id', $id)->first();
        if (!$article) {
            return response([
                'message' => [],
                'data' => []
            ], 404);
        }

        $article->owner();
        unset($article->owner);
        unset($article->text);
        $date = Web::jalali($article);

        // $article->thumb = Config::get('app.url') . $article->thumb;

        $article->created_at = $date['created_at']['time'];

        return response([
            'message' => [],
            'data' => compact('article')
        ], 200);
    }
}
