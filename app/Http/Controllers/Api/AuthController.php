<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Api;
use App\Helper\Sms;
use App\Helper\Web;
use Illuminate\Support\Facades\Mail;
use App\Mail\Signup;
use App\Mail\Forget;
use Hash;
use App\User;
use App\Token;
use App\Verification;
use App\OauthAccess;
class AuthController extends Controller
{
    protected $type = [
        'mobile' => 'موبایل',
        'email' => 'ایمیل'
    ];
    public function signup(Request $request)
    {
        $rules = [
            'type' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'phone:IR,mobile|unique:users,mobile',
            'email' => 'email|unique:users,email'
        ];
        if (!empty($request->get('type')) && $request->get('type') == "email") {
            $rules['email'] = 'required|' . $rules['email'];
        } else {
            $rules['mobile'] = 'required|' . $rules['mobile'];
        }

        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $type = $request->get('type');
        Verification::where($type ,$request->get($type))->where($type ,$request->get($type))->delete();
        $veri = new Verification;
        $veri->{$type} = $request->get($type);
        $veri->type = $type;

        if($type == "mobile") {
            $veri->mobile = Web::phoneFormat($request->get('mobile'));
        }

        $veri->first_name = $request->get('first_name');
        $veri->last_name = $request->get('last_name');
        $veri->code = rand(10000,99999);
        $veri->save();
        switch ($type) {
            case 'mobile': 
                Sms::send($request->get('mobile'), 'کد فعال سازی شما برای ثبت نام در برق چین : ' . $veri->code);
            break;

            case 'email':
                Mail::to($veri->email)->send(new Signup($veri->code));
            break;
        }

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function signupCheck(Request $request)
    {
        $rules = [
            'mobile' => 'phone:IR,mobile|unique:users,mobile',
            'type' => 'required',
            'email' => 'email'
        ];
        if (!empty($request->get('type')) && $request->get('type') == "email") {
            $rules['email'] = 'required|' . $rules['email'];
        } else {
            $rules['mobile'] = 'required|' . $rules['mobile'];
        }
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $type = $request->get('type');

        $veri = Verification::where($type ,$request->get($type))->where($type ,$request->get($type))->first();
        if (!$veri) {
            return response([
                'message' => ['کاربری با این ' . $this->type[$type] . ' در سیستم ثبت نشده است .'],
                'data' => []
            ], 404);
        }

        if ($veri->code != $request->get('confirmation')) {
            return response([
                'message' => ['کد فعال سازی صحیح نمی باشد .'],
                'data' => []
            ], 400);
        }

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function compilation(Request $request)
    {
        $rules = [
            'mobile' => 'phone:IR,mobile',
            'type' => 'required',
            'email' => 'email',
            'confirmation' => 'required',
            'password' => 'required|confirmed|min:6',
        ];
        if (!empty($request->get('type')) && $request->get('type') == "email") {
            $rules['email'] = 'required|' . $rules['email'];
        } else {
            $rules['mobile'] = 'required|' . $rules['mobile'];
        }
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }
        $type = $request->get('type');
        $veri = Verification::where($type ,$request->get($type))->where($type ,$request->get($type))->first();
        if (!$veri) {
            return response([
                'message' => ['کاربری با این ' . $this->type[$type] . ' در سیستم ثبت نشده است .'],
                'data' => []
            ], 404);
        }

        if ($veri->code != $request->get('confirmation')) {
            return response([
                'message' => ['کد وارد شده صحیح نمی باشد .'],
                'data' => []
            ], 400);
        }

        $user = new User;
        $user->mobile = $veri->mobile;
        $user->email = $veri->email;
        $user->{$type . '_confirm'} = true;
        $user->first_name = $veri->first_name;
        $user->last_name = $veri->last_name;
        $user->register_from = 'app';
        $user->password = Hash::make($request->get('password'));
        $user->status = "active";
        $user->save();
        $veri->delete();
        $token = $user->createToken('general')->accessToken;
        return response([
            'message' => [],
            'data' => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'mobile' => $user->mobile,
                'email' => $user->email,
                'avatar' => $user->avatar,
                'token' => $token
            ]
        ], 200);
    }

    public function forget(Request $request)
    {
        $rules = [
            'input' => 'required|phone:IR,mobile',
        ];
        $type = "mobile";
        if (Api::validationCheck($request ,$rules)) {
            $type = "email";
            $rules = [
                'input' => 'required|email',
            ];
            if (Api::validationCheck($request ,$rules)) {
                return response([
                    'message' => ['موبایل و یا ایمیل وارد شده معتبر نمی باشد .'],
                    'data' => []
                ], 400);
            }
        }

        $user = User::where($type, $request->get('input'))->first();
        if (!$user) {
            return response([
                'message' => ['کاربری با این مشخصات در سیستم ثبت نشده است .'],
                'data' => []
            ], 404);
        }

        $user->forget_token = rand(10000,99999);
        $user->save();

        switch ($type) {
            case 'mobile':
                Sms::send($request->get('input'), 'کد بازیابی رمز عبور در برق چین : ' . $user->forget_token);
            break;

            case 'email':
                Mail::to($request->get('input'))->send(new Forget($user->forget_token));
            break;
        }

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function forgetCheck(Request $request)
    {
        $rules = [
            'input' => 'required|phone:IR,mobile',
        ];
        $type = "mobile";
        if (Api::validationCheck($request ,$rules)) {
            $type = "email";
            $rules = [
                'input' => 'required|email',
            ];
            if (Api::validationCheck($request ,$rules)) {
                return response([
                    'message' => ['موبایل و یا ایمیل وارد شده معتبر نمی باشد .'],
                    'data' => []
                ], 400);
            }
        }

        $user = User::where($type, $request->get('input'))->first();
        if (!$user) {
            return response([
                'message' => ['کاربری با این مشخصات در سیستم ثبت نشده است .'],
                'data' => []
            ], 404);
        }

        if ($user->forget_token != $request->get('confirmation')) {
            return response([
                'message' => ['کد وارد شده صحیح نمی باشد .'],
                'data' => []
            ], 400);
        }

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function forgetCompilation(Request $request)
    {
        $rules = [
            'input' => 'required|phone:IR,mobile',
        ];
        $type = "mobile";
        if (Api::validationCheck($request ,$rules)) {
            $type = "email";
            $rules = [
                'input' => 'required|email',
            ];
            if (Api::validationCheck($request ,$rules)) {
                return response([
                    'message' => ['موبایل و یا ایمیل وارد شده معتبر نمی باشد .'],
                    'data' => []
                ], 400);
            }
        }
        $user = User::where($type, $request->get('input'))->first();
        if (!$user) {
            return response([
                'message' => ['کاربری با این مشخصات در سیستم ثبت نشده است .'],
                'data' => []
            ], 404);
        }


        $rules = [
            'confirmation' => 'required',
            'password' => 'required|confirmed'
        ];
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }

        if ($user->forget_token != $request->get('confirmation')) {
            return response([
                'message' => ['کد وارد شده صحیح نمی باشد .'],
                'data' => []
            ], 400);
        }

        $user->password = Hash::make($request->get('password'));
        $user->save();

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }

    public function signin(Request $request)
    {
        $rules = [
            'input' => 'required|phone:IR,mobile',
        ];
        $type = "mobile";
        if (Api::validationCheck($request ,$rules)) {
            $type = "email";
            $rules = [
                'input' => 'required|email',
            ];
            if (Api::validationCheck($request ,$rules)) {
                return response([
                    'message' => ['موبایل و یا ایمیل وارد شده معتبر نمی باشد .'],
                    'data' => []
                ], 400);
            }
        }

        $user = User::where($type ,$request->get('input'))->first();
        if (!$user) {
            return response([
                'message' => ['کاربری با این مشخصات در سیستم ثبت نشده است .'],
                'data' => []
            ], 404);
        }

        if ($user->status != "active") {
            return response([
                'message' => ['حساب کاربری مورد نظر فعال نمی باشد .'],
                'data' => []
            ], 400);
        }

        if (!Hash::check($request->get('password'), $user->password)) {
            return response([
                'message' => ['رمز عبور صحیح نمی باشد .'],
                'data' => []
            ] ,400);
        }
        Token::where('user_id', $user->id)->delete();
        $token = $user->createToken('general')->accessToken;

        return response([
            'message' => [],
            'data' => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'mobile' => $user->mobile,
                'email' => $user->email,
                'token' => $token,
            ]
        ], 200);
    }

    public function signout(Request $request)
    {
        $user = $request->user();
        OauthAccess::where('user_id', $user->id)->delete();

        return response([
            'message' => [],
            'data' => []
        ], 200);
    }
}
