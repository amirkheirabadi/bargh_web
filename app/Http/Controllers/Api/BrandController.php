<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;

class BrandController extends Controller
{
    public function index(Request $request)
    {
        $brands = Brand::where('name_fa', 'like' , '%' . $request->get('name') . '%')->Orwhere('name_en', 'like' , '%' . $request->get('name') . '%')->where('verified' ,'yes');

        if ($request->get('famous')) {
            $brands = $brands->where('famous', 'yes');
        }

        $brands = $brands->skip(((int) $request->get('page') - 1) * 20)->orderByRaw("FIELD(famous, \"yes\", \"no\")")->limit(20)->get();

        return response([
            'data' => $brands,
            'message' => []
        ], 200);
    }
}
