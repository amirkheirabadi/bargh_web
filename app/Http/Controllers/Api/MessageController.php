<?php

namespace App\Http\Controllers\Api;

use App\Message;
use App\MessageContact;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Web;
use App\Helper\Api;
use Config; 

class MessageController extends Controller
{
    public function contacts(Request $request, $status = 'normal')
    {
        $user = $request->user();
        $contacts = MessageContact::where('user_id', $user['id'])->where('status', $status)->orderBy('last_update', 'DESC')->get();

        foreach ($contacts as $contact) {
            $contact->last_update = Web::jalali($contact->last_update, true);

            $contact->getContact();
        }

        return response([
            'data' => compact('contacts'),
            'message' => []
        ], 200);
    }

    public function messageList(Request $request) {
        $user = $request->user;
        $user = [
            'id' => 2
        ];
        $rules = [
            'entity' => 'required',
            'id' => 'required',
        ];

        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }

        $entity = $request->get('entity');
        $id = $request->get('id');
        $businessUserId = "";
        if ($entity != "user") {
            $business = User::checkBusiness($entity, $id);
            if (!$business || $business->user_id == $user['id']) {
                return response([
                    'data' => [],
                    'message' => ['خطا در عملیات']
                ], 400);
            }
            $businessUserId = $business->user_id;
        }

        $conditions = [
            'user_id' => $user['id'],
            'contact_entity' => $entity,
        ];
        if ($entity == "user") {
            $conditions['contact_id'] = $id;
        } else {
            $conditions['contact_entity_id'] = $id;
        }
        $contactMine = MessageContact::firstOrCreate($conditions, [
            'user_entity' => 'user',
            'contact_id' => $businessUserId,
            'status' => 'normal',
            'last_update' => new \DateTime()
        ]);

        $conditions = [
            'contact_id' => $user['id'],
            'user_entity' => $entity,
        ];
        if ($entity == "user") {
            $conditions['user_id'] = $id;
            $businessUserId = $id;
        } else {
            $conditions['user_entity_id'] = $id;
        }
        $contactHis = MessageContact::firstOrCreate($conditions, [
            'contact_entity' => 'user',
            'user_id' => $businessUserId,
            'status' => 'normal',
            'last_update' => new \DateTime()
        ]);
        $contactHis->getContact();
        $contactMine->getContact();
        $messages = Message::whereIn('contact_id', [$contactMine->id, $contactHis->id])->skip($request->get('skip', 0))->limit($request->get('limit', 40))->get();
  
        foreach ($messages as $message) {
            $message->created = Web::jalali($message->created_at, true);
            $message->created_timestamps = $message->created_at->timestamp;

            if ($message->contact_id == $contactMine->id) {
                $message->owner = 'mine';
            } else {
                $message->owner = 'his';
            }
        }

        Message::where('contact_id', $contactHis->id)->update([
            'seen' => 'yes'
        ]);

        return response([
            'data' => [
                'messages' => $messages,
                'contact' => $contactHis->contact,
                'contact_mine' => $contactMine->contact
            ],
            'message' => []
        ], 200);
    }

    public function messageSend(Request $request) {
        $rules = [
            'type' => 'required',
            'data' => 'required',
            'from' => 'required',
            'to' => 'required',
        ];
        $user = $request->user();
        if (Api::validationCheck($request ,$rules)) {
            return Api::validation($request, $rules);
        }

        $contacts = MessageContact::whereIn('id', [$request->get('from'), $request->get('to')])->get();

        if (sizeof($contacts) != 2) {
            return response([
                'message' => ['خطا در عملیات'],
                'data' => []
            ], 400);
        }

        $contact_id = $contacts[1]['id'];


        if ($contacts[0]['user_entity'] == "user") {
            if ($contacts[0]['user_id'] == $user['id']) {
                $contact_id = $contacts[0]['id'];
            }
        } else {
            $business = User::checkBusiness($entity, $id);
            if ($business && $business->user_id == $user['id']) {
                $contact_id = $contacts[0]['id'];
            }
        }

        $message = new Message;
        $message->contact_id = $contact_id;
        $message->seen = 'no';

        switch ($request->get('type', 'text')) {
            case 'text':
                $message->text = $request->get('data');
                $message->type = $request->get('type', 'text');
                break;
        }

        $message->save();

        foreach ($contacts as $contact) {
            if ($contact->id == $contact_id) {
                $contact->unread = $contact->unread + 1;
                $contact->save();
            }
        }

        return response([
            'data' => ['message' => $message],
            'message' => []
        ], 200);
    }
}
