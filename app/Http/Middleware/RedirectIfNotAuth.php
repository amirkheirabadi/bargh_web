<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
           switch ($guard) {
               case 'admin':
                    return redirect('/admin/auth');
                break;
               
               case 'api':
                   return response([], 401);
                break;
           }
        }
        $request->user = Auth::guard($guard)->user();
        return $next($request);
    }
}
