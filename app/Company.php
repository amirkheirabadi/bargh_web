<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Address;
use App\Brand;

class Company extends Model
{
    protected $table = "companies";
    protected $primaryKey = "id";

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function brands()
    {
        return $this->belongsToMany('App\Brand', 'companies_brands', 'company_id', 'brand_id')->withPivot('endrose', 'highlight', 'id');
    }

    public function address()
    {
        if (empty($this->id)) {
            return;
        }
        $this->address = Address::where('entity', 'company')->where('entity_id', $this->id)->first();
    }

    public function setBrands($brands)
    {
        $this->brands()->detach();
        foreach ($brands as $brand) {
            $br = Brand::where('name_en', $brand['name_en'])->first();
            if (!$br) {
                $br = new Brand;
                $br->fa_name = $brand['fa_name'];
                $br->en_name = $brand['en_name'];
                $br->description = $brand['description'];
                $br->verified = "no";
                $br->save();
            }
            $this->brands()->attach($br->id);
        }
   }
}
