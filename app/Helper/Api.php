<?php
namespace App\Helper;

use Validator;
use Carbon\Carbon;
use Storage;

class Api {
    public static function validationCheck($request ,$rules)
    {
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return true;
        } else {
            return false;
        }
    }

    public static function validation($request ,$rules) {
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response([
                'message' => $validator->messages()->all(),
                'data' => []
            ], 400);
        }
    }

    // public static function date($date, $diff = false, $format = "")
    // {
    //    Carbon::setLocale('fa');
    //    $time = Carbon::createFromFormat('Y-m-d H:i:s', $date);
    // }
    
    public static function catalog($request,$input, $old = "") {
        if ($old && Storage::exists(public_path(). $old)) {
            unlink(public_path(). $old);
        }
        if ($request->hasFile($input) && $request->file($input)->isValid()) {
            $name = str_random(40);
            $request->file($input)->move('files/catalog', $name . '.pdf');
            return 'files/catalog'. $name. '.pdf';
        }
        return "";
    }
}
?>
