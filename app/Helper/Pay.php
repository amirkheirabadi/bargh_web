<?php

namespace App\Helper;

use Config;
use Zarinpal\Zarinpal;

class Pay
{
    public static function request($amount, $desc, $callback)
    {
        $test = new Zarinpal(config('Zarinpal.merchantID'));
        $answer = $test->request($callback, $amount, $desc);
        if(isset($answer['Authority'])) {
            return [
                'status' => true,
                'url' => 'https://sandbox.zarinpal.com/pg/StartPay/' . $answer['Authority'],
                'Authority' => $answer['Authority']
            ];
        }

        return [
            'status' => false
        ];
    }

    public static function verify($authority, $amount)
    {
        $test = new Zarinpal(config('Zarinpal.merchantID'));

        return $test->verify('OK',$amount, $authority);
    }
}

 ?>
