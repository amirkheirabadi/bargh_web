<?php

namespace App\Helper;

use Flash;
use Validator;
use Auth;
use jDate;
use App\Product;
use App\Company;
use App\Importer;
use App\Producer;
use App\Technician;
use DB;

class Web
{
    public static function validationCheck($request ,$rules)
    {
        $validator = Validator::make($request->all(), $rules);

        return $validator->fails();
    }

    public static function validation($request, $rules,  $url = '')
    {
        $messages = [];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            foreach ($validator->messages()->all() as $error) {
                Flash::error($error);
            }

            if ($url == '') {
                return $messages;
            }

            return redirect($url);
        }
    }
    
    public static function user($guard)
    {
        return Auth::guard($guard)->user();
    }

    public static function uploader($request ,$file ,$path , $resize ,$size = false)
    {
        if ($request->hasFile($file) && $request->file($file)->isValid()) {
            $name = str_random(40);
            $request->file($file)->move($path,  $name . '.' . $request->file($file)->getClientOriginalExtension());
            return '/' . $path . '/' . $name . '.' . $request->file($file)->getClientOriginalExtension();
        }
        return null;
    }

    public static function toGregorian($date)
    {
        $date = explode('/', $date);
        $date = \Morilog\Jalali\jDateTime::toGregorian($date[2], $date[1], $date[0]);

        return $date[0] . '-' . $date[1] . '-' . $date[2];
    }

    public static function jalali($object, $solid = false)
    {
        if ($solid) {
            $date = jDate::forge($object);
            return [
                'time' => $date->format('datetime'),
                'ago' => $date->ago(),
            ];
        }

        $results = [];
        if (!empty($object['created_at'])) {
            $date = jDate::forge($object['created_at']);
            $results['created_at'] = [
                'time' => $date->format('datetime'),
                'ago' => $date->ago(),
            ];
        }

         if (!empty($object['updated_at']) && $object['updated_at']) {
            $date = jDate::forge($object['updated_at']);
            $results['updated_at'] = [
                'time' => $date->format('datetime'),
                'ago' => $date->ago(),
            ];
        }

        if (!empty($object['answer_at']) && $object['answer_at']) {
            $date = jDate::forge($object['answer_at']);
            $results['answer_at'] = [
                'time' => $date->format('datetime'),
                'ago' => $date->ago(),
            ];
        }

        return $results;
    }

    static function restyleText($input){
        $suffixes = array('', 'k', 'm', 'g', 't');
        $suffixIndex = 0;

        while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes))
        {
            $suffixIndex++;
            $input /= 1000;
        }

        return (
            $input > 0
                // precision of 3 decimal places
                ? floor($input * 1000) / 1000
                : ceil($input * 1000) / 1000
            )
            . $suffixes[$suffixIndex];
    }

    public static function phoneFormat($mobile)
    {
        if(substr($mobile, 0,1) != 0) {
            return '0' . $mobile;
        }
        return $mobile;
    }

    public static function popular($class, $count = 5, $pastDay = null) {
        $type = "App\\" . $class;
        $type = new $type;
        $tableName = $type->getTable();

        $data = $type::leftJoin('visits', 'visits.entity_id', '=', $tableName . '.id')->where('visits.entity', $class);
        $currentDate = \Carbon\Carbon::now();
        if ($pastDay) {
            $data = $data->where('visits.created_at' ,'>',$currentDate->subDays($pastDay));
        }

        switch ($class) {
            case 'product':
                $data = $data->select($tableName . '.id', $tableName . '.name', $tableName . '.thumb', $tableName . '.category_id', $tableName . '.user_id', $tableName . '.entity', $tableName . '.description', $tableName . '.price', $tableName . '.price_to', $tableName . '.price_type', $tableName . '.price_expire', $tableName . '.status', $tableName . '.view', $tableName . '.rating', $tableName . '.comment_count', $tableName . '.created_at', $tableName . '.updated_at', DB::raw('count("visits.entity_id") as visit_count'));
            break;
        }

        $data = $data->groupBy($tableName . '.id')
            ->orderBy('visit_count', 'desc')
            ->limit($count)
            ->get();

        return $data;
    }

    public static function favorits($class ,$count = 5, $pastDay = null) {
        $type = "App\\" . $class;
        $type = new $type;
        $tableName = $type->getTable();

        $data = $type::leftJoin('endrose', 'endrose.entity_id', '=', $tableName . '.id')->where('endrose.entity', $class);
        $currentDate = \Carbon\Carbon::now();

        if ($pastDay) {
            $data = $data::where('endrose.created_at' ,$currentDate->subDays($pastDay));
        }

        switch ($class) {
            case 'product':
                $data = $data->select($tableName . '.id', $tableName . '.name', $tableName . '.thumb', $tableName . '.category_id', $tableName . '.user_id', $tableName . '.entity', $tableName . '.description', $tableName . '.price', $tableName . '.price_to', $tableName . '.price_type', $tableName . '.price_expire', $tableName . '.status', $tableName . '.view', $tableName . '.rating', $tableName . '.comment_count', $tableName . '.created_at', $tableName . '.updated_at', DB::raw('count("visits.entity_id") as favo_count'));
            break;
        }

        $data = $data->select($tableName . '.id', $tableName . '.name', DB::raw('count("endrose.entity_id") as favo_count'))
            ->groupBy($tableName . '.id')
            ->orderBy('favo_count', 'desc')
            ->limit($count)
            ->get();

        return $data;
    }
}
