<?php

namespace App\Helper;

use Config;

class Sms
{

    public static function send($number, $text)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'http://ip.sms.ir/SendMessage.ashx', [
            'form_params' => [
                'user' => Config::get('barghchin.sms.user'),
                'pass' => Config::get('barghchin.sms.pass'),
                'lineno' => Config::get('barghchin.sms.no'),
                'to' => $number,
                'text' => $text
            ]
        ]);
        $response->getStatusCode();
    }
}

 ?>
