<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Storage;
use App\Picture;
use App\Visit;
use DB;

class Product extends Model
{
    protected $table = "products";
    protected $primaryKey = "id";

    public function category() {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function formatData() {
        $pictures = Picture::where("entity", "product")->where("entity_id", $this->id)->get();
        $this->pictures = $pictures;
    }

    public function setThumb($request, $input) {
        $sizes = config('barghchin.product_thumb_sizes');
        if ($request->hasFile($input) && $request->file($input)->isValid()) {
            $name = str_random(40);
            $request->file($input)->move('temp/', $name.'.jpg');

            $img = Image::make(public_path().'/temp/' . $name . '.jpg');

            foreach ($sizes as $sizeName => $size) {
                if (!empty($this->thumb) && Storage::exists(public_path() . $this->thumb . '_' . $sizeName . '.jpg')) {
                    unlink(public_path() . $this->thumb . '_' . $sizeName . '.jpg');
                }
                $img->resize($size[0], $size[1]);
                $img->save(public_path().'/files/products/' . $name . '_' . $sizeName . '.jpg');
            }    

            $this->thumb = '/files/products/' . $name;
            $this->save(); 
            if (Storage::exists(public_path().'/temp/' . $name . '.jpg')) {
                unlink(public_path().'/temp/' . $name . '.jpg');
            }
        }
    }

    public function setGallery($request, $input) {
        $size = config('barghchin.product_gallery_size');
        for ($i=1; $i <= 10; $i++) { 
            $inputFile = $input . '_' . $i;

            if ($request->hasFile($inputFile) && $request->file($inputFile)->isValid()) {

                $name = str_random(40);
                $request->file($inputFile)->move(public_path().'/temp/', $name.'.jpg');

                $img = Image::make(public_path().'/temp/' . $name . '.jpg');
    
                $img->resize($size[0], $size[1]);

                $img->save(public_path().'/files/products/gallery/' . $name . '.jpg');

                $picture = new Picture;

                $picture->user_id =  $this->user_id;
                $picture->entity = "product";
                $picture->entity_id =  $this->id;
                $picture->url =  '/files/products/gallery/' . $name . '.jpg';
                $picture->save();
                if (Storage::exists(public_path().'/temp/' . $name . '.jpg')) {
                    unlink(public_path().'/temp/' . $name . '.jpg');
                }
            }       
        }
    }
}
