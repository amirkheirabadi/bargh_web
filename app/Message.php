<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "messages";
    protected $primaryKey = "id";


    public function contact()
    {
        return $this->hasOne('App\MessageContact', 'id', 'contact_id');
    }
}
