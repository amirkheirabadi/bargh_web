<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Importer;
use App\Producer;

class Agent extends Model
{
    protected $table = 'agents';
    protected $primaryKey = "id";

    public $timestamps = false;


    public function address()
    {
        if (empty($this->id)) {
            return;
        }
        $this->address = Address::where('entity', 'company')->where('entity_id', $this->id)->first();
    }

    public function owner()
    {
        switch ($this->entity) {
            case 'importer':
                $place = Importer::where('id', $this->entity_id)->first();
                $this->place = 'وارد کننده - ' . $place->name;
            break;

            case 'producer':
                $place = Producer::where('id', $this->entity_id)->first();
                $this->place = 'تولید کننده - ' . $place->name;
            break;
        }
    }
}
