<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;
class AdminForget extends Mailable
{
    use Queueable, SerializesModels;
    protected $code;
    protected $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $code)
    {
        $this->code = $code;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = URL::to('/admin/auth/forgetcheck/' . $this->email . '/' . $this->code);
        return $this->view('mail.adminForget')->with([
            'url' =>$url,
        ]);;
    }
}
