<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Change extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($code)
     {
         $this->code = $code;
     }

     /**
      * Build the message.
      *
      * @return $this
      */
     public function build()
     {
         return $this->view('mail.change')->with([
             'code' => $this->code,
         ]);;
     }
}
