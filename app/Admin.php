<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = "admins";
    protected $primaryKey = "id";

    public function news()
    {
        return $this->hasMany('App\News', 'admin_id', 'id');
    }
}
