<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Brand;
use App\Category;
class Importer extends Model
{
    protected $table = "importers";
    protected $primaryKey = "id";

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function categories()
    {
      return $this->belongsToMany('App\Category', 'categories_importers', 'importer_id', 'category_id')->withPivot('endrose', 'country', 'highlight', 'id');
    }

    public function address()
    {
      if (!$this->id) {
        return;
      }
      $this->address = Address::where('entity', 'importer')->where('entity_id', $this->id)->first();
    }

    public function brands()
    {
      return $this->belongsToMany('App\Brand', 'importers_brands', 'importer_id', 'brand_id')->withPivot('endrose', 'highlight', 'id');
    }

    public function setBrands($brands)
    {
        $this->brands()->detach();
        foreach ($brands as $brand) {
            $br = Brand::where('name_fa', $brand['name_fa'])->orWhere('name_en', $brand['name_en'])->first();
            if (!$br) {
                $br = new Brand;
                $br->fa_name = $brand['fa_name'];
                $br->en_name = $brand['en_name'];
                $br->description = $brand['description'];
                $br->verified = "no";
                $br->save();
            }
            $this->brands()->attach($br->id);
        }
    }

    public function setCategories($categories)
    {
       $this->categories()->detach();
       foreach ($categories as $cat['name']) {
           $br = Category::where('name', $cat['name'])->first();
           if (!$cat) {
               $cat = new Category;
               $cat->name = $cat['name'];
               $cat->verified = "no";
               $cat->save();
           }
           $this->categories()->attach($cat->id, ['country' => $cat['country']]);
       }
    }
}
