<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table = "skills";
    protected $primaryKey = "id";

    public $timestamps = false;

    public function technicians()
    {
        return $this->belongsToMany('App\Technician', 'technicians_skill', 'skill_id', 'technicians_id');
    }
}
