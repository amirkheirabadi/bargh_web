<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brands";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_brands', 'brand_id', 'category_id');
    }

    public function categorycompanies()
    {
        return $this->belongsToMany('App\Company', 'companies_brands', 'brand_id', 'company_id');
    }

    public function importers()
    {
        return $this->belongsToMany('App\Importer', 'importer_brands', 'brand_id', 'brand_id');
    }
}
