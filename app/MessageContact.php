<?php

namespace App;
use App\User;
use App\Companyl;
use App\Producer;
use App\Importer;
use App\Technician;
use Illuminate\Database\Eloquent\Model;

class MessageContact extends Model
{
    protected $table = "message_contact";
    protected $primaryKey = "id";
    protected $fillable = ['user_entity', 'user_id', 'entity_id', 'contact_entity', 'user_entity', 'contact_entity_id', 'user_entity_id', 'contact_id', 'status', 'last_update'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function contactuser() {
        return $this->belongsTo('App\User', 'contact_id', 'id');
    }

    public function getContact() {
        $entity = [
            'company'=> 'شرکت / فروشنده',
            'importer'=> 'وارد کننده',
            'producer'=> 'تولید کننده',
            'technician'=> 'تکنسین'
        ];

        switch ($this->user_entity) {
            case 'user':
                $this->contact = User::where('id', $this->user_id)->first();

                $this->contact->name = $this->contact->first_name . ' ' . $this->contact->last_name;
            break;
            
            case 'company':
                $this->contact = Company::where('id', $this->user_entity_id)->first();
            break;

            case 'producer':
                $this->contact = Producer::where('id', $this->user_entity_id)->first();
            break;

            case 'importer':
                $this->contact = Importer::where('id', $this->user_entity_id)->first();
            break;

            case 'technician':
                $this->contact = Technician::where('id', $this->user_entity_id)->first();
            break;
        }

        $avatar = config('app.url') . '/default_avatar.png';
        if(!empty($this->contact->avatar)) {
            $avatar = config('app.url') . $this->contact->avatar;
        }
        $this->contact_id = $this->contact->contact_id = $this->id;
        $this->contact->avatar = $avatar;
    }
}
