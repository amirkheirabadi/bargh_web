<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Company;
use App\Producer;
use App\Importer;
use App\Technician;
use App\Helper\Web;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $table = "users";
    protected $primaryKey = "id";

    public function findForPassport($username) {
        return $this->where('mobile', $username)->first();
    }

    public function profile()
    {
        $this->company = Company::where('user_id', $this->id)->with(['brands'])->first();
        if ($this->company) {
            $this->company->address();
        }

        $this->producer = Producer::where('user_id', $this->id)->first();
        if ($this->producer) {
            $this->producer->address();
        }
        $this->importer = Importer::where('user_id', $this->id)->first();
        if ($this->importer) {
            $this->importer->address();
        }

        $this->technician = Technician::where('user_id', $this->id)->first();
        if ($this->technician) {
            $this->technician->address();
        }
        
        $this->premium = false;
        $this->premium = date("Y-m-d");
        if ($this->expire_premium && $this->expire_premium > date("Y-m-d")) {
            $this->premium = true;

            $date = Web::jalali($this->expire_premium, true);
            $this->expire_premium = $date;
        }
        
        $avatar = config('app.url') . '/default_avatar.png';
        if($this->avatar) {
            $avatar = config('app.url') . $this->avatar;
        }
        $this->avatar = $avatar;

        unset($this->password);
    }

    public function products()
    {
        return $this->hasMany('App\Products', 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transactions', 'user_id', 'id');
    }

    public static function checkBusiness($entity, $id, $user = null) {
        $class = "App\\" . $entity;
        $class = studly_case($class);
        $record = $class::where('id', $id);

        if ($user) {
            $record = $record->where('user_id', $user);
        }

        $record = $record->first();

        return $record;
    }
}
