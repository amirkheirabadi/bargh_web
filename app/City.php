<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "city";
    protected $primarykey = "id";

    public $timestamps = false;

    public function province()
    {
        return $this->belongsTo('App\Province', 'province_id', 'id');
    }
}
