<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = "transactions";
    protected $primaryKey = "id";

    public function owner() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
