$(document).ready(function() {
    $("#brandslist").owlCarousel({
        navigation : true,
        pagination : false,
        items : 3,
    }); 
    $("#slider").owlCarousel({
        navigation : true,
        pagination : false,
        singleItem : true
    }); 
	$(window).load(function() {
		$('.sp-wrap').smoothproducts();
	});
});
$(document).ready(function() {
	//Default Action
	$(".tab_content").hide(); //Hide all content
	$("ul.tabbs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabbs li").click(function() {
		$("ul.tabbs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});
});