<?php

return [
    'page_limit' => 10,

    'sms' => [
        'no' => '50002015132337',
        'user' => '09396132337',
        'pass' => '123asdbargh'
    ],

    'news_thumb' => [960, 720],
    'product_thumb_sizes' => [
        'small' => [320,240],
        'normal' => [640,480],
        'large' => [960,720]
    ],
    'product_gallery_size' => [960,720],
    'user_avatar' => [128,128],
    'user_cover' => [960,720],
    'category_size' => [128, 128],
    'brand_size' => [128, 128],
    'ads_size' => [960,720],

    'default_avatar_user' => '/default_avatar.png',
    'default_avatar_bussiness' => '/default_avatar.png',
];
?>
